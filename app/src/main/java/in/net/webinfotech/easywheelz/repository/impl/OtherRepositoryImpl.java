package in.net.webinfotech.easywheelz.repository.impl;

import android.util.Log;

import com.google.gson.Gson;

import in.net.webinfotech.easywheelz.domain.model.Order.EmployeeOrderDetailsWrapper;
import in.net.webinfotech.easywheelz.domain.model.Order.EmployeeOrderHistoryWrapper;
import in.net.webinfotech.easywheelz.domain.model.Order.JourneyHistoryWrapper;
import in.net.webinfotech.easywheelz.domain.model.Order.OrderCancelResponse;
import in.net.webinfotech.easywheelz.domain.model.Order.VendorOrderDetailsWrapper;
import in.net.webinfotech.easywheelz.domain.model.Order.VendorOrderHistoryWrapper;
import in.net.webinfotech.easywheelz.domain.model.Other.BookingDataWrapper;
import in.net.webinfotech.easywheelz.domain.model.Other.BookingResponse;
import in.net.webinfotech.easywheelz.domain.model.Other.CabResponse;
import in.net.webinfotech.easywheelz.domain.model.Other.JourneyEndResponse;
import in.net.webinfotech.easywheelz.domain.model.Other.JourneyStartResponse;
import in.net.webinfotech.easywheelz.domain.model.Other.OrderAcceptResponse;
import in.net.webinfotech.easywheelz.domain.model.Other.OrderRejectResponse;
import in.net.webinfotech.easywheelz.repository.APIclient;
import in.net.webinfotech.easywheelz.repository.OtherRepository;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Raj on 07-06-2019.
 */

public class OtherRepositoryImpl {

    OtherRepository mRepository;

    public OtherRepositoryImpl() {
        mRepository = APIclient.createService(OtherRepository.class);
    }

    public BookingDataWrapper fetchBookingData(String apiKey, int userId){
        BookingDataWrapper bookingDataWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchData(apiKey, userId);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    bookingDataWrapper = null;
                }else{
                    bookingDataWrapper = gson.fromJson(responseBody, BookingDataWrapper.class);
                }
            } else {
                bookingDataWrapper = null;
            }
        }catch (Exception e){
            bookingDataWrapper = null;
        }
        return bookingDataWrapper;
    }

    public BookingResponse bookCab(String apiKey, int userId, int vendorId, int carTypeId, String pickUpDate, String pickUpTime, int reportingLocationId, String reportingAddress, int typeOfTravelId, String visitPlace){
        BookingResponse bookingResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> book = mRepository.book(apiKey, userId, vendorId, carTypeId, pickUpDate, pickUpTime, reportingLocationId, reportingAddress, typeOfTravelId, visitPlace);

            Response<ResponseBody> response = book.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    bookingResponse = null;
                }else{
                    bookingResponse = gson.fromJson(responseBody, BookingResponse.class);
                }
            } else {
                bookingResponse = null;
            }
        }catch (Exception e){
            bookingResponse = null;
        }
        return bookingResponse;
    }

    public CabResponse fetchCabs(String apiKey, int userId, int cabTypeId){
        CabResponse cabResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchCabs(apiKey, userId, cabTypeId);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    cabResponse = null;
                }else{
                    cabResponse = gson.fromJson(responseBody, CabResponse.class);
                }
            } else {
                cabResponse = null;
            }
        }catch (Exception e){
            cabResponse = null;
        }
        return cabResponse;
    }

    public EmployeeOrderHistoryWrapper getEmployeeOrderHistories(String apiKey, int userId, int pageNo){
        EmployeeOrderHistoryWrapper employeeOrderHistoryWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> get = mRepository.fetchEmployeeOrderHistory(apiKey, userId, pageNo);

            Response<ResponseBody> response = get.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    employeeOrderHistoryWrapper = null;
                }else{
                    employeeOrderHistoryWrapper = gson.fromJson(responseBody, EmployeeOrderHistoryWrapper.class);
                }
            } else {
                employeeOrderHistoryWrapper = null;
            }
        }catch (Exception e){
            employeeOrderHistoryWrapper = null;
        }
        return employeeOrderHistoryWrapper;
    }

    public JourneyStartResponse startJourney(String apiKey, int userId, int bookingId, String startKm){
        JourneyStartResponse journeyStartResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> start = mRepository.startJourney(apiKey, userId, bookingId, startKm);

            Response<ResponseBody> response = start.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    journeyStartResponse = null;
                }else{
                    journeyStartResponse = gson.fromJson(responseBody, JourneyStartResponse.class);
                }
            } else {
                journeyStartResponse = null;
            }
        }catch (Exception e){
            journeyStartResponse= null;
        }
        return journeyStartResponse;
    }

    public EmployeeOrderDetailsWrapper getEmployeeOrderDetails(String apiKey, int userId, int bookingId){
        EmployeeOrderDetailsWrapper employeeOrderDetailsWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> get = mRepository.getEmployeeOrderDetails(apiKey, userId, bookingId);

            Response<ResponseBody> response = get.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    employeeOrderDetailsWrapper = null;
                }else{
                    employeeOrderDetailsWrapper = gson.fromJson(responseBody, EmployeeOrderDetailsWrapper.class);
                }
            } else {
                employeeOrderDetailsWrapper = null;
            }
        }catch (Exception e){
            employeeOrderDetailsWrapper = null;
        }
        return employeeOrderDetailsWrapper;
    }

    public JourneyEndResponse endJourney(String apiKey, int userId, int bookingId, String endKm){
        JourneyEndResponse journeyEndResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> end = mRepository.endJourney(apiKey, userId, bookingId, endKm);

            Response<ResponseBody> response = end.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    journeyEndResponse = null;
                }else{
                    journeyEndResponse = gson.fromJson(responseBody, JourneyEndResponse.class);
                }
            } else {
                journeyEndResponse = null;
            }
        }catch (Exception e){
            journeyEndResponse= null;
        }
        return journeyEndResponse;
    }

    public VendorOrderHistoryWrapper getVendorOrderHistory(String apiKey, int userId, int page){
        VendorOrderHistoryWrapper vendorOrderHistoryWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchVendorOrderHistory(apiKey, userId, page);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    vendorOrderHistoryWrapper = null;
                }else{
                    vendorOrderHistoryWrapper = gson.fromJson(responseBody, VendorOrderHistoryWrapper.class);
                }
            } else {
                vendorOrderHistoryWrapper = null;
            }
        }catch (Exception e){
            vendorOrderHistoryWrapper = null;
        }
        return vendorOrderHistoryWrapper;
    }

    public VendorOrderDetailsWrapper getVendorOrderDetails(String apiKey, int userId, int bookingId){
        VendorOrderDetailsWrapper vendorOrderDetailsWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.getVendorOrderDetails(apiKey, userId, bookingId);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    vendorOrderDetailsWrapper = null;
                }else{
                    vendorOrderDetailsWrapper = gson.fromJson(responseBody, VendorOrderDetailsWrapper.class);
                }
            } else {
                vendorOrderDetailsWrapper = null;
            }
        }catch (Exception e){
            vendorOrderDetailsWrapper = null;
        }
        return vendorOrderDetailsWrapper;
    }

    public OrderAcceptResponse acceptOrder(String apiKey, int userId, int bookingId, int cabId, String driverName, String bookingMobile, String cabNo){

        OrderAcceptResponse orderAcceptResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> accept = mRepository.acceptOrder(apiKey, userId, bookingId, cabId, driverName, bookingMobile, cabNo);

            Response<ResponseBody> response = accept.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    orderAcceptResponse = null;
                }else{
                    orderAcceptResponse = gson.fromJson(responseBody, OrderAcceptResponse.class);
                }
            } else {
                orderAcceptResponse = null;
            }
        }catch (Exception e){
            orderAcceptResponse = null;
        }
        return orderAcceptResponse;
    }

    public OrderRejectResponse rejectOrder(String apiKey, int userId, int bookingId, String comment){
        OrderRejectResponse orderRejectResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> reject = mRepository.rejectOrder(apiKey, userId, bookingId, comment);

            Response<ResponseBody> response = reject.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    orderRejectResponse = null;
                }else{
                    orderRejectResponse = gson.fromJson(responseBody, OrderRejectResponse.class);
                }
            } else {
                orderRejectResponse = null;
            }
        }catch (Exception e){
            orderRejectResponse = null;
        }
        return orderRejectResponse;
    }

    public JourneyHistoryWrapper fetchEmployeeJourneyHistory(String apiKey, int userId, int page){
        JourneyHistoryWrapper journeyHistoryWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchEmployeeJourneyHistory(apiKey, userId, page);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    journeyHistoryWrapper = null;
                }else{
                    journeyHistoryWrapper = gson.fromJson(responseBody, JourneyHistoryWrapper.class);
                }
            } else {
                journeyHistoryWrapper = null;
            }
        }catch (Exception e){
            journeyHistoryWrapper = null;
        }
        return journeyHistoryWrapper;
    }

    public JourneyHistoryWrapper fetchVendorJourneyHistory(String apiKey, int userId, int page){
        JourneyHistoryWrapper journeyHistoryWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchVendorJourneyHistory(apiKey, userId, page);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    journeyHistoryWrapper = null;
                }else{
                    journeyHistoryWrapper = gson.fromJson(responseBody, JourneyHistoryWrapper.class);
                }
            } else {
                journeyHistoryWrapper = null;
            }
        }catch (Exception e){
            journeyHistoryWrapper = null;
        }
        return journeyHistoryWrapper;
    }

    public OrderCancelResponse cancelOrder(int userId, String apiKey, int bookingId){
        OrderCancelResponse orderCancelResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> cancel = mRepository.cancelBooking(apiKey, userId, bookingId);

            Response<ResponseBody> response = cancel.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    orderCancelResponse = null;
                }else{
                    orderCancelResponse = gson.fromJson(responseBody, OrderCancelResponse.class);
                }
            } else {
                orderCancelResponse = null;
            }
        }catch (Exception e){
            orderCancelResponse = null;
        }
        return orderCancelResponse;
    }

}
