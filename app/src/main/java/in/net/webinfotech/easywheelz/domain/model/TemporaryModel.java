package in.net.webinfotech.easywheelz.domain.model;

/**
 * Created by Raj on 28-03-2019.
 */

public class TemporaryModel {
    public String itemName;

    public TemporaryModel(String itemName) {
        this.itemName = itemName;
    }
}
