package in.net.webinfotech.easywheelz.presentation.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AbsListView;

import com.google.firebase.iid.FirebaseInstanceId;

import butterknife.BindView;
import in.net.webinfotech.easywheelz.R;
import in.net.webinfotech.easywheelz.domain.executors.impl.ThreadExecutor;
import in.net.webinfotech.easywheelz.presentation.presenters.VendorPresenter;
import in.net.webinfotech.easywheelz.presentation.presenters.impl.VendorPresenterImpl;
import in.net.webinfotech.easywheelz.presentation.routers.VendorsRouter;
import in.net.webinfotech.easywheelz.presentation.ui.adapters.VendorCabOrderHistoryAdapter;
import in.net.webinfotech.easywheelz.threading.MainThreadImpl;

public class VendorActivity extends BaseActivity implements VendorPresenter.View, VendorsRouter {

    @BindView(R.id.recycler_view_vendor_cab_order)
    RecyclerView recyclerView;
    VendorPresenterImpl mPresenter;
    @BindView(R.id.layout_loader)
    View layoutLoader;
    @BindView(R.id.pagination_progress_layout)
    View paginationProgressLayout;
    Boolean isScrolling = false;
    Integer currentItems;
    Integer totalItems;
    Integer scrollOutItems;
    int pageNo = 1;
    int totalPage = 1;
    LinearLayoutManager layoutManager;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inflateContent(R.layout.activity_vendor_cab_orders);
        initialisePresenter();
        showLoader();
        setFirebaseToken();
        setSwipeRefreshLayout();
    }

    public void setSwipeRefreshLayout(){
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isScrolling = false;
                pageNo = 1;
                totalPage = 1;
                mPresenter.fetchOrders(pageNo, "refresh");
            }
        });
    }

    public void initialisePresenter(){
        mPresenter = new VendorPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this, this);
    }

    @Override
    public void loadOrdersAdapter(VendorCabOrderHistoryAdapter adapter, final int totalPage) {
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
                {
                    isScrolling= true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItems = layoutManager.getChildCount();
                totalItems  = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstCompletelyVisibleItemPosition();
                if(!recyclerView.canScrollVertically(1))
                {
                    if(pageNo < totalPage) {
                        isScrolling = false;
                        pageNo = pageNo + 1;
                        showPaginationProgressLayout();
                        mPresenter.fetchOrders(pageNo, "");
                    }
                }
            }
        });
    }

    @Override
    public void showLoader() {
        layoutLoader.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoader() {
        layoutLoader.setVisibility(View.GONE);
    }

    @Override
    public void showPaginationProgressLayout() {
        paginationProgressLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hidePaginationProgressLayout() {
        paginationProgressLayout.setVisibility(View.GONE);
    }

    @Override
    public void goToOrderDetails(int bookingId, int bookingStatus) {
        Intent orderDetailsIntent;
        if(bookingStatus == 1){
            orderDetailsIntent = new Intent(this, VendorCabOrderDetailsActivity.class);
        }else{
            orderDetailsIntent = new Intent(this, VendorJourneyDetailsActivity.class);
        }
        orderDetailsIntent.putExtra("bookingId", bookingId);
        startActivity(orderDetailsIntent);
    }

    public void setFirebaseToken(){
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        mPresenter.setFirebaseToken(refreshedToken);
    }

    @Override
    protected void onResume() {
        super.onResume();
        isScrolling = false;
        pageNo = 1;
        totalPage = 1;
        mPresenter.fetchOrders(pageNo, "refresh");
    }

    @Override
    public void stopRefreshing() {
        if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing())
        {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

}
