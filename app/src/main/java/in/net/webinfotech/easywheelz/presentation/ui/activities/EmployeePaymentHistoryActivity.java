package in.net.webinfotech.easywheelz.presentation.ui.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.easywheelz.R;
import in.net.webinfotech.easywheelz.presentation.ui.adapters.EmployeePaymentHistoryAdapter;

public class EmployeePaymentHistoryActivity extends AppCompatActivity implements EmployeePaymentHistoryAdapter.Callback {

    @BindView(R.id.recycler_view_payment_history)
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_payment_history);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Payment History");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setAdapter();
    }

    public void setAdapter(){
        EmployeePaymentHistoryAdapter employeePaymentHistoryAdapter = new EmployeePaymentHistoryAdapter(this);
        recyclerView.setAdapter(employeePaymentHistoryAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }


    @Override
    public void onCardClicked(int id) {
        Intent intent = new Intent(this, EmployeeJourneyDetailsActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
