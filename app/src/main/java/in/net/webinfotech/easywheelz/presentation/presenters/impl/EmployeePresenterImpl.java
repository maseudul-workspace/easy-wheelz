package in.net.webinfotech.easywheelz.presentation.presenters.impl;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import in.net.webinfotech.easywheelz.AndroidApplication;
import in.net.webinfotech.easywheelz.domain.executors.Executor;
import in.net.webinfotech.easywheelz.domain.executors.MainThread;
import in.net.webinfotech.easywheelz.domain.interactors.CabBookInteractor;
import in.net.webinfotech.easywheelz.domain.interactors.FetchBookingDataInteractor;
import in.net.webinfotech.easywheelz.domain.interactors.SetFirebaseTokenInteractor;
import in.net.webinfotech.easywheelz.domain.interactors.impl.CabBookInteractorImpl;
import in.net.webinfotech.easywheelz.domain.interactors.impl.FetchBookingDataInteractorImpl;
import in.net.webinfotech.easywheelz.domain.interactors.impl.SetFirebaseTokenInteractorImpl;
import in.net.webinfotech.easywheelz.domain.model.Other.BookingData;
import in.net.webinfotech.easywheelz.domain.model.User.UserInfo;
import in.net.webinfotech.easywheelz.presentation.presenters.EmployeePresenter;
import in.net.webinfotech.easywheelz.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.easywheelz.repository.impl.OtherRepositoryImpl;
import in.net.webinfotech.easywheelz.repository.impl.UserRepositoryImpl;

/**
 * Created by Raj on 07-06-2019.
 */

public class EmployeePresenterImpl extends AbstractPresenter implements EmployeePresenter, FetchBookingDataInteractor.Callback, CabBookInteractor.Callback, SetFirebaseTokenInteractor.Callback {

    Context mContext;
    FetchBookingDataInteractorImpl mInteractor;
    AndroidApplication androidApplication;
    EmployeePresenter.View mView;
    CabBookInteractorImpl cabBookInteractor;
    SetFirebaseTokenInteractorImpl setFirebaseTokenInteractor;

    public EmployeePresenterImpl(Executor executor, MainThread mainThread, Context context, EmployeePresenter.View view) {
        super(executor, mainThread);
        this.mContext = context;
        mView = view;
    }

    @Override
    public void fetchBookingData() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        mInteractor = new FetchBookingDataInteractorImpl(mExecutor, mMainThread, this, new OtherRepositoryImpl(), userInfo.api_key, userInfo.user_id);
        mInteractor.execute();
    }

    @Override
    public void setFirebaseToken(String firebaseToken) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        setFirebaseTokenInteractor = new SetFirebaseTokenInteractorImpl(mExecutor, mMainThread, this, new UserRepositoryImpl(), userInfo.api_key, userInfo.user_id, firebaseToken);
        setFirebaseTokenInteractor.execute();
    }

    @Override
    public void bookCab(int vendorId, int carTypeId, String pickUpDate, String pickUpTime, int reportingLocationId, String reportingAddress, int typeOfTravelId, String visitPlace) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        cabBookInteractor  = new CabBookInteractorImpl(mExecutor, mMainThread, this, new OtherRepositoryImpl(),
                                                        userInfo.api_key, userInfo.user_id, vendorId, carTypeId, pickUpDate, pickUpTime, reportingLocationId, reportingAddress, typeOfTravelId, visitPlace);
        cabBookInteractor.execute();
    }

    @Override
    public void onBookingDataFetchSuccess(BookingData bookingData) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setBookingData(mContext, bookingData);
        mView.loadData(bookingData);
    }

    @Override
    public void onBookingDataFetchFail(String errorMsg) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        if(androidApplication.getBookingData(mContext) != null){
            mView.loadData(androidApplication.getBookingData(mContext));
        }
    }

    @Override
    public void onBookingSuccess() {
        mView.dismisDialog();
        Toasty.success(mContext, "Your booking is confirmed", Toast.LENGTH_LONG, true).show();
        mView.onBookingSuccess();
    }

    @Override
    public void onBookingFail(String errorMsg) {
        mView.dismisDialog();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_LONG, true).show();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void onSetFirebaseTokenSuccess() {

    }

    @Override
    public void onSetFirebaseTokenFail() {

    }
}
