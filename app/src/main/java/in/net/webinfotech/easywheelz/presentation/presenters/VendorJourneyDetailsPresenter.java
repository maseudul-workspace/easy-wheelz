package in.net.webinfotech.easywheelz.presentation.presenters;

import in.net.webinfotech.easywheelz.domain.model.Order.VendorOrderDetails;
import in.net.webinfotech.easywheelz.presentation.presenters.base.BasePresenter;

/**
 * Created by Raj on 02-07-2019.
 */

public interface VendorJourneyDetailsPresenter extends BasePresenter{
    void fetchJourneyDetails(int bookingId);
    interface View{
        void loadData(VendorOrderDetails vendorOrderDetails);
        void showLoader();
        void hideLoader();
    }
}
