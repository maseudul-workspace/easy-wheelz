package in.net.webinfotech.easywheelz.presentation.presenters;

import in.net.webinfotech.easywheelz.presentation.presenters.base.BasePresenter;
import in.net.webinfotech.easywheelz.presentation.ui.adapters.EmployeeJourneyHistoryAdapter;

/**
 * Created by Raj on 25-06-2019.
 */

public interface EmployeeJourneyHistoryPresenter extends BasePresenter{
    void fetchJourneyHistory(int pageNo, String type);
    interface View{
        void loadAdapter(EmployeeJourneyHistoryAdapter adapter, int totalPage);
        void showLoader();
        void hideLoader();
        void showPaginationProgressLayout();
        void hidePaginationProgressLayout();
        void stopRefreshing();
    }
}
