package in.net.webinfotech.easywheelz.presentation.presenters;

import in.net.webinfotech.easywheelz.presentation.presenters.base.BasePresenter;

/**
 * Created by Raj on 04-06-2019.
 */

public interface ResetPasswordPresenter extends BasePresenter{
    void changePassword(String apiKey, int userId, String currentPassword, String newPassword);
    interface View{
        void showLoader();
        void hideLoader();
    }
}
