package in.net.webinfotech.easywheelz.domain.interactors;

/**
 * Created by Raj on 04-06-2019.
 */

public interface ChangePasswordInteractor {
    interface Callback{
        void onChangePasswordSuccess();
        void onChangePasswordFail(String errorMsg);
    }
}
