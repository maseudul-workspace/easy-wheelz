package in.net.webinfotech.easywheelz.presentation.routers;

/**
 * Created by Raj on 21-06-2019.
 */

public interface VendorsRouter {
    void goToOrderDetails(int bookingId, int bookingStatus);
}
