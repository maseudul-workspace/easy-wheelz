package in.net.webinfotech.easywheelz.domain.interactors.impl;

import in.net.webinfotech.easywheelz.domain.executors.Executor;
import in.net.webinfotech.easywheelz.domain.executors.MainThread;
import in.net.webinfotech.easywheelz.domain.interactors.GetBookingHistoryInteractor;
import in.net.webinfotech.easywheelz.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.easywheelz.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 10-06-2019.
 */

public class GetBookingHistoryInteractorImpl extends AbstractInteractor implements GetBookingHistoryInteractor{

    Callback mCallback;
    String apiKey;
    int userId;
    OtherRepositoryImpl mRepository;

    public GetBookingHistoryInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, String apiKey, int userId, OtherRepositoryImpl mRepository) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.apiKey = apiKey;
        this.userId = userId;
        this.mRepository = mRepository;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingBookingHistoryFail(errorMsg);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingBookingHistorySuccess();
            }
        });
    }

    @Override
    public void run() {

    }
}
