package in.net.webinfotech.easywheelz.repository.impl;

import android.util.Log;

import com.google.gson.Gson;

import in.net.webinfotech.easywheelz.domain.model.User.ChangePasswordResponse;
import in.net.webinfotech.easywheelz.domain.model.User.SetFirebaseTokenResponse;
import in.net.webinfotech.easywheelz.domain.model.User.UserInfoWrapper;
import in.net.webinfotech.easywheelz.repository.APIclient;
import in.net.webinfotech.easywheelz.repository.UserRepository;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Raj on 03-06-2019.
 */

public class UserRepositoryImpl {
    UserRepository mRepository;

    public UserRepositoryImpl() {
        mRepository = APIclient.createService(UserRepository.class);
    }

    public UserInfoWrapper checkUserLogin(String email, String password, int userType){
        UserInfoWrapper userInfoWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> check = mRepository.userLogInCheck(email, password, userType);

            Response<ResponseBody> response = check.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    userInfoWrapper = null;
                }else{
                    userInfoWrapper = gson.fromJson(responseBody, UserInfoWrapper.class);
                }
            } else {
                userInfoWrapper = null;
            }
        }catch (Exception e){
            userInfoWrapper = null;
        }
        return userInfoWrapper;
    }

    public ChangePasswordResponse changePassword(String apiKey, int userId, String oldPswd, String newPswd){
        ChangePasswordResponse changePasswordResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> change = mRepository.changePassword(apiKey, userId, oldPswd, newPswd, newPswd);

            Response<ResponseBody> response = change.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    changePasswordResponse = null;
                }else{
                    changePasswordResponse = gson.fromJson(responseBody, ChangePasswordResponse.class);
                }
            } else {
                changePasswordResponse = null;
            }
        }catch (Exception e){
            changePasswordResponse = null;
        }
        return changePasswordResponse;
    }

    public SetFirebaseTokenResponse setFirebaseTokenResponse(String apiKey, int userId, String firebaseToken){
        SetFirebaseTokenResponse setFirebaseTokenResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> set = mRepository.setFireBaseToken(apiKey, userId, firebaseToken);

            Response<ResponseBody> response = set.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    setFirebaseTokenResponse = null;
                }else{
                    setFirebaseTokenResponse = gson.fromJson(responseBody, SetFirebaseTokenResponse.class);
                }
            } else {
                setFirebaseTokenResponse = null;
            }
        }catch (Exception e){
            setFirebaseTokenResponse = null;
        }
        return setFirebaseTokenResponse;
    }

}
