package in.net.webinfotech.easywheelz.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import in.net.webinfotech.easywheelz.AndroidApplication;
import in.net.webinfotech.easywheelz.domain.executors.Executor;
import in.net.webinfotech.easywheelz.domain.executors.MainThread;
import in.net.webinfotech.easywheelz.domain.interactors.GetEmployeeOrderDetailsInteractor;
import in.net.webinfotech.easywheelz.domain.interactors.impl.GetEmployeeOrderDetailsInteractorImpl;
import in.net.webinfotech.easywheelz.domain.model.Order.EmployeeOrderDetails;
import in.net.webinfotech.easywheelz.domain.model.User.UserInfo;
import in.net.webinfotech.easywheelz.presentation.presenters.EmployeeOrderDetailsPresenter;
import in.net.webinfotech.easywheelz.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.easywheelz.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 13-06-2019.
 */

public class EmployeeOrderDetailsPresenterImpl extends AbstractPresenter implements EmployeeOrderDetailsPresenter, GetEmployeeOrderDetailsInteractor.Callback {

    Context mContext;
    EmployeeOrderDetailsPresenter.View mView;
    GetEmployeeOrderDetailsInteractorImpl mInteractor;
    AndroidApplication androidApplication;

    public EmployeeOrderDetailsPresenterImpl(Executor executor,
                                             MainThread mainThread,
                                             Context context,
                                             EmployeeOrderDetailsPresenter.View view
                                             ) {
        super(executor, mainThread);
        mContext = context;
        mView = view;
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void fetchOrderDetails(int bookingId) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        mInteractor = new GetEmployeeOrderDetailsInteractorImpl(mExecutor, mMainThread, this, new OtherRepositoryImpl(), userInfo.api_key, userInfo.user_id, bookingId);
        mInteractor.execute();
    }

    @Override
    public void onGettingOrderDetailsSuccess(EmployeeOrderDetails employeeOrderDetails) {
        mView.loadData(employeeOrderDetails);
        mView.hideLoader();
    }

    @Override
    public void onGettingOrderDetailsFail(String errorMsg) {
        mView.hideLoader();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_LONG, true).show();

    }
}
