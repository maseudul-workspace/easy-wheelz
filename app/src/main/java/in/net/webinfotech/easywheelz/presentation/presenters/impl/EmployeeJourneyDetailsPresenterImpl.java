package in.net.webinfotech.easywheelz.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import in.net.webinfotech.easywheelz.AndroidApplication;
import in.net.webinfotech.easywheelz.domain.executors.Executor;
import in.net.webinfotech.easywheelz.domain.executors.MainThread;
import in.net.webinfotech.easywheelz.domain.interactors.GetEmployeeOrderDetailsInteractor;
import in.net.webinfotech.easywheelz.domain.interactors.impl.GetEmployeeOrderDetailsInteractorImpl;
import in.net.webinfotech.easywheelz.domain.model.Order.EmployeeOrderDetails;
import in.net.webinfotech.easywheelz.domain.model.User.UserInfo;
import in.net.webinfotech.easywheelz.presentation.presenters.EmployeeJourneyDetailsPresenter;
import in.net.webinfotech.easywheelz.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.easywheelz.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 01-07-2019.
 */

public class EmployeeJourneyDetailsPresenterImpl extends AbstractPresenter implements EmployeeJourneyDetailsPresenter, GetEmployeeOrderDetailsInteractor.Callback {

    Context mContext;
    EmployeeJourneyDetailsPresenter.View mView;
    GetEmployeeOrderDetailsInteractorImpl mInteractor;
    AndroidApplication androidApplication;

    public EmployeeJourneyDetailsPresenterImpl(Executor executor, MainThread mainThread, EmployeeJourneyDetailsPresenter.View view, Context context) {
        super(executor, mainThread);
        mView = view;
        mContext = context;
    }

    @Override
    public void fetchJourneyDetails(int bookingId) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        mInteractor = new GetEmployeeOrderDetailsInteractorImpl(mExecutor, mMainThread, this, new OtherRepositoryImpl(), userInfo.api_key, userInfo.user_id, bookingId);
        mInteractor.execute();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void onGettingOrderDetailsSuccess(EmployeeOrderDetails employeeOrderDetails) {
        mView.loadData(employeeOrderDetails);
        mView.hideLoader();
    }

    @Override
    public void onGettingOrderDetailsFail(String errorMsg) {
        mView.hideLoader();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT, true).show();
    }
}
