package in.net.webinfotech.easywheelz;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import in.net.webinfotech.easywheelz.domain.model.Other.BookingData;
import in.net.webinfotech.easywheelz.domain.model.User.UserInfo;

/**
 * Created by Raj on 02-04-2019.
 */

public class AndroidApplication extends Application {

    public String userType;
    public UserInfo userInfo;
    public BookingData bookingData;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public void setUserType(String type){
        userType = type;
    }

    public String getUserType(){
        return userType;
    }

    public void setUserInfo(Context context, UserInfo userInfo){
        this.userInfo = userInfo;
        SharedPreferences sharedPref = context.getSharedPreferences(
                getString(R.string.KEY_FLUFFY_APP_PREFERENCES), Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();

        if(userInfo != null) {
            editor.putString(getString(R.string.KEY_USER_DETAILS), new Gson().toJson(userInfo));
        } else {
            editor.putString(getString(R.string.KEY_USER_DETAILS), "");
        }

        editor.commit();
    }

    public UserInfo getUserInfo(Context context){
        UserInfo user;
        if(userInfo != null){
            user = userInfo;
        }else{
            SharedPreferences sharedPref = context.getSharedPreferences(
                    getString(R.string.KEY_FLUFFY_APP_PREFERENCES), Context.MODE_PRIVATE);
            String userInfoJson = sharedPref.getString(getString(R.string.KEY_USER_DETAILS),"");
            if(userInfoJson.isEmpty()){
                user = null;
            }else{
                try {
                    user = new Gson().fromJson(userInfoJson, UserInfo.class);
                    this.userInfo = user;
                }catch (Exception e){
                    user = null;
                }
            }
        }
        return user;
    }

    public void setBookingData(Context context, BookingData bookingData){
        this.bookingData = bookingData;
        SharedPreferences sharedPref = context.getSharedPreferences(
                getString(R.string.KEY_FLUFFY_APP_PREFERENCES), Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();

        if(bookingData != null) {
            editor.putString(getString(R.string.KEY_BOOKING_DATA), new Gson().toJson(bookingData));
        } else {
            editor.putString(getString(R.string.KEY_BOOKING_DATA), "");
        }

        editor.commit();
    }

    public BookingData getBookingData(Context context){
        BookingData bookingData;
        if(this.bookingData != null){
            bookingData = this.bookingData;
        }else{
            SharedPreferences sharedPref = context.getSharedPreferences(
                    getString(R.string.KEY_FLUFFY_APP_PREFERENCES), Context.MODE_PRIVATE);
            String bookingDataJson = sharedPref.getString(getString(R.string.KEY_BOOKING_DATA),"");
            if(bookingDataJson.isEmpty()){
                bookingData = null;
            }else{
                try {
                    bookingData = new Gson().fromJson(bookingDataJson, BookingData.class);
                    this.bookingData = bookingData;
                }catch (Exception e){
                    bookingData = null;
                }
            }
        }
        return bookingData;
    }

}
