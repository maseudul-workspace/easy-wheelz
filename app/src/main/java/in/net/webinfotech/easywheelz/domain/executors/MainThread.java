package in.net.webinfotech.easywheelz.domain.executors;

/**
 * Created by Raj on 26-03-2019.
 */

public interface MainThread {
    /**
     * Make runnable operation run in the main thread.
     *
     * @param runnable The runnable to run.
     */
    void post(final Runnable runnable);
}
