package in.net.webinfotech.easywheelz.presentation.routers;

/**
 * Created by Raj on 13-06-2019.
 */

public interface EmployeeOrderHistoryRouter {
    void goToJourneyDetails(int booingId);
    void goToOrderDetails(int bookingId);
}
