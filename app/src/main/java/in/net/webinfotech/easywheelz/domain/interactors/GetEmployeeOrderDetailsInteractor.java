package in.net.webinfotech.easywheelz.domain.interactors;

import in.net.webinfotech.easywheelz.domain.model.Order.EmployeeOrderDetails;

/**
 * Created by Raj on 12-06-2019.
 */

public interface GetEmployeeOrderDetailsInteractor {
    interface Callback{
        void onGettingOrderDetailsSuccess(EmployeeOrderDetails employeeOrderDetails);
        void onGettingOrderDetailsFail(String errorMsg);
    }
}
