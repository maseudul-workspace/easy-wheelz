package in.net.webinfotech.easywheelz.domain.interactors;

import in.net.webinfotech.easywheelz.domain.model.User.UserInfo;

/**
 * Created by Raj on 03-06-2019.
 */

public interface CheckUserLoginInteractor {
    interface Callback{
        void onLoginSuccess(UserInfo userInfo);
        void onLoginFail(String errorMsg);
    }
}
