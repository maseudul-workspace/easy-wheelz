package in.net.webinfotech.easywheelz.presentation.ui.dialogs;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import in.net.webinfotech.easywheelz.R;

/**
 * Created by Raj on 13-06-2019.
 */

public class JourneyFinishConfirmationDialog {

    public interface Callback{
        void onConfirmationClicked();
    }

    AlertDialog.Builder builder;
    Context mContext;
    Activity mActivity;
    AlertDialog dialog;
    View dialogContainer;
    Callback mCallback;

    public JourneyFinishConfirmationDialog(Context mContext, Activity mActivity, Callback mCallback) {
        this.mContext = mContext;
        this.mActivity = mActivity;
        this.mCallback = mCallback;
    }

    public void showDialog(String startingKm, String endingKm, String totalKm){
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.layout_journey_finish_confirm_dialog, null);

        TextView txtViewStartKm = (TextView) dialogContainer.findViewById(R.id.txt_view_start_km);
        TextView txtViewEndKm = (TextView) dialogContainer.findViewById(R.id.txt_view_end_km);
        TextView txtTotalDistance = (TextView) dialogContainer.findViewById(R.id.txt_view_total_km);
        Button btnConfirm = (Button) dialogContainer.findViewById(R.id.btn_confirm_journey_finish);

        txtViewStartKm.setText(startingKm);
        txtViewEndKm.setText(endingKm);
        txtTotalDistance.setText(totalKm);

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onConfirmationClicked();
                dialog.dismiss();
            }
        });

        builder = new AlertDialog.Builder(mContext);
        builder.setView(dialogContainer);
        dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.show();

    }

}
