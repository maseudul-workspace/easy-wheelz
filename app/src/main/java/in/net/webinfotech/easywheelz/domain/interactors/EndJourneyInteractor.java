package in.net.webinfotech.easywheelz.domain.interactors;

/**
 * Created by Raj on 13-06-2019.
 */

public interface EndJourneyInteractor {
    interface Callback{
        void onJourneyEndSuccess();
        void onJourneyEndFail(String errorMsg);
    }
}
