package in.net.webinfotech.easywheelz.domain.interactors.impl;

import in.net.webinfotech.easywheelz.domain.executors.Executor;
import in.net.webinfotech.easywheelz.domain.executors.MainThread;
import in.net.webinfotech.easywheelz.domain.interactors.CheckUserLoginInteractor;
import in.net.webinfotech.easywheelz.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.easywheelz.domain.model.User.UserInfo;
import in.net.webinfotech.easywheelz.domain.model.User.UserInfoWrapper;
import in.net.webinfotech.easywheelz.repository.impl.UserRepositoryImpl;

/**
 * Created by Raj on 03-06-2019.
 */

public class CheckUserLoginInteractorImpl extends AbstractInteractor implements CheckUserLoginInteractor {

    UserRepositoryImpl mRepository;
    Callback mCallback;
    String email;
    String password;
    int userType;

    public CheckUserLoginInteractorImpl(Executor threadExecutor,
                                        MainThread mainThread,
                                        Callback callback,
                                        UserRepositoryImpl repository,
                                        String email,
                                        String password,
                                        int userType
                                        ) {
        super(threadExecutor, mainThread);
        this.mCallback = callback;
        this.mRepository = repository;
        this.email = email;
        this.password = password;
        this.userType = userType;

    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onLoginFail(errorMsg);
            }
        });
    }

    private void postMessage(final UserInfo userInfo){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onLoginSuccess(userInfo);
            }
        });
    }

    @Override
    public void run() {
        final UserInfoWrapper userInfoWrapper = mRepository.checkUserLogin(email, password, userType);
        if(userInfoWrapper == null){
           notifyError("Something went wrong");
        }else if(!userInfoWrapper.status){
            notifyError(userInfoWrapper.message);
        }else{
            postMessage(userInfoWrapper.userInfo);
        }
    }
}
