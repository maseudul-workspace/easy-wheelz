package in.net.webinfotech.easywheelz.domain.interactors.impl;

import in.net.webinfotech.easywheelz.domain.executors.Executor;
import in.net.webinfotech.easywheelz.domain.executors.MainThread;
import in.net.webinfotech.easywheelz.domain.interactors.FetchCabInteractor;
import in.net.webinfotech.easywheelz.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.easywheelz.domain.model.Other.Cab;
import in.net.webinfotech.easywheelz.domain.model.Other.CabResponse;
import in.net.webinfotech.easywheelz.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 10-06-2019.
 */

public class FetchCabInteractorImpl extends AbstractInteractor implements FetchCabInteractor {

    Callback mCallback;
    OtherRepositoryImpl mRepository;
    String apiKey;
    int userId;
    int cabTypeId;

    public FetchCabInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, OtherRepositoryImpl mRepository, String apiKey, int userId, int cabTypeId) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.apiKey = apiKey;
        this.userId = userId;
        this.cabTypeId = cabTypeId;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onCabFetchFail(errorMsg);
            }
        });
    }

    private void postMessage(final Cab[] cabs){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onCabFetchSuccess(cabs);
            }
        });
    }

    @Override
    public void run() {
        final CabResponse cabResponse = mRepository.fetchCabs(apiKey, userId, cabTypeId);
        if(cabResponse == null){
            notifyError("Something went wrong");
        }else if(!cabResponse.status){
            notifyError(cabResponse.message);
        }else{
            postMessage(cabResponse.cabs);
        }
    }
}
