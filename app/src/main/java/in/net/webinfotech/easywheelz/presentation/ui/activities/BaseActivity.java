package in.net.webinfotech.easywheelz.presentation.ui.activities;

import android.content.Intent;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;

import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.easywheelz.AndroidApplication;
import in.net.webinfotech.easywheelz.R;

public class BaseActivity extends AppCompatActivity {

    @BindView(R.id.toolbar) @Nullable
    Toolbar toolbar;
    Drawer employeeDrawer;
    Drawer vendorDrawer;
    AndroidApplication androidApplication;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected void inflateContent(@LayoutRes int inflatedResID) {
        setContentView(R.layout.activity_base);
        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame);
        getLayoutInflater().inflate(inflatedResID, contentFrameLayout);
        ButterKnife.bind(this);
        androidApplication = (AndroidApplication) getApplicationContext();
        if(androidApplication.getUserInfo(this).userType == 1){
            setEmployeeDrawer();
        }else{
            setVendorDrawer();
        }
    }

    public void setEmployeeDrawer(){
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);

        // Create the AccountHeader
        AccountHeader headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.drawable.login_gradient_background)
                .addProfiles(new ProfileDrawerItem().withIcon(R.drawable.logo_white).withName("Easy Wheels"))
                .build();

        employeeDrawer = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withAccountHeader(headerResult)
                .withHasStableIds(true)
                .withDisplayBelowStatusBar(false)
                .withActionBarDrawerToggleAnimated(true)
                .addDrawerItems(
                        new PrimaryDrawerItem().withName("Request Cab").withIcon(R.drawable.cab_grey).withSelectable(false).withIdentifier(1),
                        new PrimaryDrawerItem().withName("Order History").withIcon(R.drawable.order_grey).withSelectable(false).withIdentifier(3),
                        new PrimaryDrawerItem().withName("Journey History").withIcon(R.drawable.payment_icon).withSelectable(false).withIdentifier(4),
                        new PrimaryDrawerItem().withName("Change Password").withIcon(R.drawable.password).withSelectable(false).withIdentifier(7),
                        new PrimaryDrawerItem().withName("Log Out").withIcon(R.drawable.logout_menu).withSelectable(false).withIdentifier(8)
                ).withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        if(drawerItem != null){
                            switch (((int) drawerItem.getIdentifier())){
                                case 1:
                                    Intent employeeIntent = new Intent(getApplicationContext(), EmployeeActivity.class);
                                    startActivity(employeeIntent);
                                    break;
                                case 7:
                                    Intent resetPasswordActivity = new Intent(getApplicationContext(), ResetPasswordActivity.class);
                                    startActivity(resetPasswordActivity);
                                    break;
                                case 6:
                                    Intent journeyIntent = new Intent(getApplicationContext(), EmployeeJourneyActivity.class);
                                    journeyIntent.putExtra("type", "end");
                                    startActivity(journeyIntent);
                                    break;
                                case 4:
                                    Intent journeyHistoryIntent = new Intent(getApplicationContext(), EmployeeJourneyHistoryActivity.class);
                                    startActivity(journeyHistoryIntent);
                                    break;
                                case 3:
                                    Intent orderHistoryIntent = new Intent(getApplicationContext(), EmployeeOrderHistoryActivity.class);
                                    startActivity(orderHistoryIntent);
                                    break;
                                case 5:
                                    Intent otherCabBookingIntent = new Intent(getApplicationContext(), EmployeeOtherCabBookActivity.class);
                                    startActivity(otherCabBookingIntent);
                                    break;
                                case 8:
                                    androidApplication = (AndroidApplication) getApplicationContext();
                                    androidApplication.setUserInfo(getApplicationContext(), null);
                                    Intent loginIntent = new Intent(getApplicationContext(), LoginActivity.class);
                                    startActivity(loginIntent);
                                    finish();
                                    break;
                            }
                        }
                        return false;
                    }
                }).build();
        actionbar.setHomeAsUpIndicator(R.drawable.ic_menu);
    }

    public void setVendorDrawer(){
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setTitle("Order History");
        // Create the AccountHeader
        AccountHeader headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.drawable.login_gradient_background)
                .addProfiles(new ProfileDrawerItem().withIcon(R.drawable.logo_white).withName("Easy Wheels"))
                .build();

        vendorDrawer = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withAccountHeader(headerResult)
                .withHasStableIds(true)
                .withDisplayBelowStatusBar(false)
                .withActionBarDrawerToggleAnimated(true)
                .addDrawerItems(
                        new PrimaryDrawerItem().withName("Order History").withIcon(R.drawable.order_grey).withSelectable(false).withIdentifier(2),
                        new PrimaryDrawerItem().withName("Journey History").withIcon(R.drawable.payment_icon).withSelectable(false).withIdentifier(3),
                        new PrimaryDrawerItem().withName("Change Password").withIcon(R.drawable.password).withSelectable(false).withIdentifier(4),
                        new PrimaryDrawerItem().withName("Log Out").withIcon(R.drawable.logout_menu).withSelectable(false).withIdentifier(5)
                ).withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        if(drawerItem != null){
                            switch (((int) drawerItem.getIdentifier())){
                                case 2:
                                    Intent ordersIntent = new Intent(getApplicationContext(), VendorActivity.class);
                                    startActivity(ordersIntent);
                                    break;
                                case 3:
                                    Intent journeysIntent = new Intent(getApplicationContext(), VendorJourneyHistoryActivity.class);
                                    startActivity(journeysIntent);
                                    break;
                                case 4:
                                    Intent resetPasswordActivity = new Intent(getApplicationContext(), ResetPasswordActivity.class);
                                    startActivity(resetPasswordActivity);
                                    break;
                                case 5:
                                    androidApplication = (AndroidApplication) getApplicationContext();
                                    androidApplication.setUserInfo(getApplicationContext(), null);
                                    Intent loginIntent = new Intent(getApplicationContext(), LoginActivity.class);
                                    startActivity(loginIntent);
                                    finish();
                                    break;
                            }
                        }
                        return false;
                    }
                }).build();
        actionbar.setHomeAsUpIndicator(R.drawable.ic_menu);
    }

}
