package in.net.webinfotech.easywheelz.domain.interactors;

/**
 * Created by Raj on 11-07-2019.
 */

public interface CancelOrderInteractor {
    interface Callback{
        void onOrderCancelSuccess(int position);
        void onOrderCancelFail(String errorMsg);
    }
}
