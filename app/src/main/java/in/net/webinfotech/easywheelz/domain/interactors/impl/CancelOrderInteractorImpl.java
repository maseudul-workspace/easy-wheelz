package in.net.webinfotech.easywheelz.domain.interactors.impl;

import in.net.webinfotech.easywheelz.domain.executors.Executor;
import in.net.webinfotech.easywheelz.domain.executors.MainThread;
import in.net.webinfotech.easywheelz.domain.interactors.CancelOrderInteractor;
import in.net.webinfotech.easywheelz.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.easywheelz.domain.model.Order.OrderCancelResponse;
import in.net.webinfotech.easywheelz.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 11-07-2019.
 */

public class CancelOrderInteractorImpl extends AbstractInteractor implements CancelOrderInteractor {

    Callback mCallback;
    OtherRepositoryImpl mRepository;
    String apiKey;
    int userId;
    int bookingId;
    int position;

    public CancelOrderInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, OtherRepositoryImpl mRepository, String apiKey, int userId, int bookingId, int position) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.apiKey = apiKey;
        this.userId = userId;
        this.bookingId = bookingId;
        this.position = position;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onOrderCancelFail(errorMsg);
            }
        });
    }

    private void postMessage(final int position){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onOrderCancelSuccess(position);
            }
        });
    }

    @Override
    public void run() {
        final OrderCancelResponse orderCancelResponse = mRepository.cancelOrder(userId, apiKey, bookingId);
        if(orderCancelResponse == null){
            notifyError("Something Went Wrong");
        } else if(!orderCancelResponse.status){
            notifyError(orderCancelResponse.message);
        }else{
            postMessage(position);
        }
    }
}
