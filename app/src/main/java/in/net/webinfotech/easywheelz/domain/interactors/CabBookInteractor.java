package in.net.webinfotech.easywheelz.domain.interactors;

/**
 * Created by Raj on 08-06-2019.
 */

public interface CabBookInteractor {
    interface Callback{
        void onBookingSuccess();
        void onBookingFail(String errorMsg);
    }
}
