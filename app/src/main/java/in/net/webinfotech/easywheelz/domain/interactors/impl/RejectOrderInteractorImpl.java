package in.net.webinfotech.easywheelz.domain.interactors.impl;

import in.net.webinfotech.easywheelz.domain.executors.Executor;
import in.net.webinfotech.easywheelz.domain.executors.MainThread;
import in.net.webinfotech.easywheelz.domain.interactors.RejectOrderInterator;
import in.net.webinfotech.easywheelz.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.easywheelz.domain.model.Other.OrderRejectResponse;
import in.net.webinfotech.easywheelz.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 21-06-2019.
 */

public class RejectOrderInteractorImpl extends AbstractInteractor implements RejectOrderInterator {

    Callback mCallback;
    OtherRepositoryImpl mRepository;
    String apiKey;
    int userId;
    int bookingId;
    String comment;

    public RejectOrderInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, OtherRepositoryImpl mRepository, String apiKey, int userId, int bookingId, String comment) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.apiKey = apiKey;
        this.userId = userId;
        this.bookingId = bookingId;
        this.comment = comment;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onOrderRejectFail(errorMsg);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onOrderRejectSuccess();
            }
        });
    }

    @Override
    public void run() {
        final OrderRejectResponse orderRejectResponse = mRepository.rejectOrder(apiKey, userId, bookingId, comment);
        if(orderRejectResponse == null){
            notifyError("Something went wrong");
        }else if(!orderRejectResponse.status){
            notifyError(orderRejectResponse.message);
        }else{
            postMessage();
        }
    }
}
