package in.net.webinfotech.easywheelz.presentation.ui.activities;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import in.net.webinfotech.easywheelz.R;
import in.net.webinfotech.easywheelz.domain.executors.impl.ThreadExecutor;
import in.net.webinfotech.easywheelz.domain.model.Order.VendorOrderDetails;
import in.net.webinfotech.easywheelz.domain.model.Other.Cab;
import in.net.webinfotech.easywheelz.presentation.presenters.VendorOrderDetailsPresenter;
import in.net.webinfotech.easywheelz.presentation.presenters.impl.VendorOrderDetailsPresenterImpl;
import in.net.webinfotech.easywheelz.threading.MainThreadImpl;

public class VendorCabOrderDetailsActivity extends AppCompatActivity implements VendorOrderDetailsPresenter.View {

    View orderAcceptDialogContainer;
    View orderRejectDialogContainer;
    AlertDialog.Builder orderAcceptBuilder;
    AlertDialog.Builder orderRejectBuilder;
    AlertDialog orderAcceptDialog;
    AlertDialog orderRejectDialog;
    RadioGroup carsRadioGroup;
    VendorOrderDetailsPresenterImpl mPresenter;
    @BindView(R.id.txt_view_employee_name)
    TextView txtViewEmployeeName;
    @BindView(R.id.txt_view_employee_email)
    TextView txtViewEmployeeEmail;
    @BindView(R.id.txt_view_pick_up_time)
    TextView txtViewPickUpTime;
    @BindView(R.id.txt_view_pick_up_date)
    TextView txtViewPickUpDate;
    @BindView(R.id.txt_view_reporting_address)
    TextView txtViewReportingAddress;
    @BindView(R.id.txt_view_place_to_visit)
    TextView txtViewPlaceToVisit;
    @BindView(R.id.txt_view_car_type)
    TextView txtViewCarType;
    @BindView(R.id.main_layout)
    View mainLayout;
    @BindView(R.id.layout_loader)
    View layoutLoader;
    @BindView(R.id.txt_view_type_of_travel)
    TextView txtViewTypeOfTravel;
    View acceptDialogLoader;
    int bookingId;
    int cabId;
    Cab[] cabs;
    boolean isCabSelected = false;
    View rejectDialogLooader;
    @BindView(R.id.txt_view_availability_status)
    TextView txtViewAvailabilityStatus;
    @BindView(R.id.btn_order_accept)
    Button btnOrderAccept;
    @BindView(R.id.btn_order_reject)
    Button btnOrderReject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendor_cab_order_details);
        ButterKnife.bind(this);
        bookingId = getIntent().getIntExtra("bookingId", 0);
        getSupportActionBar().setTitle("Order Details");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setOrderAcceptDialog();
        setOrderRejectDialog();
        initialisePresenter();
        showLoader();
        mPresenter.getOrderDetails(bookingId);
    }

    public void initialisePresenter(){
        mPresenter = new VendorOrderDetailsPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setOrderAcceptDialog(){
        orderAcceptDialogContainer = getLayoutInflater().inflate(R.layout.vendor_order_accept_dialog, null);

//        Initialise different views
        carsRadioGroup = (RadioGroup) orderAcceptDialogContainer.findViewById(R.id.radio_group_car_type);
        final ImageView imgViewDownArrowCars = (ImageView) orderAcceptDialogContainer.findViewById(R.id.img_view_down_arrow_cars);
        final ImageView imgViewUpArrowCars = (ImageView) orderAcceptDialogContainer.findViewById(R.id.img_view_up_arrow_cars);
        acceptDialogLoader = (View) orderAcceptDialogContainer.findViewById(R.id.layout_loader_dialog);
        final EditText editTextDriverName = (EditText) orderAcceptDialogContainer.findViewById(R.id.edit_text_driver_name);
        final EditText editTextDriverPhone = (EditText) orderAcceptDialogContainer.findViewById(R.id.edit_text_driver_phone_no);
        final EditText editTextCabNo = (EditText) orderAcceptDialogContainer.findViewById(R.id.edit_text_cab_no);
        Button btnSubmit = (Button) orderAcceptDialogContainer.findViewById(R.id.btn_submit);
//        Set up listeners
        imgViewDownArrowCars.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgViewDownArrowCars.setVisibility(View.GONE);
                imgViewUpArrowCars.setVisibility(View.VISIBLE);
                carsRadioGroup.setVisibility(View.VISIBLE);
            }
        });

        carsRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                isCabSelected = true;
                RadioButton radioButton = (RadioButton) radioGroup.findViewById(i);
                String cabName = radioButton.getText().toString();
                for(int index = 0; index < cabs.length; index++){
                    if(cabs[index].cab_name.equals(cabName)){
                        cabId = cabs[index].cab_id;
                        break;
                    }
                }
            }
        });

        imgViewUpArrowCars.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgViewUpArrowCars.setVisibility(View.GONE);
                imgViewDownArrowCars.setVisibility(View.VISIBLE);
                carsRadioGroup.setVisibility(View.GONE);
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!isCabSelected || editTextCabNo.getText().toString().trim().isEmpty() || editTextDriverName.getText().toString().trim().isEmpty() || editTextDriverPhone.getText().toString().trim().isEmpty()){
                    Toasty.error(getApplicationContext(), "Fields missing", Toast.LENGTH_SHORT, true).show();
                }else{
                    showAcceptDialogLoader();
                    mPresenter.acceptOrder(bookingId, cabId, editTextDriverName.getText().toString(), editTextDriverPhone.getText().toString(), editTextCabNo.getText().toString());
                }
            }
        });

//        Create dialog builder
        orderAcceptBuilder = new AlertDialog.Builder(this, android.R.style.Theme_Light_NoTitleBar_Fullscreen);
        orderAcceptBuilder.setView(orderAcceptDialogContainer);
        orderAcceptDialog = orderAcceptBuilder.create();
    }

    @OnClick(R.id.btn_order_accept) void onAcceptClicked(){
        orderAcceptDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        orderAcceptDialog.show();
    }

    public void setOrderRejectDialog(){
        orderRejectDialogContainer = getLayoutInflater().inflate(R.layout.vendor_order_reject_dialog, null);
        rejectDialogLooader = (View) orderRejectDialogContainer.findViewById(R.id.layout_loader_dialog);
        final EditText editTextRejectComment = (EditText) orderRejectDialogContainer.findViewById(R.id.edit_text_reject_comment);
        Button btnSubmit = (Button) orderRejectDialogContainer.findViewById(R.id.btn_submit);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(editTextRejectComment.getText().toString().trim().isEmpty()){
                    Toasty.error(getApplicationContext(), "Fields missing", Toast.LENGTH_SHORT, true).show();
                }else{
                    showRejectDialogLoader();
                    mPresenter.rejectOrder(bookingId, editTextRejectComment.getText().toString());
                }
            }
        });

        orderRejectBuilder = new AlertDialog.Builder(this, android.R.style.Theme_Light_NoTitleBar_Fullscreen);
        orderRejectBuilder.setView(orderRejectDialogContainer);
        orderRejectDialog = orderRejectBuilder.create();
    }

    @OnClick(R.id.btn_order_reject) void onRejectClicked(){
        orderRejectDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        orderRejectDialog.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void setCabs(Cab[] cabs) {
        this.cabs = cabs;
        if(this.cabs.length == 0){
            txtViewAvailabilityStatus.setText("* No Cabs Available");
            btnOrderAccept.setVisibility(View.GONE);
        }else{
            for(int i = 0; i < cabs.length; i++){
                RadioButton radioButton = new RadioButton(this);
                radioButton.setText(cabs[i].cab_name);
                radioButton.setId(View.generateViewId());
                carsRadioGroup.addView(radioButton);
            }
        }
    }

    @Override
    public void loadData(VendorOrderDetails vendorOrderDetails) {
        mainLayout.setVisibility(View.VISIBLE);
        txtViewEmployeeName.setText(vendorOrderDetails.employee_name);
        txtViewEmployeeEmail.setText(vendorOrderDetails.employee_email);
        txtViewCarType.setText(vendorOrderDetails.cabType);
        txtViewPickUpDate.setText(vendorOrderDetails.pickupDate);
        txtViewPickUpTime.setText(vendorOrderDetails.pickUpTime);
        txtViewPlaceToVisit.setText(vendorOrderDetails.visitPlace);
        txtViewReportingAddress.setText(vendorOrderDetails.reportingAddress);
        txtViewTypeOfTravel.setText(vendorOrderDetails.travelTypeName);
        if(vendorOrderDetails.bookingStatus == 4){
            txtViewAvailabilityStatus.setText("* Booking Cancelled");
            btnOrderAccept.setVisibility(View.GONE);
            btnOrderReject.setVisibility(View.GONE);
        }else{
            mPresenter.fetchCabs(vendorOrderDetails.cabTypeId);

        }
    }

    @Override
    public void showLoader() {
        layoutLoader.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoader() {
        layoutLoader.setVisibility(View.GONE);
    }

    @Override
    public void showAcceptDialogLoader() {
        acceptDialogLoader.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideAcceptDialogLoader() {
        acceptDialogLoader.setVisibility(View.GONE);
    }

    @Override
    public void showRejectDialogLoader() {
        rejectDialogLooader.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideRejectDialogLoader() {
        rejectDialogLooader.setVisibility(View.GONE);
    }

    @Override
    public void finishActivity() {
        finish();
    }

    @Override
    public void onCabGettingFailed() {
        txtViewAvailabilityStatus.setText("* No Cabs Available");
        btnOrderAccept.setVisibility(View.GONE);
    }
}
