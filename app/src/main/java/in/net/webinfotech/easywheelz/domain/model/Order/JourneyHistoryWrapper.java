package in.net.webinfotech.easywheelz.domain.model.Order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 25-06-2019.
 */

public class JourneyHistoryWrapper {
    @SerializedName("status")
    @Expose
    public boolean status;

    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("code")
    @Expose
    public int code;

    @SerializedName("total_page")
    @Expose
    public int total_page;

    @SerializedName("data")
    @Expose
    public JourneyDetails[] journeyDetails;

}
