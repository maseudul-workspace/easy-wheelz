package in.net.webinfotech.easywheelz.domain.interactors.impl;

import in.net.webinfotech.easywheelz.domain.executors.Executor;
import in.net.webinfotech.easywheelz.domain.executors.MainThread;
import in.net.webinfotech.easywheelz.domain.interactors.ChangePasswordInteractor;
import in.net.webinfotech.easywheelz.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.easywheelz.domain.model.User.ChangePasswordResponse;
import in.net.webinfotech.easywheelz.repository.impl.UserRepositoryImpl;

/**
 * Created by Raj on 04-06-2019.
 */

public class ChangePasswordInteratorImpl extends AbstractInteractor implements ChangePasswordInteractor {

    Callback mCallback;
    UserRepositoryImpl mRepository;
    String apiKey;
    int userId;
    String oldPassword;
    String newPassword;

    public ChangePasswordInteratorImpl(Executor threadExecutor,
                                       MainThread mainThread,
                                       Callback mCallback,
                                       UserRepositoryImpl mRepository,
                                       String apiKey,
                                       int userId,
                                       String oldPassword,
                                       String newPassword) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.apiKey = apiKey;
        this.userId = userId;
        this.oldPassword = oldPassword;
        this.newPassword = newPassword;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onChangePasswordFail(errorMsg);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onChangePasswordSuccess();
            }
        });
    }

    @Override
    public void run() {
        final ChangePasswordResponse changePasswordResponse = mRepository.changePassword(apiKey, userId, oldPassword, newPassword);

        if(changePasswordResponse == null){
            notifyError("Something went wrong");
        }else if(!changePasswordResponse.status){
            notifyError(changePasswordResponse.message);
        }else{
            postMessage();
        }

    }
}
