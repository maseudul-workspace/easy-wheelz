package in.net.webinfotech.easywheelz.domain.model.Other;

/**
 * Created by Raj on 29-06-2019.
 */

public class NotificationModel {

    public String title;
    public String message;

    public NotificationModel(String title, String message) {
        this.title = title;
        this.message = message;
    }
}
