package in.net.webinfotech.easywheelz.domain.interactors.base;

/**
 * Created by Raj on 26-03-2019.
 */

public interface Interactor {
    /**
     * This is the main method that starts an interactor. It will make sure that the interactor operation is done on a
     * background thread.
     */
    void execute();
}
