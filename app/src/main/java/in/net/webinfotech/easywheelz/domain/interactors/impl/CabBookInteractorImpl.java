package in.net.webinfotech.easywheelz.domain.interactors.impl;

import in.net.webinfotech.easywheelz.domain.executors.Executor;
import in.net.webinfotech.easywheelz.domain.executors.MainThread;
import in.net.webinfotech.easywheelz.domain.interactors.CabBookInteractor;
import in.net.webinfotech.easywheelz.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.easywheelz.domain.model.Other.BookingResponse;
import in.net.webinfotech.easywheelz.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 08-06-2019.
 */

public class CabBookInteractorImpl extends AbstractInteractor implements CabBookInteractor {

    Callback mCallback;
    OtherRepositoryImpl mRepository;
    String apiKey;
    int userId;
    int vendorId;
    int carTypeId;
    String pickUpDate;
    String pickUpTime;
    int reportingLocationId;
    String reportingAddress;
    int typeOfTravelId;
    String visitPlace;


    public CabBookInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, OtherRepositoryImpl mRepository, String apiKey, int userId, int vendorId, int carTypeId, String pickUpDate, String pickUpTime, int reportingLocationId, String reportingAddress, int typeOfTravelId, String visitPlace) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.apiKey = apiKey;
        this.userId = userId;
        this.vendorId = vendorId;
        this.carTypeId = carTypeId;
        this.pickUpDate = pickUpDate;
        this.pickUpTime = pickUpTime;
        this.reportingLocationId = reportingLocationId;
        this.reportingAddress = reportingAddress;
        this.typeOfTravelId = typeOfTravelId;
        this.visitPlace = visitPlace;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onBookingFail(errorMsg);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onBookingSuccess();
            }
        });
    }


    @Override
    public void run() {
        final BookingResponse bookingResponse = mRepository.bookCab(apiKey, userId, vendorId, carTypeId, pickUpDate, pickUpTime, reportingLocationId, reportingAddress, typeOfTravelId, visitPlace);
        if(bookingResponse == null){
            notifyError("Something went wrong");
        }else if(!bookingResponse.status){
            notifyError(bookingResponse.message);
        }else{
            postMessage();
        }
    }
}
