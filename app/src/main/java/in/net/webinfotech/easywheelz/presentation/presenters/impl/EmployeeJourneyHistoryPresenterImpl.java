package in.net.webinfotech.easywheelz.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import in.net.webinfotech.easywheelz.AndroidApplication;
import in.net.webinfotech.easywheelz.domain.executors.Executor;
import in.net.webinfotech.easywheelz.domain.executors.MainThread;
import in.net.webinfotech.easywheelz.domain.interactors.FetchEmployeeJourneyHistoryInteractor;
import in.net.webinfotech.easywheelz.domain.interactors.impl.FetchEmployeeJourneyHistoryInteractorImpl;
import in.net.webinfotech.easywheelz.domain.model.Order.JourneyDetails;
import in.net.webinfotech.easywheelz.domain.model.User.UserInfo;
import in.net.webinfotech.easywheelz.presentation.presenters.EmployeeJourneyHistoryPresenter;
import in.net.webinfotech.easywheelz.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.easywheelz.presentation.routers.JourneyHistoryRouter;
import in.net.webinfotech.easywheelz.presentation.ui.adapters.EmployeeJourneyHistoryAdapter;
import in.net.webinfotech.easywheelz.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 25-06-2019.
 */

public class EmployeeJourneyHistoryPresenterImpl extends AbstractPresenter implements EmployeeJourneyHistoryPresenter,
                                                                                FetchEmployeeJourneyHistoryInteractor.Callback,
                                                                                EmployeeJourneyHistoryAdapter.Callback{

    Context mContext;
    EmployeeJourneyHistoryPresenter.View mView;
    FetchEmployeeJourneyHistoryInteractorImpl mInteractor;
    AndroidApplication androidApplication;
    EmployeeJourneyHistoryAdapter adapter;
    JourneyHistoryRouter mRouter;
    JourneyDetails[] newJourneyDetails;

    public EmployeeJourneyHistoryPresenterImpl(Executor executor,
                                               MainThread mainThread,
                                               Context context,
                                               EmployeeJourneyHistoryPresenter.View view,
                                               JourneyHistoryRouter router
                                       ) {
        super(executor, mainThread);
        mContext = context;
        mView = view;
        mRouter = router;
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void fetchJourneyHistory(int pageNo, String type) {
        if(type.equals("refresh")){
            newJourneyDetails = null;
        }
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        mInteractor = new FetchEmployeeJourneyHistoryInteractorImpl(mExecutor, mMainThread, this, userInfo.api_key, userInfo.user_id, new OtherRepositoryImpl(), pageNo);
        mInteractor.execute();
    }

    @Override
    public void onFetchJourneyHistorySuccess(JourneyDetails[] journeyDetails, int totalPage) {

        JourneyDetails[] tempJourneyDetails;
        tempJourneyDetails = newJourneyDetails;
        try {
            int len1 = tempJourneyDetails.length;
            int len2 = journeyDetails.length;
            newJourneyDetails = new JourneyDetails[len1 + len2];
            System.arraycopy(tempJourneyDetails, 0, newJourneyDetails, 0, len1);
            System.arraycopy(journeyDetails, 0, newJourneyDetails, len1, len2);
            adapter.updateDataSet(newJourneyDetails);
            adapter.notifyDataSetChanged();
            mView.hidePaginationProgressLayout();
        }catch (NullPointerException e){
            newJourneyDetails = journeyDetails;
            adapter = new EmployeeJourneyHistoryAdapter(mContext, journeyDetails, this);
            mView.loadAdapter(adapter, totalPage);
            mView.hideLoader();
        }
        mView.stopRefreshing();
    }

    @Override
    public void onFetchJourneyHistoryFail(String errorMsg) {
        mView.hideLoader();
        mView.stopRefreshing();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT, true).show();
    }

    @Override
    public void goToJourneyDetails(int bookingId) {
        mRouter.goToJourneyDetails(bookingId);
    }
}
