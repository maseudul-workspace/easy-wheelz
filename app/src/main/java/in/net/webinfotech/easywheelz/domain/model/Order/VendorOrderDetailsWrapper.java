package in.net.webinfotech.easywheelz.domain.model.Order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 20-06-2019.
 */

public class VendorOrderDetailsWrapper {
    @SerializedName("status")
    @Expose
    public boolean status;

    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("code")
    @Expose
    public int code;

    @SerializedName("data")
    @Expose
    public VendorOrderDetails[] vendorOrderDetails;
}
