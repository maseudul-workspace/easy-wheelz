package in.net.webinfotech.easywheelz.domain.model.Other;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 07-06-2019.
 */

public class CarType {
    @SerializedName("type_id")
    @Expose
    public int type_id;

    @SerializedName("type_name")
    @Expose
    public String type_name;
}
