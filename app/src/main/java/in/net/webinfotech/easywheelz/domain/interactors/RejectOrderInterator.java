package in.net.webinfotech.easywheelz.domain.interactors;

/**
 * Created by Raj on 21-06-2019.
 */

public interface RejectOrderInterator {
    interface Callback{
        void onOrderRejectSuccess();
        void onOrderRejectFail(String errorMsg);
    }
}
