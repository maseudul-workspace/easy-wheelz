package in.net.webinfotech.easywheelz.presentation.ui.activities;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.easywheelz.R;
import in.net.webinfotech.easywheelz.domain.executors.impl.ThreadExecutor;
import in.net.webinfotech.easywheelz.presentation.presenters.EmployeeOrderHistoryPresenter;
import in.net.webinfotech.easywheelz.presentation.presenters.impl.EmployeeOrderHistoryPresenterImpl;
import in.net.webinfotech.easywheelz.presentation.routers.EmployeeOrderHistoryRouter;
import in.net.webinfotech.easywheelz.presentation.ui.adapters.EmployeeOrderHistoryAdapter;
import in.net.webinfotech.easywheelz.threading.MainThreadImpl;

public class EmployeeOrderHistoryActivity extends AppCompatActivity implements EmployeeOrderHistoryPresenter.View, EmployeeOrderHistoryRouter {

    @BindView(R.id.recycler_view_employee_order_history)
    RecyclerView recyclerView;
    @BindView(R.id.layout_loader)
    View layoutLoader;
    @BindView(R.id.pagination_progress_layout)
    View paginationProgressLayout;
    EmployeeOrderHistoryPresenterImpl mPresenter;
    Boolean isScrolling = false;
    Integer currentItems;
    Integer totalItems;
    Integer scrollOutItems;
    int pageNo = 1;
    int totalPage = 1;
    LinearLayoutManager layoutManager;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_order_history);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Order History");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initialisePresenter();
        showLoader();
        setSwipeRefreshLayout();
    }

    public void setSwipeRefreshLayout(){
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                pageNo = 1;
                totalPage = 1;
                mPresenter.getOrderHistory(pageNo, "refresh");
            }
        });
    }

    public void initialisePresenter(){
        mPresenter = new EmployeeOrderHistoryPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this,this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void loadData(EmployeeOrderHistoryAdapter adapter, final int totalPage) {
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
                {
                    isScrolling= true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItems = layoutManager.getChildCount();
                totalItems  = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstCompletelyVisibleItemPosition();
                if(!recyclerView.canScrollVertically(1))
                {
                    if(pageNo < totalPage) {
                        isScrolling = false;
                        pageNo = pageNo + 1;
                        showPaginationLoader();
                        mPresenter.getOrderHistory(pageNo, "");
                    }
                }
            }
        });
        recyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showLoader() {
        layoutLoader.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoader() {
        layoutLoader.setVisibility(View.GONE);
    }

    @Override
    public void showPaginationLoader() {
        paginationProgressLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hidePaginationLoader() {
        paginationProgressLayout.setVisibility(View.GONE);
    }

    @Override
    public void goToJourneyDetails(int booingId) {
        Intent journeyIntent = new Intent(this, EmployeeJourneyActivity.class);
        journeyIntent.putExtra("bookingId", booingId);
        startActivity(journeyIntent);
    }

    @Override
    public void goToOrderDetails(int bookingId) {
        Intent orderDetailsIntent = new Intent(this, EmployeeOrderDetailsActivity.class);
        orderDetailsIntent.putExtra("bookingId", bookingId);
        startActivity(orderDetailsIntent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        isScrolling = false;
        pageNo = 1;
        totalPage = 1;
        mPresenter.getOrderHistory(pageNo, "refresh");
    }

    @Override
    public void stopRefreshing() {
        if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing())
        {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

}
