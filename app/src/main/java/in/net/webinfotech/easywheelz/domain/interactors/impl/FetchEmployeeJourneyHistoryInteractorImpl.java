package in.net.webinfotech.easywheelz.domain.interactors.impl;

import in.net.webinfotech.easywheelz.domain.executors.Executor;
import in.net.webinfotech.easywheelz.domain.executors.MainThread;
import in.net.webinfotech.easywheelz.domain.interactors.FetchEmployeeJourneyHistoryInteractor;
import in.net.webinfotech.easywheelz.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.easywheelz.domain.model.Order.JourneyDetails;
import in.net.webinfotech.easywheelz.domain.model.Order.JourneyHistoryWrapper;
import in.net.webinfotech.easywheelz.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 25-06-2019.
 */

public class FetchEmployeeJourneyHistoryInteractorImpl extends AbstractInteractor implements FetchEmployeeJourneyHistoryInteractor {

    Callback mCallback;
    String apiKey;
    int userId;
    OtherRepositoryImpl mRepository;
    int page;

    public FetchEmployeeJourneyHistoryInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, String apiKey, int userId, OtherRepositoryImpl mRepository, int page) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.apiKey = apiKey;
        this.userId = userId;
        this.mRepository = mRepository;
        this.page = page;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onFetchJourneyHistoryFail(errorMsg);
            }
        });
    }

    private void postMessage(final JourneyDetails[] journeyDetails, final int totalPage){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onFetchJourneyHistorySuccess(journeyDetails, totalPage);
            }
        });
    }

    @Override
    public void run() {
        final JourneyHistoryWrapper journeyHistoryWrapper = mRepository.fetchEmployeeJourneyHistory(apiKey, userId, page);
        if(journeyHistoryWrapper == null){
            notifyError("Something went wrong");
        }else if(!journeyHistoryWrapper.status){
            notifyError(journeyHistoryWrapper.message);
        }else{
            postMessage(journeyHistoryWrapper.journeyDetails, journeyHistoryWrapper.total_page);
        }
    }
}
