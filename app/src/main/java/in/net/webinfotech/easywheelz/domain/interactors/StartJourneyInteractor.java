package in.net.webinfotech.easywheelz.domain.interactors;

/**
 * Created by Raj on 11-06-2019.
 */

public interface StartJourneyInteractor {
    interface Callback{
        void onJourneyStartedSuccess();
        void onJourStartedFail(String errorMsg);
    }
}
