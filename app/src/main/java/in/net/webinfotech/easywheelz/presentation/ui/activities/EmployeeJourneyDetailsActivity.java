package in.net.webinfotech.easywheelz.presentation.ui.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.easywheelz.R;
import in.net.webinfotech.easywheelz.domain.executors.impl.ThreadExecutor;
import in.net.webinfotech.easywheelz.domain.model.Order.EmployeeOrderDetails;
import in.net.webinfotech.easywheelz.presentation.presenters.EmployeeJourneyDetailsPresenter;
import in.net.webinfotech.easywheelz.presentation.presenters.impl.EmployeeJourneyDetailsPresenterImpl;
import in.net.webinfotech.easywheelz.threading.MainThreadImpl;

public class EmployeeJourneyDetailsActivity extends AppCompatActivity implements EmployeeJourneyDetailsPresenter.View{

    @BindView(R.id.txt_view_pick_up_date)
    TextView txtViewPickUpDate;
    @BindView(R.id.txt_view_pick_up_time)
    TextView txtViewPickUpTime;
    @BindView(R.id.txt_view_reporting_address)
    TextView txtViewReportingAddress;
    @BindView(R.id.txt_view_place_to_visit)
    TextView txtViewPlaceToVisit;
    @BindView(R.id.txt_view_car_type)
    TextView txtViewCabType;
    @BindView(R.id.txt_view_start_date)
    TextView txtViewStartDate;
    @BindView(R.id.txt_view_end_date)
    TextView txtViewEndDate;
    @BindView(R.id.txt_view_start_km)
    TextView txtViewStartKm;
    @BindView(R.id.txt_view_end_km)
    TextView txtViewEndKm;
    @BindView(R.id.txt_view_base_fare)
    TextView txtViewBaseFare;
    @BindView(R.id.txt_view_total_distance)
    TextView txtViewTotalDistance;
    @BindView(R.id.main_layout)
    View mainLayout;
    @BindView(R.id.layout_loader)
    View layoutLoader;
    @BindView(R.id.txt_view_cab_name)
    TextView txtViewCabName;
    @BindView(R.id.txt_view_cab_no)
    TextView txtViewCabNo;
    @BindView(R.id.txt_view_driver_phone_no)
    TextView txtViewDriverPhoneNo;
    @BindView(R.id.txt_view_driver_name)
    TextView txtViewDriverName;
    @BindView(R.id.layout_time_taken)
    View layoutTimeTaken;
    @BindView(R.id.layout_rate_per_km)
    View layoutRatePerKm;
    @BindView(R.id.layout_per_km_cost)
    View layoutPerKmCost;
    @BindView(R.id.layout_total_cost)
    View layoutTotalCost;
    @BindView(R.id.txt_view_time_taken)
    TextView txtViewTimeTaken;
    @BindView(R.id.txt_view_rate_per_km)
    TextView txtViewRatePerKm;
    @BindView(R.id.txt_view_per_km_cost)
    TextView txtViewPerKmCost;
    @BindView(R.id.txt_view_total_cost)
    TextView txtViewTotalCost;
    @BindView(R.id.txt_view_booking_id)
    TextView txtViewBookingId;
    @BindView(R.id.txt_view_booking_date)
    TextView txtViewBookingDate;
    @BindView(R.id.txt_view_type_of_travel)
    TextView txtViewTravelType;
    EmployeeJourneyDetailsPresenterImpl mPresenter;
    int bookingId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_journey_details);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Payment Details");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initialisePresenter();
        bookingId = getIntent().getIntExtra("bookingId", 0);
        mPresenter.fetchJourneyDetails(bookingId);
        showLoader();
    }

    public void initialisePresenter(){
        mPresenter = new EmployeeJourneyDetailsPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void loadData(EmployeeOrderDetails employeeOrderDetails) {
        txtViewBookingId.setText(Integer.toString(employeeOrderDetails.booking_id));
        try {
            txtViewBookingDate.setText(convertDate(employeeOrderDetails.bookingDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        mainLayout.setVisibility(View.VISIBLE);
        txtViewCabName.setText(employeeOrderDetails.cabName);
        txtViewCabNo.setText(employeeOrderDetails.cabNo);
        txtViewDriverName.setText(employeeOrderDetails.driverName);
        txtViewDriverPhoneNo.setText(employeeOrderDetails.driverMobile);
        txtViewBaseFare.setText("Rs. " + employeeOrderDetails.baseFare);
        txtViewCabType.setText(employeeOrderDetails.cabType);
        txtViewTravelType.setText(employeeOrderDetails.travelTypeName);
        try {
            txtViewEndDate.setText(convertDate(employeeOrderDetails.journeyEndDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        txtViewEndKm.setText(employeeOrderDetails.journeyEndKm + " km");
        txtViewPickUpDate.setText(employeeOrderDetails.pickupDate);
        txtViewPickUpTime.setText(employeeOrderDetails.pickUpTime);
        txtViewPlaceToVisit.setText(employeeOrderDetails.visitPlace);
        txtViewReportingAddress.setText(employeeOrderDetails.reportingAddress);
        try {
            txtViewStartDate.setText(convertDate(employeeOrderDetails.journeyStartDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        txtViewStartKm.setText(employeeOrderDetails.journeyStartKm + " km");
        txtViewTotalDistance.setText(employeeOrderDetails.totalDistanceKm + " km");

        if(employeeOrderDetails.timeTaken == null){
            layoutTimeTaken.setVisibility(View.GONE);
        }else{
            txtViewTimeTaken.setText(employeeOrderDetails.timeTaken);
            layoutTimeTaken.setVisibility(View.VISIBLE);
        }

        if(employeeOrderDetails.ratePerKm == null){
            layoutRatePerKm.setVisibility(View.GONE);
        }else{
            layoutRatePerKm.setVisibility(View.VISIBLE);
            txtViewRatePerKm.setText(Double.toString(employeeOrderDetails.ratePerKm));
        }

        if(employeeOrderDetails.perKmCost == null){
            layoutPerKmCost.setVisibility(View.GONE);
        }else{
            layoutPerKmCost.setVisibility(View.VISIBLE);
            txtViewPerKmCost.setText("Rs. " + Double.toString(employeeOrderDetails.perKmCost));
        }

        if(employeeOrderDetails.totalCost == null){
            layoutTotalCost.setVisibility(View.GONE);
        }else{
            layoutTotalCost.setVisibility(View.VISIBLE);
            txtViewTotalCost.setText("Rs. " + employeeOrderDetails.totalCost);
        }

    }

    @Override
    public void showLoader() {
        layoutLoader.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoader() {
        layoutLoader.setVisibility(View.GONE);
    }

    public String convertDate(String dateString) throws ParseException {
        DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.ENGLISH);
        DateFormat targetFormat = new SimpleDateFormat("MMMM dd, yyyy hh:mm a");
        Date date = originalFormat.parse(dateString);
        String formattedDate = targetFormat.format(date);
        return formattedDate;
    }

}
