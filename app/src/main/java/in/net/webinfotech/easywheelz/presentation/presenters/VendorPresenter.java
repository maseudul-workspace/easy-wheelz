package in.net.webinfotech.easywheelz.presentation.presenters;

import in.net.webinfotech.easywheelz.presentation.presenters.base.BasePresenter;
import in.net.webinfotech.easywheelz.presentation.ui.adapters.VendorCabOrderHistoryAdapter;

/**
 * Created by Raj on 20-06-2019.
 */

public interface VendorPresenter extends BasePresenter{
    void fetchOrders(int page, String type);
    void setFirebaseToken(String firebaseToken);
    interface View{
        void loadOrdersAdapter(VendorCabOrderHistoryAdapter adapter, int totalPage);
        void showLoader();
        void hideLoader();
        void showPaginationProgressLayout();
        void hidePaginationProgressLayout();
        void stopRefreshing();
    }
}
