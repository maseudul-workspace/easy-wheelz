package in.net.webinfotech.easywheelz.services;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import in.net.webinfotech.easywheelz.AndroidApplication;
import in.net.webinfotech.easywheelz.domain.model.Other.NotificationModel;
import in.net.webinfotech.easywheelz.presentation.ui.activities.EmployeeOrderHistoryActivity;
import in.net.webinfotech.easywheelz.presentation.ui.activities.VendorCabOrderDetailsActivity;
import in.net.webinfotech.easywheelz.util.NotificationUtils;

/**
 * Created by Raj on 29-06-2019.
 */

public class ITCFirebaseMessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        String message = "";
        try{
            message = remoteMessage.getData().get("message");
        }catch (NullPointerException e){
            message = "From ITC Application";
        }

        String title;
        try{
            title = remoteMessage.getData().get("title");
        }catch (NullPointerException e){
            title = "Notification Title";
        }

        String bookingIdString = remoteMessage.getData().get("booking_id");
        String apiKey = remoteMessage.getData().get("api_key");
        int bookingIdInt = Integer.parseInt(bookingIdString);

        String userType = remoteMessage.getData().get("user_type");
        int userTypeInt = Integer.parseInt(userType);
        Intent resultIntent;

        if(userTypeInt == 1){
            resultIntent = new Intent(this, EmployeeOrderHistoryActivity.class);
        }else{
            resultIntent = new Intent(this, VendorCabOrderDetailsActivity.class);
            resultIntent.putExtra("bookingId", bookingIdInt);
        }
        NotificationModel notificationModel = new NotificationModel(title, message);
        NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        if(androidApplication.getUserInfo(this) != null && androidApplication.getUserInfo(this).api_key.equals(apiKey)){
            notificationUtils.displayNotification(notificationModel, resultIntent);
        }
    }

    @Override
    public void handleIntent(Intent intent) {
        super.handleIntent(intent);
    }

    @Override
    public void onTaskRemoved(Intent rootIntent){
        Intent restartServiceIntent = new Intent(getApplicationContext(), this.getClass());
        restartServiceIntent.setPackage(getPackageName());

        PendingIntent restartServicePendingIntent = PendingIntent.getService(getApplicationContext(), 1, restartServiceIntent, PendingIntent.FLAG_ONE_SHOT);
        AlarmManager alarmService = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        alarmService.set(
                AlarmManager.ELAPSED_REALTIME,
                SystemClock.elapsedRealtime() + 1000,
                restartServicePendingIntent);

        super.onTaskRemoved(rootIntent);
    }



}
