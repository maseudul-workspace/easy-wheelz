package in.net.webinfotech.easywheelz.presentation.presenters;

import in.net.webinfotech.easywheelz.presentation.presenters.base.BasePresenter;

/**
 * Created by Raj on 03-06-2019.
 */

public interface LoginPresenter extends BasePresenter {
    void loginCheck(String email, String password, int userType);
    interface View{
        void showLoader();
        void hideLoader();
    }
}
