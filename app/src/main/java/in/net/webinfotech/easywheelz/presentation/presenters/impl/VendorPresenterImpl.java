package in.net.webinfotech.easywheelz.presentation.presenters.impl;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import in.net.webinfotech.easywheelz.AndroidApplication;
import in.net.webinfotech.easywheelz.domain.executors.Executor;
import in.net.webinfotech.easywheelz.domain.executors.MainThread;
import in.net.webinfotech.easywheelz.domain.interactors.GetVendorOrderHistoryInteractor;
import in.net.webinfotech.easywheelz.domain.interactors.SetFirebaseTokenInteractor;
import in.net.webinfotech.easywheelz.domain.interactors.impl.GetVendorOrderHistoryInteractorImpl;
import in.net.webinfotech.easywheelz.domain.interactors.impl.SetFirebaseTokenInteractorImpl;
import in.net.webinfotech.easywheelz.domain.model.Order.VendorOrderHistory;
import in.net.webinfotech.easywheelz.domain.model.User.UserInfo;
import in.net.webinfotech.easywheelz.presentation.presenters.VendorPresenter;
import in.net.webinfotech.easywheelz.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.easywheelz.presentation.routers.VendorsRouter;
import in.net.webinfotech.easywheelz.presentation.ui.adapters.VendorCabOrderHistoryAdapter;
import in.net.webinfotech.easywheelz.repository.impl.OtherRepositoryImpl;
import in.net.webinfotech.easywheelz.repository.impl.UserRepositoryImpl;

/**
 * Created by Raj on 20-06-2019.
 */

public class VendorPresenterImpl extends AbstractPresenter implements VendorPresenter, GetVendorOrderHistoryInteractor.Callback,
                                                                        VendorCabOrderHistoryAdapter.Callback, SetFirebaseTokenInteractor.Callback {

    Context mConetxt;
    VendorPresenter.View mView;
    AndroidApplication androidApplication;
    GetVendorOrderHistoryInteractorImpl mInetractor;
    VendorsRouter mRouter;
    SetFirebaseTokenInteractorImpl setFirebaseTokenInteractor;
    VendorOrderHistory[] newVendorOrderHistories;
    VendorCabOrderHistoryAdapter adapter;

    public VendorPresenterImpl(Executor executor, MainThread mainThread, Context mConetxt, View mView, VendorsRouter router) {
        super(executor, mainThread);
        this.mConetxt = mConetxt;
        this.mView = mView;
        mRouter = router;
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void fetchOrders(int page, String type) {
        if(type.equals("refresh")){
            newVendorOrderHistories = null;
        }
        androidApplication = (AndroidApplication) mConetxt.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mConetxt);
        mInetractor = new GetVendorOrderHistoryInteractorImpl(mExecutor, mMainThread, this, userInfo.api_key, userInfo.user_id, page, new OtherRepositoryImpl());
        mInetractor.execute();
    }

    @Override
    public void setFirebaseToken(String firebaseToken) {
        androidApplication = (AndroidApplication) mConetxt.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mConetxt);
        setFirebaseTokenInteractor = new SetFirebaseTokenInteractorImpl(mExecutor, mMainThread, this, new UserRepositoryImpl(), userInfo.api_key, userInfo.user_id, firebaseToken);
        setFirebaseTokenInteractor.execute();
    }

    @Override
    public void onGettingOrdersSuccess(VendorOrderHistory[] vendorOrderHistories, int totalPage) {

        VendorOrderHistory[] tempVendorOrderHistories;
        tempVendorOrderHistories = newVendorOrderHistories;
        try {
            int len1 = tempVendorOrderHistories.length;
            int len2 = vendorOrderHistories.length;
            newVendorOrderHistories = new VendorOrderHistory[len1 + len2];
            System.arraycopy(tempVendorOrderHistories, 0, newVendorOrderHistories, 0, len1);
            System.arraycopy(vendorOrderHistories, 0, newVendorOrderHistories, len1, len2);
            adapter.updateDataset(newVendorOrderHistories);
            adapter.notifyDataSetChanged();
            mView.hidePaginationProgressLayout();
        }catch (NullPointerException e){
            newVendorOrderHistories = vendorOrderHistories;
            adapter = new VendorCabOrderHistoryAdapter(mConetxt, this, vendorOrderHistories);
            mView.loadOrdersAdapter(adapter, totalPage);
            mView.hideLoader();
        }
        mView.stopRefreshing();
    }

    @Override
    public void onGettingOrdersFail(String errorMsg) {
        mView.hideLoader();
        Toasty.error(mConetxt, errorMsg, Toast.LENGTH_SHORT, true);
        mView.stopRefreshing();
    }

    @Override
    public void goToOrderDetails(int bookingId, int bookingStatus) {
        mRouter.goToOrderDetails(bookingId, bookingStatus);
    }

    @Override
    public void onSetFirebaseTokenSuccess() {
    }

    @Override
    public void onSetFirebaseTokenFail() {
    }
}
