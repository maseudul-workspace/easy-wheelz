package in.net.webinfotech.easywheelz.domain.interactors.impl;

import in.net.webinfotech.easywheelz.domain.executors.Executor;
import in.net.webinfotech.easywheelz.domain.executors.MainThread;
import in.net.webinfotech.easywheelz.domain.interactors.GetVendorOrderDetailsInteractor;
import in.net.webinfotech.easywheelz.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.easywheelz.domain.model.Order.VendorOrderDetails;
import in.net.webinfotech.easywheelz.domain.model.Order.VendorOrderDetailsWrapper;
import in.net.webinfotech.easywheelz.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 20-06-2019.
 */

public class GetVendorOrderDetailsInteractorImpl extends AbstractInteractor implements GetVendorOrderDetailsInteractor {

    Callback mCallback;
    OtherRepositoryImpl mRepository;
    String apiKey;
    int userId;
    int bookingId;

    public GetVendorOrderDetailsInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, OtherRepositoryImpl mRepository, String apiKey, int userId, int bookingId) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.apiKey = apiKey;
        this.userId = userId;
        this.bookingId = bookingId;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingOrderDetailsFail(errorMsg);
            }
        });
    }

    private void postMessage(final VendorOrderDetails vendorOrderDetails){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingOrderDetailsSuccess(vendorOrderDetails);
            }
        });
    }

    @Override
    public void run() {
        final VendorOrderDetailsWrapper vendorOrderDetailsWrapper = mRepository.getVendorOrderDetails(apiKey, userId, bookingId);
        if(vendorOrderDetailsWrapper == null){
            notifyError("Something went wrong");
        }else if(!vendorOrderDetailsWrapper.status){
            notifyError(vendorOrderDetailsWrapper.message);
        }else{
            postMessage(vendorOrderDetailsWrapper.vendorOrderDetails[0]);
        }
    }
}
