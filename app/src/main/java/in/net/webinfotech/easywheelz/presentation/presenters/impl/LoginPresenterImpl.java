package in.net.webinfotech.easywheelz.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import in.net.webinfotech.easywheelz.AndroidApplication;
import in.net.webinfotech.easywheelz.domain.executors.Executor;
import in.net.webinfotech.easywheelz.domain.executors.MainThread;
import in.net.webinfotech.easywheelz.domain.interactors.CheckUserLoginInteractor;
import in.net.webinfotech.easywheelz.domain.interactors.impl.CheckUserLoginInteractorImpl;
import in.net.webinfotech.easywheelz.domain.model.User.UserInfo;
import in.net.webinfotech.easywheelz.presentation.presenters.LoginPresenter;
import in.net.webinfotech.easywheelz.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.easywheelz.presentation.routers.LoginRouter;
import in.net.webinfotech.easywheelz.repository.impl.UserRepositoryImpl;

/**
 * Created by Raj on 03-06-2019.
 */

public class LoginPresenterImpl extends AbstractPresenter implements LoginPresenter, CheckUserLoginInteractor.Callback {

    Context mContext;
    LoginPresenter.View mView;
    CheckUserLoginInteractorImpl mInteractor;
    int userType;
    AndroidApplication androidApplication;
    LoginRouter mRouter;

    public LoginPresenterImpl(Executor executor, MainThread mainThread, Context context, LoginPresenter.View view, LoginRouter router) {
        super(executor, mainThread);
        mContext = context;
        mView = view;
        this.mRouter = router;
    }

    @Override
    public void loginCheck(String email, String password, int userType) {
        this.userType = userType;
        mInteractor = new CheckUserLoginInteractorImpl(mExecutor, mMainThread, this, new UserRepositoryImpl(), email, password, userType);
        mInteractor.execute();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void onLoginSuccess(UserInfo userInfo) {
        mView.hideLoader();
        Toasty.success(mContext, "Login Successful", Toast.LENGTH_SHORT, true).show();
        userInfo.userType = userType;
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setUserInfo(mContext, userInfo);
        if(userType == 1){
            mRouter.goToEmployeeActivity();
        }else{
            mRouter.goToVendorActivity();
        }
    }

    @Override
    public void onLoginFail(String errorMsg) {
        mView.hideLoader();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT, true).show();
    }
}
