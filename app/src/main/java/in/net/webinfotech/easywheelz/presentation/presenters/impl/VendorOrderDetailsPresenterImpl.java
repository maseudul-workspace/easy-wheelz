package in.net.webinfotech.easywheelz.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import in.net.webinfotech.easywheelz.AndroidApplication;
import in.net.webinfotech.easywheelz.domain.executors.Executor;
import in.net.webinfotech.easywheelz.domain.executors.MainThread;
import in.net.webinfotech.easywheelz.domain.interactors.AcceptOrderInteractor;
import in.net.webinfotech.easywheelz.domain.interactors.FetchCabInteractor;
import in.net.webinfotech.easywheelz.domain.interactors.GetVendorOrderDetailsInteractor;
import in.net.webinfotech.easywheelz.domain.interactors.RejectOrderInterator;
import in.net.webinfotech.easywheelz.domain.interactors.impl.AcceptOrderInteractorImpl;
import in.net.webinfotech.easywheelz.domain.interactors.impl.FetchCabInteractorImpl;
import in.net.webinfotech.easywheelz.domain.interactors.impl.GetVendorOrderDetailsInteractorImpl;
import in.net.webinfotech.easywheelz.domain.interactors.impl.RejectOrderInteractorImpl;
import in.net.webinfotech.easywheelz.domain.model.Order.VendorOrderDetails;
import in.net.webinfotech.easywheelz.domain.model.Other.Cab;
import in.net.webinfotech.easywheelz.domain.model.User.UserInfo;
import in.net.webinfotech.easywheelz.presentation.presenters.VendorOrderDetailsPresenter;
import in.net.webinfotech.easywheelz.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.easywheelz.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 10-06-2019.
 */

public class VendorOrderDetailsPresenterImpl extends AbstractPresenter implements VendorOrderDetailsPresenter,
                                                                                    FetchCabInteractor.Callback,
                                                                                    GetVendorOrderDetailsInteractor.Callback,
                                                                                    AcceptOrderInteractor.Callback,
                                                                                    RejectOrderInterator.Callback
                                                                                    {
    Context mContext;
    FetchCabInteractorImpl cabInteractor;
    VendorOrderDetailsPresenter.View mView;
    GetVendorOrderDetailsInteractorImpl getVendorOrderDetailsInteractor;
    AndroidApplication androidApplication;
    AcceptOrderInteractorImpl acceptOrderInteractor;
    RejectOrderInteractorImpl rejectOrderInteractor;

    public VendorOrderDetailsPresenterImpl(Executor executor,
                                           MainThread mainThread,
                                           Context context,
                                           VendorOrderDetailsPresenter.View view
                                           ) {
        super(executor, mainThread);
        mContext = context;
        mView = view;
    }

    @Override
    public void fetchCabs(int cabTypeId) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        cabInteractor = new FetchCabInteractorImpl(mExecutor, mMainThread, this, new OtherRepositoryImpl(), userInfo.api_key, userInfo.user_id, cabTypeId);
        cabInteractor.execute();
    }

    @Override
    public void getOrderDetails(int bookingId) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        getVendorOrderDetailsInteractor = new GetVendorOrderDetailsInteractorImpl(mExecutor, mMainThread, this, new OtherRepositoryImpl(), userInfo.api_key, userInfo.user_id, bookingId);
        getVendorOrderDetailsInteractor.execute();
    }

    @Override
    public void acceptOrder(int bookingId, int cabId, String driverName, String bookingMobile, String cabNo) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        acceptOrderInteractor = new AcceptOrderInteractorImpl(mExecutor, mMainThread, this, new OtherRepositoryImpl(), userInfo.api_key, userInfo.user_id, bookingId, cabId, driverName, bookingMobile, cabNo);
        acceptOrderInteractor.execute();
    }

    @Override
    public void rejectOrder(int bookingId, String comment) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        rejectOrderInteractor = new RejectOrderInteractorImpl(mExecutor, mMainThread, this, new OtherRepositoryImpl(), userInfo.api_key, userInfo.user_id, bookingId, comment);
        rejectOrderInteractor.execute();
    }

    @Override
    public void onCabFetchSuccess(Cab[] cabs) {
        mView.setCabs(cabs);
    }

    @Override
    public void onCabFetchFail(String errorMsg) {
        mView.onCabGettingFailed();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void onGettingOrderDetailsSuccess(VendorOrderDetails vendorOrderDetails) {
        mView.loadData(vendorOrderDetails);
        mView.hideLoader();
    }

    @Override
    public void onGettingOrderDetailsFail(String errorMsg) {
        mView.hideLoader();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT, true).show();
    }

    @Override
    public void onOrderAcceptSuccess() {
        Toasty.success(mContext, "Order accepted successfully", Toast.LENGTH_SHORT, true).show();
        mView.finishActivity();
    }

    @Override
    public void onOrderAcceptFail(String errorMsg) {
        mView.hideAcceptDialogLoader();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT, true).show();
    }

    @Override
    public void onOrderRejectSuccess() {
        Toasty.success(mContext, "Order rejected", Toast.LENGTH_SHORT, true).show();
        mView.finishActivity();
    }

    @Override
    public void onOrderRejectFail(String errorMsg) {
        mView.hideRejectDialogLoader();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT, true).show();
    }
                                                                                    }
