package in.net.webinfotech.easywheelz.presentation.ui.activities;

import android.animation.ObjectAnimator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import in.net.webinfotech.easywheelz.R;
import in.net.webinfotech.easywheelz.domain.executors.impl.ThreadExecutor;
import in.net.webinfotech.easywheelz.domain.model.Order.EmployeeOrderDetails;
import in.net.webinfotech.easywheelz.presentation.presenters.EmployeeJourneyPresenter;
import in.net.webinfotech.easywheelz.presentation.presenters.impl.EmployeeJourneyPresenterImpl;
import in.net.webinfotech.easywheelz.presentation.ui.dialogs.JourneyFinishConfirmationDialog;
import in.net.webinfotech.easywheelz.threading.MainThreadImpl;

public class EmployeeJourneyActivity extends AppCompatActivity implements EmployeeJourneyPresenter.View, JourneyFinishConfirmationDialog.Callback{

    @BindView(R.id.seekbar_journey)
    SeekBar journeySeekbar;
    @BindView(R.id.card_view_start_km)
    CardView cardViewStartKm;
    @BindView(R.id.card_view_end_km)
    CardView cardViewEndKm;
    @BindView(R.id.relative_layout_end_km)
    RelativeLayout relativeLayoutEndKm;
    @BindView(R.id.relative_layout_start_km)
    RelativeLayout relativeLayoutStartKm;
    @BindView(R.id.edit_text_start_km)
    EditText editTextStartKm;
    @BindView(R.id.edit_text_end_km)
    EditText editTextEndKm;
    @BindView(R.id.txt_view_start_km)
    TextView txtViewStartKm;
    @BindView(R.id.txt_view_end_km)
    TextView txtViewEndKm;
    @BindView(R.id.btn_start_journey)
    Button btnStartJourney;
    @BindView(R.id.btn_end_journey)
    Button btnEndJourney;
    @BindView(R.id.txt_view_journey_status)
    TextView txtViewourneyStatus;
    @BindView(R.id.main_layout)
    View mainLayout;
    @BindView(R.id.layout_loader)
    View loaderLayout;
    @BindView(R.id.layout_loader_main)
    View loaderLayoutMain;
    EmployeeJourneyPresenterImpl mPresenter;
    int bookingId;
    long startKm;
    JourneyFinishConfirmationDialog confirmationDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_journey);
        ButterKnife.bind(this);
        bookingId = getIntent().getIntExtra("bookingId", 0);
        getSupportActionBar().setTitle("Journey Details");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initialisePresenter();
        initialiseDialog();
        mPresenter.getJourneyDetails(bookingId);
        disableSeekbarTouch();
    }

    public void initialisePresenter(){
        mPresenter = new EmployeeJourneyPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void initialiseDialog(){
        confirmationDialog = new JourneyFinishConfirmationDialog(this, this, this);
    }

    public void disableSeekbarTouch(){
        journeySeekbar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
    }

    public void updateSeekbar(int initialValue, int finalValue){
        final ObjectAnimator animation = ObjectAnimator.ofInt(journeySeekbar, "progress", initialValue, finalValue);
        animation.setDuration(2000);
        animation.setInterpolator(new DecelerateInterpolator());
        animation.start();
        journeySeekbar.clearAnimation();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void loadData(final EmployeeOrderDetails employeeOrderDetails) {
        mainLayout.setVisibility(View.VISIBLE);
        switch (employeeOrderDetails.journeyStatus){
            case 1:
                cardViewStartKm.setVisibility(View.GONE);
                relativeLayoutStartKm.setVisibility(View.VISIBLE);
                relativeLayoutStartKm.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        relativeLayoutStartKm.setVisibility(View.GONE);
                        cardViewStartKm.setVisibility(View.VISIBLE);
                        editTextStartKm.requestFocus();
                    }
                });
                cardViewEndKm.setVisibility(View.GONE);
                relativeLayoutEndKm.setVisibility(View.GONE);
                btnEndJourney.setVisibility(View.GONE);
                btnStartJourney.setVisibility(View.VISIBLE);
                txtViewourneyStatus.setText("Not Started");
                break;
            case 2:
                startKm = employeeOrderDetails.journeyStartKm;
                relativeLayoutStartKm.setVisibility(View.VISIBLE);
                txtViewStartKm.setText(employeeOrderDetails.journeyStartKm + " km");
                relativeLayoutStartKm.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        relativeLayoutStartKm.setVisibility(View.GONE);
                        cardViewStartKm.setVisibility(View.VISIBLE);
                        btnStartJourney.setText("Update");
                        btnStartJourney.setVisibility(View.VISIBLE);
                        editTextStartKm.setText(Long.toString(employeeOrderDetails.journeyStartKm));
                        editTextStartKm.requestFocus();
                    }
                });
                cardViewStartKm.setVisibility(View.GONE);
                cardViewEndKm.setVisibility(View.GONE);
                relativeLayoutEndKm.setVisibility(View.VISIBLE);
                relativeLayoutEndKm.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        relativeLayoutEndKm.setVisibility(View.GONE);
                        cardViewEndKm.setVisibility(View.VISIBLE);
                        editTextEndKm.requestFocus();
                    }
                });
                btnEndJourney.setVisibility(View.VISIBLE);
                btnStartJourney.setVisibility(View.GONE);
                txtViewourneyStatus.setText("Ongoing");
                updateSeekbar(0, 50);
                break;
            case 3:
                txtViewourneyStatus.setText("Completed");
                relativeLayoutStartKm.setVisibility(View.VISIBLE);
                relativeLayoutEndKm.setVisibility(View.VISIBLE);
                cardViewStartKm.setVisibility(View.GONE);
                txtViewStartKm.setText(employeeOrderDetails.journeyStartKm + " km");
                cardViewEndKm.setVisibility(View.GONE);
                txtViewEndKm.setText(employeeOrderDetails.journeyEndKm + " km");
                btnStartJourney.setVisibility(View.GONE);
                btnEndJourney.setVisibility(View.GONE);
                updateSeekbar(50, 100);
                break;
        }
    }

    @Override
    public void showLoader() {
        loaderLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoader() {
        loaderLayout.setVisibility(View.GONE);
    }

    @Override
    public void showMainLoader() {
       loaderLayoutMain.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideMainLoader() {
        loaderLayoutMain.setVisibility(View.GONE);
    }

    @OnClick(R.id.btn_start_journey) void onStartButtonClicked(){
        if(editTextStartKm.getText().toString().trim().isEmpty()){
            Toasty.warning(this, "Please enter starting km", Toast.LENGTH_SHORT, true).show();
        }else{
            mPresenter.startJourney(bookingId, editTextStartKm.getText().toString());
            showLoader();
        }
    }

    @OnClick(R.id.btn_end_journey) void onJourneyEndClicked(){
       if(editTextEndKm.getText().toString().trim().isEmpty()){
           Toasty.warning(this, "Please enter starting km", Toast.LENGTH_SHORT, true).show();
       }else if(Long.parseLong(editTextEndKm.getText().toString()) < startKm){
           Toasty.warning(this, "Ending km should be greater than starting km", Toast.LENGTH_SHORT, true).show();
       }else{
           long endingKm = Long.parseLong(editTextEndKm.getText().toString());
           String totalKm = Long.toString(endingKm - startKm);
           confirmationDialog.showDialog(Long.toString(startKm), Long.toString(endingKm), totalKm);
       }
    }


    @Override
    public void onConfirmationClicked() {
        mPresenter.endJourney(bookingId, editTextEndKm.getText().toString());
        showLoader();
    }
}
