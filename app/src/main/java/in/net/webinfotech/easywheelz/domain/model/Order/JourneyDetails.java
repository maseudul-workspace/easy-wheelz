package in.net.webinfotech.easywheelz.domain.model.Order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 25-06-2019.
 */

public class JourneyDetails {
    @SerializedName("booking_id")
    @Expose
    public int booking_id;

    @SerializedName("reporting_address")
    @Expose
    public String reportingAddress;

    @SerializedName("visit_place")
    @Expose
    public String visitPlace;

    @SerializedName("pickup_date")
    @Expose
    public String pickupDate;

    @SerializedName("pickup_time")
    @Expose
    public String pickUpTime;

    @SerializedName("booking_date")
    @Expose
    public String bookingDate;

    @SerializedName("cab_type")
    @Expose
    public String cabType;

    @SerializedName("journey_start_date")
    @Expose
    public String journeyStartDate;

    @SerializedName("journey_start_km")
    @Expose
    public String journeyStartKm;

    @SerializedName("journey_end_date")
    @Expose
    public String journeyEndDate;

    @SerializedName("journey_end_km")
    @Expose
    public String journeyEndKm;

    @SerializedName("total_distance_km")
    @Expose
    public String totalDistanceKm;

    @SerializedName("journey_status")
    @Expose
    public int journeyStatus;

    @SerializedName("base_fare")
    @Expose
    public String baseFare;

    @SerializedName("bookingStatus")
    @Expose
    public int bookingStatus;

    @SerializedName("employee_name")
    @Expose
    public String employee_name;

    @SerializedName("employee_email")
    @Expose
    public String employee_email;

}
