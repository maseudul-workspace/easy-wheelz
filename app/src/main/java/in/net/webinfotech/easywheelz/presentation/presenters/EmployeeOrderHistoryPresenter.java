package in.net.webinfotech.easywheelz.presentation.presenters;

import in.net.webinfotech.easywheelz.presentation.presenters.base.BasePresenter;
import in.net.webinfotech.easywheelz.presentation.ui.adapters.EmployeeOrderHistoryAdapter;

/**
 * Created by Raj on 11-06-2019.
 */

public interface EmployeeOrderHistoryPresenter extends BasePresenter{
    void getOrderHistory(int page, String type);
    interface View{
        void loadData(EmployeeOrderHistoryAdapter adapter, int totalPage);
        void showLoader();
        void hideLoader();
        void showPaginationLoader();
        void hidePaginationLoader();
        void stopRefreshing();
    }
}
