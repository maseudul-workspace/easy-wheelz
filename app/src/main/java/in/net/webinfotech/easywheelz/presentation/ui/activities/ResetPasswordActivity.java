package in.net.webinfotech.easywheelz.presentation.ui.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import in.net.webinfotech.easywheelz.AndroidApplication;
import in.net.webinfotech.easywheelz.R;
import in.net.webinfotech.easywheelz.domain.executors.impl.ThreadExecutor;
import in.net.webinfotech.easywheelz.domain.model.User.UserInfo;
import in.net.webinfotech.easywheelz.presentation.presenters.ResetPasswordPresenter;
import in.net.webinfotech.easywheelz.presentation.presenters.impl.ResetPasswordPresenterImpl;
import in.net.webinfotech.easywheelz.threading.MainThreadImpl;

public class ResetPasswordActivity extends AppCompatActivity implements ResetPasswordPresenter.View{

    @BindView(R.id.edit_text_current_password)
    EditText editTextCurrentPassword;
    @BindView(R.id.edit_text_new_password)
    EditText editTextNewPassword;
    @BindView(R.id.edit_text_repeat_password)
    EditText editTextRepeatPassword;
    @BindView(R.id.layout_loader)
    View loaderLayout;
    AndroidApplication androidApplication;
    ResetPasswordPresenterImpl mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        ButterKnife.bind(this);
        initialisePresenter();
    }

    public void initialisePresenter(){
        mPresenter = new ResetPasswordPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @OnClick(R.id.btn_change_password)void changePassword(){
        if(editTextCurrentPassword.getText().toString().trim().isEmpty() ||
                editTextNewPassword.getText().toString().trim().isEmpty() ||
                editTextRepeatPassword.getText().toString().trim().isEmpty()){
            Toasty.warning(this, "Some fields are missing", Toast.LENGTH_SHORT, true).show();
        }else if(!editTextNewPassword.getText().toString().equals(editTextRepeatPassword.getText().toString())){
            Toasty.warning(this, "Passwords mismatch", Toast.LENGTH_SHORT, true).show();
        }else{
            androidApplication = (AndroidApplication) getApplicationContext();
            UserInfo userInfo = androidApplication.getUserInfo(this);
            mPresenter.changePassword(userInfo.api_key, userInfo.user_id, editTextCurrentPassword.getText().toString(), editTextNewPassword.getText().toString());
            showLoader();
        }
    }

    @Override
    public void showLoader() {
        loaderLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoader() {
        loaderLayout.setVisibility(View.GONE);
    }
}
