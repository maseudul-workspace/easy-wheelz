package in.net.webinfotech.easywheelz.domain.interactors;

/**
 * Created by Raj on 21-06-2019.
 */

public interface AcceptOrderInteractor {
    interface Callback{
        void onOrderAcceptSuccess();
        void onOrderAcceptFail(String errorMsg);
    }
}
