package in.net.webinfotech.easywheelz.presentation.ui.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.easywheelz.R;
import in.net.webinfotech.easywheelz.domain.executors.impl.ThreadExecutor;
import in.net.webinfotech.easywheelz.domain.model.Order.VendorOrderDetails;
import in.net.webinfotech.easywheelz.presentation.presenters.VendorJourneyDetailsPresenter;
import in.net.webinfotech.easywheelz.presentation.presenters.impl.VendorJourneyDetailsPresenterImpl;
import in.net.webinfotech.easywheelz.threading.MainThreadImpl;

public class VendorJourneyDetailsActivity extends AppCompatActivity implements VendorJourneyDetailsPresenter.View {

    @BindView(R.id.txt_view_pick_up_date)
    TextView txtViewPickUpDate;
    @BindView(R.id.txt_view_pick_up_time)
    TextView txtViewPickUpTime;
    @BindView(R.id.txt_view_reporting_address)
    TextView txtViewReportingAddress;
    @BindView(R.id.txt_view_place_to_visit)
    TextView txtViewPlaceToVisit;
    @BindView(R.id.txt_view_car_type)
    TextView txtViewCabType;
    @BindView(R.id.txt_view_start_date)
    TextView txtViewStartDate;
    @BindView(R.id.txt_view_end_date)
    TextView txtViewEndDate;
    @BindView(R.id.txt_view_start_km)
    TextView txtViewStartKm;
    @BindView(R.id.txt_view_end_km)
    TextView txtViewEndKm;
    @BindView(R.id.txt_view_base_fare)
    TextView txtViewBaseFare;
    @BindView(R.id.txt_view_total_distance)
    TextView txtViewTotalDistance;
    @BindView(R.id.main_layout)
    View mainLayout;
    @BindView(R.id.layout_loader)
    View layoutLoader;
    @BindView(R.id.txt_view_employee_name)
    TextView txtViewEmployeeName;
    @BindView(R.id.txt_view_employee_email)
    TextView txtViewEmployeeEmail;
    @BindView(R.id.txt_view_cab_name)
    TextView txtViewCabName;
    @BindView(R.id.txt_view_cab_no)
    TextView txtViewCabNo;
    @BindView(R.id.txt_view_driver_phone_no)
    TextView txtViewDriverPhoneNo;
    @BindView(R.id.txt_view_driver_name)
    TextView txtViewDriverName;
    @BindView(R.id.layout_journey_start_date)
    View layoutJourneyStartDate;
    @BindView(R.id.layout_journey_end_date)
    View layoutJourneyEndDate;
    @BindView(R.id.layout_starting_km)
    View layoutStartingKm;
    @BindView(R.id.layout_ending_km)
    View layoutEndingKm;
    @BindView(R.id.layout_total_distance)
    View layoutTotalDistance;
    @BindView(R.id.layout_time_taken)
    View layoutTimeTaken;
    @BindView(R.id.layout_rate_per_km)
    View layoutRatePerKm;
    @BindView(R.id.layout_per_km_cost)
    View layoutPerKmCost;
    @BindView(R.id.layout_total_cost)
    View layoutTotalCost;
    @BindView(R.id.txt_view_time_taken)
    TextView txtViewTimeTaken;
    @BindView(R.id.txt_view_rate_per_km)
    TextView txtViewRatePerKm;
    @BindView(R.id.txt_view_per_km_cost)
    TextView txtViewPerKmCost;
    @BindView(R.id.txt_view_total_cost)
    TextView txtViewTotalCost;
    @BindView(R.id.layout_cab_name)
    View layoutCabName;
    @BindView(R.id.layout_cab_number)
    View layoutCabNumber;
    @BindView(R.id.layout_driver_name)
    View layoutDriverName;
    @BindView(R.id.layout_driver_phone)
    View layoutDriverPhone;
    @BindView(R.id.layout_base_fare)
    View layoutBaseFare;
    @BindView(R.id.txt_view_booking_id)
    TextView txtViewBookingId;
    @BindView(R.id.txt_view_booking_date)
    TextView txtViewBookingDate;
    @BindView(R.id.txt_view_type_of_travel)
    TextView txtViewTypeOfTravel;
    int bookingId;
    VendorJourneyDetailsPresenterImpl mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendor_journey_details);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initialisePresenter();
        bookingId = getIntent().getIntExtra("bookingId", 0);
        mPresenter.fetchJourneyDetails(bookingId);
        showLoader();
    }

    public void initialisePresenter(){
        mPresenter = new VendorJourneyDetailsPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @Override
    public void loadData(VendorOrderDetails vendorOrderDetails) {
        mainLayout.setVisibility(View.VISIBLE);
        txtViewBookingId.setText(Integer.toString(vendorOrderDetails.booking_id));
        try {
            txtViewBookingDate.setText(convertBookingDate(vendorOrderDetails.bookingDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        txtViewBaseFare.setText("Rs. " + vendorOrderDetails.baseFare);
        txtViewCabType.setText(vendorOrderDetails.cabType);
        txtViewEndKm.setText(vendorOrderDetails.journeyEndKm + " km");
        try {
            txtViewPickUpDate.setText(convertPickUpDate(vendorOrderDetails.pickupDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        txtViewPickUpTime.setText(vendorOrderDetails.pickUpTime);
        txtViewPlaceToVisit.setText(vendorOrderDetails.visitPlace);
        txtViewReportingAddress.setText(vendorOrderDetails.reportingAddress);
        txtViewStartKm.setText(vendorOrderDetails.journeyStartKm + " km");
        txtViewTotalDistance.setText(vendorOrderDetails.totalDistanceKm + " km");
        txtViewEmployeeName.setText(vendorOrderDetails.employee_name);
        txtViewEmployeeEmail.setText(vendorOrderDetails.employee_email);
        txtViewCabName.setText(vendorOrderDetails.cabName);
        txtViewCabNo.setText(vendorOrderDetails.cabNo);
        txtViewDriverName.setText(vendorOrderDetails.driverName);
        txtViewDriverPhoneNo.setText(vendorOrderDetails.driverMobile);
        txtViewTypeOfTravel.setText(vendorOrderDetails.travelTypeName);
        if(vendorOrderDetails.journeyStartDate == null){
            layoutJourneyStartDate.setVisibility(View.GONE);
        }else {
            layoutJourneyStartDate.setVisibility(View.VISIBLE);
            try {
                txtViewStartDate.setText(convertBookingDate(vendorOrderDetails.journeyStartDate));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        if(vendorOrderDetails.journeyEndDate == null){
            layoutJourneyEndDate.setVisibility(View.GONE);
        }else{
            layoutJourneyEndDate.setVisibility(View.VISIBLE);
            try {
                txtViewEndDate.setText(convertBookingDate(vendorOrderDetails.journeyEndDate));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        if(vendorOrderDetails.journeyStartKm == null){
            layoutStartingKm.setVisibility(View.GONE);
        }else{
            layoutStartingKm.setVisibility(View.VISIBLE);
        }
        if(vendorOrderDetails.journeyEndKm == null || vendorOrderDetails.journeyEndKm.isEmpty()){
            layoutEndingKm.setVisibility(View.GONE);
        }else{
            layoutEndingKm.setVisibility(View.VISIBLE);
        }
        if(vendorOrderDetails.totalDistanceKm == null){
            layoutTotalDistance.setVisibility(View.GONE);
        }else{
            layoutTotalDistance.setVisibility(View.VISIBLE);
        }

        if(vendorOrderDetails.timeTaken == null){
            layoutTimeTaken.setVisibility(View.GONE);
        }else{
            txtViewTimeTaken.setText(vendorOrderDetails.timeTaken);
            layoutTimeTaken.setVisibility(View.VISIBLE);
        }

        if(vendorOrderDetails.ratePerKm == null){
            layoutRatePerKm.setVisibility(View.GONE);
        }else{
            layoutRatePerKm.setVisibility(View.VISIBLE);
            txtViewRatePerKm.setText("Rs. " + vendorOrderDetails.ratePerKm);
        }

        if(vendorOrderDetails.perKmCost == null){
            layoutPerKmCost.setVisibility(View.GONE);
        }else{
            layoutPerKmCost.setVisibility(View.VISIBLE);
            txtViewPerKmCost.setText(vendorOrderDetails.perKmCost);
        }

        if(vendorOrderDetails.totalCost == null){
            layoutTotalCost.setVisibility(View.GONE);
        }else{
            layoutTotalCost.setVisibility(View.VISIBLE);
            txtViewTotalCost.setText("Rs. " + vendorOrderDetails.totalCost);
        }

        if(vendorOrderDetails.cabName == null){
            layoutCabName.setVisibility(View.GONE);
        }else{
            layoutCabName.setVisibility(View.VISIBLE);
        }

        if(vendorOrderDetails.cabNo == null){
            layoutCabNumber.setVisibility(View.GONE);
        }else{
            layoutCabNumber.setVisibility(View.VISIBLE);
        }

        if(vendorOrderDetails.driverName == null){
            layoutDriverName.setVisibility(View.GONE);
        }else{
            layoutDriverName.setVisibility(View.VISIBLE);
        }

        if(vendorOrderDetails.driverMobile == null){
            layoutDriverPhone.setVisibility(View.GONE);
        }else{
            layoutDriverPhone.setVisibility(View.VISIBLE);
        }

        if(vendorOrderDetails.baseFare == null){
            layoutBaseFare.setVisibility(View.GONE);
        }else{
            layoutBaseFare.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void showLoader() {
        layoutLoader.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoader() {
        layoutLoader.setVisibility(View.GONE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public String convertBookingDate(String dateString) throws ParseException {
        DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.ENGLISH);
        DateFormat targetFormat = new SimpleDateFormat("MMMM dd, yyyy hh:mm a");
        Date date = originalFormat.parse(dateString);
        String formattedDate = targetFormat.format(date);
        return formattedDate;
    }

    public String convertPickUpDate(String dateString) throws ParseException {
        DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        DateFormat targetFormat = new SimpleDateFormat("MMMM dd, yyyy");
        Date date = originalFormat.parse(dateString);
        String formattedDate = targetFormat.format(date);
        return formattedDate;
    }

}
