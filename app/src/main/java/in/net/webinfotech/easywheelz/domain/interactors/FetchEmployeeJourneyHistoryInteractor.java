package in.net.webinfotech.easywheelz.domain.interactors;

import in.net.webinfotech.easywheelz.domain.model.Order.JourneyDetails;

/**
 * Created by Raj on 25-06-2019.
 */

public interface FetchEmployeeJourneyHistoryInteractor {
    interface Callback{
        void onFetchJourneyHistorySuccess(JourneyDetails[] journeyDetails, int totalPage);
        void onFetchJourneyHistoryFail(String errorMsg);
    }
}
