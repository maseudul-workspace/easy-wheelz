package in.net.webinfotech.easywheelz.domain.interactors.impl;

import in.net.webinfotech.easywheelz.domain.executors.Executor;
import in.net.webinfotech.easywheelz.domain.executors.MainThread;
import in.net.webinfotech.easywheelz.domain.interactors.AcceptOrderInteractor;
import in.net.webinfotech.easywheelz.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.easywheelz.domain.model.Other.OrderAcceptResponse;
import in.net.webinfotech.easywheelz.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 21-06-2019.
 */

public class AcceptOrderInteractorImpl extends AbstractInteractor implements AcceptOrderInteractor {

    Callback mCallback;
    OtherRepositoryImpl mRepository;
    String apiKey;
    int userId;
    int bookingId;
    int cabId;
    String driverName;
    String bookingMobile;
    String cabNo;

    public AcceptOrderInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, OtherRepositoryImpl mRepository, String apiKey, int userId, int bookingId, int cabId, String driverName, String bookingMobile, String cabNo) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.apiKey = apiKey;
        this.userId = userId;
        this.bookingId = bookingId;
        this.cabId = cabId;
        this.driverName = driverName;
        this.bookingMobile = bookingMobile;
        this.cabNo = cabNo;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onOrderAcceptFail(errorMsg);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onOrderAcceptSuccess();
            }
        });
    }

    @Override
    public void run() {
        final OrderAcceptResponse orderAcceptResponse = mRepository.acceptOrder(apiKey, userId, bookingId, cabId, driverName, bookingMobile, cabNo);
        if(orderAcceptResponse == null){
            notifyError("Something went wrong");
        }else if(!orderAcceptResponse.status){
            notifyError(orderAcceptResponse.message);
        }else{
            postMessage();
        }
    }
}
