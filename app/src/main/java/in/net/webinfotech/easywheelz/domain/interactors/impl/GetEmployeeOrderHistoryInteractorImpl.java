package in.net.webinfotech.easywheelz.domain.interactors.impl;

import in.net.webinfotech.easywheelz.domain.executors.Executor;
import in.net.webinfotech.easywheelz.domain.executors.MainThread;
import in.net.webinfotech.easywheelz.domain.interactors.GetEmployeeOrderHistoryInteracator;
import in.net.webinfotech.easywheelz.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.easywheelz.domain.model.Order.EmployeeOrderHistory;
import in.net.webinfotech.easywheelz.domain.model.Order.EmployeeOrderHistoryWrapper;
import in.net.webinfotech.easywheelz.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 11-06-2019.
 */

public class GetEmployeeOrderHistoryInteractorImpl extends AbstractInteractor implements GetEmployeeOrderHistoryInteracator {

    Callback mCallback;
    String apiKey;
    int userId;
    int page;
    OtherRepositoryImpl mRepository;

    public GetEmployeeOrderHistoryInteractorImpl(Executor threadExecutor,
                                                 MainThread mainThread,
                                                 Callback callback,
                                                 OtherRepositoryImpl repository,
                                                 String apiKey,
                                                 int userId,
                                                 int page
                                          ) {
        super(threadExecutor, mainThread);
        this.mCallback = callback;
        this.apiKey = apiKey;
        this.userId = userId;
        this.page = page;
        mRepository = repository;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingEmployeeOrderHistoryFail(errorMsg);
            }
        });
    }

    private void postMessage(final EmployeeOrderHistory[] employeeOrderHistories, final int totalPage){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingEmployeeOrderHistorySuccess(employeeOrderHistories, totalPage);
            }
        });
    }

    @Override
    public void run() {
        final EmployeeOrderHistoryWrapper employeeOrderHistoryWrapper = mRepository.getEmployeeOrderHistories(apiKey, userId, page);
        if(employeeOrderHistoryWrapper == null){
            notifyError("Something went wrong");
        }else if(!employeeOrderHistoryWrapper.status){
            notifyError(employeeOrderHistoryWrapper.message);
        }else{
            postMessage(employeeOrderHistoryWrapper.orderHistories, employeeOrderHistoryWrapper.total_page);
        }
    }
}
