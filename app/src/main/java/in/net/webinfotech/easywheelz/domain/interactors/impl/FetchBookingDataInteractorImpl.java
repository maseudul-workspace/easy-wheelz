package in.net.webinfotech.easywheelz.domain.interactors.impl;

import in.net.webinfotech.easywheelz.domain.executors.Executor;
import in.net.webinfotech.easywheelz.domain.executors.MainThread;
import in.net.webinfotech.easywheelz.domain.interactors.FetchBookingDataInteractor;
import in.net.webinfotech.easywheelz.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.easywheelz.domain.model.Other.BookingData;
import in.net.webinfotech.easywheelz.domain.model.Other.BookingDataWrapper;
import in.net.webinfotech.easywheelz.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 07-06-2019.
 */

public class FetchBookingDataInteractorImpl extends AbstractInteractor implements FetchBookingDataInteractor {

    Callback mCallback;
    OtherRepositoryImpl mRepository;
    String apiKey;
    int userId;

    public FetchBookingDataInteractorImpl(Executor threadExecutor,
                                          MainThread mainThread,
                                          Callback callback,
                                          OtherRepositoryImpl repository,
                                          String apiKey,
                                          int userId
    ) {
        super(threadExecutor, mainThread);
        this.mCallback = callback;
        this.mRepository = repository;
        this.apiKey = apiKey;
        this.userId = userId;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onBookingDataFetchFail(errorMsg);
            }
        });
    }

    private void postMessage(final BookingData bookingData){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onBookingDataFetchSuccess(bookingData);
            }
        });
    }

    @Override
    public void run() {

        final BookingDataWrapper bookingDataWrapper = mRepository.fetchBookingData(apiKey, userId);
        if(bookingDataWrapper == null){
            notifyError("Something went wrong");
        }else if(!bookingDataWrapper.status){
            notifyError(bookingDataWrapper.message);
        }else{
            postMessage(bookingDataWrapper.bookingSections);
        }

    }
}
