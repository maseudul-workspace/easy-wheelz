package in.net.webinfotech.easywheelz.presentation.presenters;

import in.net.webinfotech.easywheelz.domain.model.Other.BookingData;
import in.net.webinfotech.easywheelz.presentation.presenters.base.BasePresenter;

/**
 * Created by Raj on 07-06-2019.
 */

public interface EmployeePresenter extends BasePresenter {
    void fetchBookingData();
    void setFirebaseToken(String firebaseToken);
    void bookCab(int vendorId, int carTypeId, String pickUpDate, String pickUpTime, int reportingLocationId, String reportingAddress, int typeOfTravelId, String visitPlace);
    interface View{
        void loadData(BookingData bookingData);
        void dismisDialog();
        void onBookingSuccess();
    }
}
