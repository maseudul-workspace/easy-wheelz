package in.net.webinfotech.easywheelz.presentation.ui.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.net.webinfotech.easywheelz.AndroidApplication;
import in.net.webinfotech.easywheelz.R;
import in.net.webinfotech.easywheelz.domain.executors.impl.ThreadExecutor;
import in.net.webinfotech.easywheelz.presentation.presenters.LoginPresenter;
import in.net.webinfotech.easywheelz.presentation.presenters.impl.LoginPresenterImpl;
import in.net.webinfotech.easywheelz.presentation.routers.LoginRouter;
import in.net.webinfotech.easywheelz.threading.MainThreadImpl;

public class LoginActivity extends AppCompatActivity implements LoginPresenter.View, LoginRouter{

    @BindView(R.id.linear_layout_horizontal_employee_login)
    LinearLayout linearLayoutHorizontalEmployeeLogin;
    @BindView(R.id.linear_layout_horizontal_vendor_login)
    LinearLayout linearLayoutHorizontalVendorLogin;
    @BindView(R.id.linear_layout_vertical_employee_login)
    LinearLayout linearLayoutVerticalEmployeeLogin;
    @BindView(R.id.linear_layout_vertical_vendor_login)
    LinearLayout linearLayoutVerticalVendorLogin;
    @BindView(R.id.edit_text_email)
    EditText editTextEmail;
    @BindView(R.id.edit_text_password)
    EditText editTextPassword;
    @BindView(R.id.layout_loader)
    View loaderLayout;
    int userType = 1;
    AndroidApplication androidApplication;
    LoginPresenterImpl mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        androidApplication = (AndroidApplication) getApplicationContext();
        androidApplication.setUserType("employee");
        ButterKnife.bind(this);
        initialisePresenter();
    }

    @OnClick(R.id.btn_login) void onLoginBtnClicked(){
        if(editTextEmail.getText().toString().trim().isEmpty() || editTextPassword.getText().toString().trim().isEmpty()){
            Toast.makeText(this, "Some fields are empty", Toast.LENGTH_SHORT).show();
        }else if(!Patterns.EMAIL_ADDRESS.matcher(editTextEmail.getText().toString()).matches()){
            Toast.makeText(this, "Please insert a valid email", Toast.LENGTH_SHORT).show();
        }else{
            showLoader();
            mPresenter.loginCheck(editTextEmail.getText().toString(), editTextPassword.getText().toString(), userType);
        }

//        if(androidApplication.getUserType().equals("employee")){
//            Intent intent = new Intent(this, EmployeeActivity.class);
//            startActivity(intent);
//        }else{
//            Intent intent = new Intent(this, VendorActivity.class);
//            startActivity(intent);
//        }

    }

    public void initialisePresenter(){
        mPresenter = new LoginPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this, this);
    }

    @OnClick(R.id.linear_layout_horizontal_vendor_login) void onVendorLoginClicked(){
        linearLayoutHorizontalVendorLogin.setVisibility(View.GONE);
        linearLayoutHorizontalEmployeeLogin.setVisibility(View.VISIBLE);
        linearLayoutVerticalEmployeeLogin.setVisibility(View.GONE);
        linearLayoutVerticalVendorLogin.setVisibility(View.VISIBLE);
        androidApplication.setUserType("vendor");
        userType = 2;
    }

    @OnClick(R.id.linear_layout_horizontal_employee_login) void onEmployeeLoginClicked(){
        linearLayoutHorizontalVendorLogin.setVisibility(View.VISIBLE);
        linearLayoutHorizontalEmployeeLogin.setVisibility(View.GONE);
        linearLayoutVerticalEmployeeLogin.setVisibility(View.VISIBLE);
        linearLayoutVerticalVendorLogin.setVisibility(View.GONE);
        androidApplication.setUserType("employee");
        userType = 1;
    }


    @Override
    public void showLoader() {
        loaderLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoader() {
        loaderLayout.setVisibility(View.INVISIBLE);
    }

    @Override
    public void goToEmployeeActivity() {
        Intent employeeIntent = new Intent(this, EmployeeActivity.class);
        startActivity(employeeIntent);
        finish();
    }

    @Override
    public void goToVendorActivity() {
        Intent vendorIntent = new Intent(this, VendorActivity.class);
        startActivity(vendorIntent);
        finish();
    }
}
