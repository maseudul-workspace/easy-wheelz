package in.net.webinfotech.easywheelz.domain.interactors;

/**
 * Created by Raj on 03-07-2019.
 */

public interface SetFirebaseTokenInteractor {
    interface Callback{
        void onSetFirebaseTokenSuccess();
        void onSetFirebaseTokenFail();
    }
}
