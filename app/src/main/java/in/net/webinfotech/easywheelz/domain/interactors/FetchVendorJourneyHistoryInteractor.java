package in.net.webinfotech.easywheelz.domain.interactors;

import in.net.webinfotech.easywheelz.domain.model.Order.JourneyDetails;

/**
 * Created by Raj on 02-07-2019.
 */

public interface FetchVendorJourneyHistoryInteractor {
    interface Callback{
        void onFetchJourneyHistorySuccess(JourneyDetails[] journeyDetails, int totalPage);
        void onFetchJourneyHistoryFail(String errorMsg);
    }
}
