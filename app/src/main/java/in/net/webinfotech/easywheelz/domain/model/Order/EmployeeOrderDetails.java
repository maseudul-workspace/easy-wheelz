package in.net.webinfotech.easywheelz.domain.model.Order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 12-06-2019.
 */

public class EmployeeOrderDetails {
    @SerializedName("booking_id")
    @Expose
    public int booking_id;

    @SerializedName("reporting_address")
    @Expose
    public String reportingAddress;

    @SerializedName("visit_place")
    @Expose
    public String visitPlace;

    @SerializedName("employee_name")
    @Expose
    public String employee_name;

    @SerializedName("employee_email")
    @Expose
    public String employee_email;

    @SerializedName("cab_type")
    @Expose
    public String cabType;

    @SerializedName("cab_type_id")
    @Expose
    public int cabTypeId;

    @SerializedName("pickup_date")
    @Expose
    public String pickupDate;

    @SerializedName("pickup_time")
    @Expose
    public String pickUpTime;

    @SerializedName("booking_date")
    @Expose
    public String bookingDate;

    @SerializedName("journey_status")
    @Expose
    public int journeyStatus;

    @SerializedName("booking_status")
    @Expose
    public int bookingStatus;

    @SerializedName("cab_id")
    @Expose
    public int cabId;

    @SerializedName("cab_name")
    @Expose
    public String cabName;

    @SerializedName("cab_no")
    @Expose
    public String cabNo;

    @SerializedName("driver_name")
    @Expose
    public String driverName;

    @SerializedName("driber_mobile")
    @Expose
    public String driverMobile;

    @SerializedName("base_fare")
    @Expose
    public Integer baseFare;

    @SerializedName("booking_reject_comment")
    @Expose
    public String bookingRejectComment;

    @SerializedName("journey_start_date")
    @Expose
    public String journeyStartDate;

    @SerializedName("journey_start_km")
    @Expose
    public Long journeyStartKm;

    @SerializedName("journey_end_date")
    @Expose
    public String journeyEndDate;

    @SerializedName("journey_end_km")
    @Expose
    public String journeyEndKm;

    @SerializedName("total_distance_km")
    @Expose
    public Integer totalDistanceKm;

    @SerializedName("time_taken")
    @Expose
    public String timeTaken;

    @SerializedName("rate_per_km")
    @Expose
    public Double ratePerKm;

    @SerializedName("per_km_cost")
    @Expose
    public Double perKmCost;

    @SerializedName("total")
    @Expose
    public String totalCost;

    @SerializedName("travel_type_name")
    @Expose
    public String travelTypeName;

}
