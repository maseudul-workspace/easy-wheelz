package in.net.webinfotech.easywheelz.domain.interactors;

import in.net.webinfotech.easywheelz.domain.model.Other.BookingData;

/**
 * Created by Raj on 07-06-2019.
 */

public interface FetchBookingDataInteractor {
    interface Callback{
        void onBookingDataFetchSuccess(BookingData bookingData);
        void onBookingDataFetchFail(String errorMsg);
    }
}
