package in.net.webinfotech.easywheelz.presentation.ui.adapters;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.easywheelz.R;
import in.net.webinfotech.easywheelz.domain.model.Order.VendorOrderHistory;

/**
 * Created by Raj on 02-04-2019.
 */

public class VendorCabOrderHistoryAdapter extends RecyclerView.Adapter<VendorCabOrderHistoryAdapter.ViewHolder> {

    public interface Callback{
        void goToOrderDetails(int bookingId, int bookingStatus);
    }

    public Context mContext;
    public Callback mCallback;
    VendorOrderHistory[] vendorOrderHistories;

    public VendorCabOrderHistoryAdapter(Context mContext, Callback mCallback, VendorOrderHistory[] vendorOrderHistories) {
        this.mContext = mContext;
        this.mCallback = mCallback;
        this.vendorOrderHistories = vendorOrderHistories;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_cab_orders_vendor, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        switch (vendorOrderHistories[position].bookingStatus){
            case 1:
                holder.txtViewStatus.setText("Pending");
                holder.txtViewStatus.setTextColor(mContext.getResources().getColor(R.color.orange1));
                break;
            case 2:
                holder.txtViewStatus.setText("Accepted");
                holder.txtViewStatus.setTextColor(mContext.getResources().getColor(R.color.green3));
                break;
            case 3:
                holder.txtViewStatus.setText("Rejected");
                holder.txtViewStatus.setTextColor(mContext.getResources().getColor(R.color.red2));
                break;
            case 4:
                holder.txtViewStatus.setText("Cancelled");
                holder.txtViewStatus.setTextColor(mContext.getResources().getColor(R.color.red2));
                break;
        }

        holder.txtViewCabType.setText(vendorOrderHistories[position].cabType);
        holder.txtViewDestination.setText(vendorOrderHistories[position].visitPlace);
        holder.txtViewReportingAddress.setText(vendorOrderHistories[position].reportingAddress);
        holder.layoutMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mCallback.goToOrderDetails(vendorOrderHistories[position].booking_id, vendorOrderHistories[position].bookingStatus);
                    }
                }, 300);
            }
        });
        if(vendorOrderHistories[position].bookingStatus == 2){
            switch (vendorOrderHistories[position].journeyStatus){
                case 1:
                    holder.txtViewJourneyStatus.setText("Journey Not Started");
                    break;
                case 2:
                    holder.txtViewJourneyStatus.setText("On The Way");
                    break;
                case 3:
                    holder.txtViewJourneyStatus.setText("Journey Completed");
                    break;
            }
        }else{
            holder.txtViewJourneyStatus.setText("");
        }

        try {
            holder.txtViewOrderDate.setText(convertDate(vendorOrderHistories[position].bookingDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return vendorOrderHistories.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.txt_view_status)
        TextView txtViewStatus;
        @BindView(R.id.txt_order_date)
        TextView txtViewOrderDate;
        @BindView(R.id.txt_view_car_type)
        TextView txtViewCabType;
        @BindView(R.id.txt_view_destination)
        TextView txtViewDestination;
        @BindView(R.id.txt_view_reporting_address)
        TextView txtViewReportingAddress;
        @BindView(R.id.txt_view_journey_status)
        TextView txtViewJourneyStatus;
        @BindView(R.id.card_layout_main)
        View layoutMain;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void updateDataset(VendorOrderHistory[] vendorOrderHistories){
        this.vendorOrderHistories = vendorOrderHistories;
    }

    public String convertDate(String dateString) throws ParseException {
        DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.ENGLISH);
        DateFormat targetFormat = new SimpleDateFormat("MMMM dd, yyyy hh:mm a");
        Date date = originalFormat.parse(dateString);
        String formattedDate = targetFormat.format(date);
        return formattedDate;
    }

}
