package in.net.webinfotech.easywheelz.presentation.presenters;

import in.net.webinfotech.easywheelz.domain.model.Order.EmployeeOrderDetails;
import in.net.webinfotech.easywheelz.presentation.presenters.base.BasePresenter;

/**
 * Created by Raj on 11-06-2019.
 */

public interface EmployeeJourneyPresenter extends BasePresenter{
    void startJourney(int bookingId, String startKm);
    void getJourneyDetails(int bookingId);
    void endJourney(int bookingId, String endKm);
    interface View{
        void loadData(EmployeeOrderDetails employeeOrderDetails);
        void showLoader();
        void hideLoader();
        void showMainLoader();
        void hideMainLoader();
    }
}
