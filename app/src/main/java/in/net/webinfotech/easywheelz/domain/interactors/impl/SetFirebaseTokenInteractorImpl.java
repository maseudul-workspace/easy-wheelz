package in.net.webinfotech.easywheelz.domain.interactors.impl;

import in.net.webinfotech.easywheelz.domain.executors.Executor;
import in.net.webinfotech.easywheelz.domain.executors.MainThread;
import in.net.webinfotech.easywheelz.domain.interactors.SetFirebaseTokenInteractor;
import in.net.webinfotech.easywheelz.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.easywheelz.domain.model.User.SetFirebaseTokenResponse;
import in.net.webinfotech.easywheelz.repository.impl.UserRepositoryImpl;

/**
 * Created by Raj on 03-07-2019.
 */

public class SetFirebaseTokenInteractorImpl extends AbstractInteractor implements SetFirebaseTokenInteractor {

    Callback mCallback;
    UserRepositoryImpl mRepository;
    String apiKey;
    int userId;
    String firebaseToken;

    public SetFirebaseTokenInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, UserRepositoryImpl mRepository, String apiKey, int userId, String firebaseToken) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.apiKey = apiKey;
        this.userId = userId;
        this.firebaseToken = firebaseToken;
    }

    private void notifyError() {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onSetFirebaseTokenFail();
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onSetFirebaseTokenSuccess();
            }
        });
    }

    @Override
    public void run() {
        SetFirebaseTokenResponse setFirebaseTokenResponse = mRepository.setFirebaseTokenResponse(apiKey, userId, firebaseToken);
        if(setFirebaseTokenResponse == null){
            notifyError();
        }else if(!setFirebaseTokenResponse.status){
            notifyError();
        }else{
            postMessage();
        }
    }
}
