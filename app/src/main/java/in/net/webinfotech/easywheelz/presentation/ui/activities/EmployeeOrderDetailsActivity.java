package in.net.webinfotech.easywheelz.presentation.ui.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.easywheelz.R;
import in.net.webinfotech.easywheelz.domain.executors.impl.ThreadExecutor;
import in.net.webinfotech.easywheelz.domain.model.Order.EmployeeOrderDetails;
import in.net.webinfotech.easywheelz.presentation.presenters.EmployeeOrderDetailsPresenter;
import in.net.webinfotech.easywheelz.presentation.presenters.impl.EmployeeOrderDetailsPresenterImpl;
import in.net.webinfotech.easywheelz.threading.MainThreadImpl;

public class EmployeeOrderDetailsActivity extends AppCompatActivity implements EmployeeOrderDetailsPresenter.View {

    @BindView(R.id.txt_view_pick_up_date)
    TextView txtViewPickUpDate;
    @BindView(R.id.txt_view_pick_up_time)
    TextView txtViewPickUpTime;
    @BindView(R.id.txt_view_reporting_address)
    TextView txtViewReportingAddress;
    @BindView(R.id.txt_view_place_to_visit)
    TextView txtViewPlaceToVisit;
    @BindView(R.id.txt_view_cab_name)
    TextView txtViewCabName;
    @BindView(R.id.txt_view_cab_no)
    TextView txtViewCabNo;
    @BindView(R.id.txt_view_driver_phone_no)
    TextView txtViewDriverPhoneNo;
    @BindView(R.id.txt_view_driver_name)
    TextView txtViewDriverName;
    @BindView(R.id.layout_main)
    View layoutMain;
    @BindView(R.id.layout_loader_main)
    View layoutLoader;
    @BindView(R.id.layout_cab_name)
    View layoutCabName;
    @BindView(R.id.layout_cab_number)
    View layoutCabNumber;
    @BindView(R.id.layout_driver_name)
    View layoutDriverName;
    @BindView(R.id.layout_driver_phone)
    View layoutDriverPhone;
    @BindView(R.id.layout_remarks)
    View layoutRemarks;
    @BindView(R.id.txt_view_remarks)
    TextView txtViewRemarks;
    @BindView(R.id.txt_view_booking_id)
    TextView txtViewBookingId;
    @BindView(R.id.txt_view_booking_date)
    TextView txtViewBookingDate;
    EmployeeOrderDetailsPresenterImpl mPresenter;
    int bookingId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_order_details);
        ButterKnife.bind(this);
        bookingId = getIntent().getIntExtra("bookingId", 0);
        getSupportActionBar().setTitle("Order Details");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initialisePresenter();
        mPresenter.fetchOrderDetails(bookingId);
    }

    public void initialisePresenter(){
        mPresenter = new EmployeeOrderDetailsPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void loadData(EmployeeOrderDetails employeeOrderDetails) {
        layoutMain.setVisibility(View.VISIBLE);
        txtViewBookingId.setText(Integer.toString(employeeOrderDetails.booking_id));
        try {
            txtViewBookingDate.setText(convertBookingDate(employeeOrderDetails.bookingDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        try {
            txtViewPickUpDate.setText(convertPickUpDate(employeeOrderDetails.pickupDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        txtViewPickUpTime.setText(employeeOrderDetails.pickUpTime);
        txtViewReportingAddress.setText(employeeOrderDetails.reportingAddress);
        txtViewPlaceToVisit.setText(employeeOrderDetails.visitPlace);
        if(employeeOrderDetails.cabName == null || employeeOrderDetails.cabName.isEmpty()){
            layoutCabName.setVisibility(View.GONE);
        }else{
            layoutCabName.setVisibility(View.VISIBLE);
            txtViewCabName.setText(employeeOrderDetails.cabName);
        }
        if(employeeOrderDetails.cabNo == null || employeeOrderDetails.cabNo.isEmpty()){
            layoutCabNumber.setVisibility(View.GONE);
        }else{
            layoutCabNumber.setVisibility(View.VISIBLE);
            txtViewCabNo.setText(employeeOrderDetails.cabNo);
        }
        if(employeeOrderDetails.driverMobile == null || employeeOrderDetails.driverMobile.isEmpty()){
            layoutDriverPhone.setVisibility(View.GONE);
        }else{
            layoutDriverPhone.setVisibility(View.VISIBLE);
            txtViewDriverPhoneNo.setText(employeeOrderDetails.driverMobile);
        }
        if(employeeOrderDetails.driverName == null || employeeOrderDetails.driverName.isEmpty()){
            layoutDriverName.setVisibility(View.GONE);
        }else{
            layoutDriverName.setVisibility(View.VISIBLE);
            txtViewDriverName.setText(employeeOrderDetails.driverName);
        }

        if(employeeOrderDetails.bookingRejectComment == null){
            layoutRemarks.setVisibility(View.GONE);
        }else{
            layoutRemarks.setVisibility(View.VISIBLE);
            txtViewRemarks.setText(employeeOrderDetails.bookingRejectComment);
        }
    }

    @Override
    public void showLoader() {
        layoutLoader.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoader() {
        layoutLoader.setVisibility(View.GONE);
    }

    public String convertBookingDate(String dateString) throws ParseException {
        DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.ENGLISH);
        DateFormat targetFormat = new SimpleDateFormat("MMMM dd, yyyy hh:mm a");
        Date date = originalFormat.parse(dateString);
        String formattedDate = targetFormat.format(date);
        return formattedDate;
    }

    public String convertPickUpDate(String dateString) throws ParseException {
        DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        DateFormat targetFormat = new SimpleDateFormat("MMMM dd, yyyy");
        Date date = originalFormat.parse(dateString);
        String formattedDate = targetFormat.format(date);
        return formattedDate;
    }

}
