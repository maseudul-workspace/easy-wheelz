package in.net.webinfotech.easywheelz.domain.interactors;

/**
 * Created by Raj on 10-06-2019.
 */

public interface GetBookingHistoryInteractor {
    interface Callback{
        void onGettingBookingHistorySuccess();
        void onGettingBookingHistoryFail(String errorMsg);
    }
}
