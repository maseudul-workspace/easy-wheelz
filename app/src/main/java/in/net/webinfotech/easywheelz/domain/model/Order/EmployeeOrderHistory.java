package in.net.webinfotech.easywheelz.domain.model.Order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 11-06-2019.
 */

public class EmployeeOrderHistory {
    @SerializedName("booking_id")
    @Expose
    public int booking_id;

    @SerializedName("reporting_address")
    @Expose
    public String reportingAddress;

    @SerializedName("visit_place")
    @Expose
    public String visitPlace;

    @SerializedName("cab_type")
    @Expose
    public String cabType;

    @SerializedName("pickup_date")
    @Expose
    public String pickupDate;

    @SerializedName("pickup_time")
    @Expose
    public String pickUpTime;

    @SerializedName("booking_date")
    @Expose
    public String bookingDate;

    @SerializedName("journey_status")
    @Expose
    public int journeyStatus;

    @SerializedName("booking_status")
    @Expose
    public int bookingStatus;

}
