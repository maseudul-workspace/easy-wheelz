package in.net.webinfotech.easywheelz.domain.interactors;

import in.net.webinfotech.easywheelz.domain.model.Order.VendorOrderDetails;

/**
 * Created by Raj on 20-06-2019.
 */

public interface GetVendorOrderDetailsInteractor {
    interface Callback{
        void onGettingOrderDetailsSuccess(VendorOrderDetails vendorOrderDetails);
        void onGettingOrderDetailsFail(String errorMsg);
    }
}
