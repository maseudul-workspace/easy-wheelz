package in.net.webinfotech.easywheelz.domain.model.Other;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 07-06-2019.
 */

public class TypeOfTravel {
    @SerializedName("travel_id")
    @Expose
    public int travel_id;

    @SerializedName("travel_name")
    @Expose
    public String travel_name;
}
