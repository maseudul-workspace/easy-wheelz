package in.net.webinfotech.easywheelz.presentation.presenters;

import in.net.webinfotech.easywheelz.domain.model.Order.EmployeeOrderDetails;
import in.net.webinfotech.easywheelz.presentation.presenters.base.BasePresenter;

/**
 * Created by Raj on 01-07-2019.
 */

public interface EmployeeJourneyDetailsPresenter extends BasePresenter{
    void fetchJourneyDetails(int bookingId);
    interface View{
        void loadData(EmployeeOrderDetails employeeOrderDetails);
        void showLoader();
        void hideLoader();
    }
}
