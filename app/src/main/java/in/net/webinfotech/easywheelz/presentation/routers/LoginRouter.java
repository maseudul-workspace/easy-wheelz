package in.net.webinfotech.easywheelz.presentation.routers;

/**
 * Created by Raj on 04-06-2019.
 */

public interface LoginRouter {
    void goToEmployeeActivity();
    void goToVendorActivity();
}
