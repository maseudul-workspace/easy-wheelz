package in.net.webinfotech.easywheelz.presentation.presenters.impl;

import android.app.ProgressDialog;
import android.content.Context;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import in.net.webinfotech.easywheelz.AndroidApplication;
import in.net.webinfotech.easywheelz.domain.executors.Executor;
import in.net.webinfotech.easywheelz.domain.executors.MainThread;
import in.net.webinfotech.easywheelz.domain.interactors.CancelOrderInteractor;
import in.net.webinfotech.easywheelz.domain.interactors.GetEmployeeOrderHistoryInteracator;
import in.net.webinfotech.easywheelz.domain.interactors.impl.CancelOrderInteractorImpl;
import in.net.webinfotech.easywheelz.domain.interactors.impl.GetEmployeeOrderHistoryInteractorImpl;
import in.net.webinfotech.easywheelz.domain.model.Order.EmployeeOrderHistory;
import in.net.webinfotech.easywheelz.domain.model.User.UserInfo;
import in.net.webinfotech.easywheelz.presentation.presenters.EmployeeOrderHistoryPresenter;
import in.net.webinfotech.easywheelz.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.easywheelz.presentation.routers.EmployeeOrderHistoryRouter;
import in.net.webinfotech.easywheelz.presentation.ui.adapters.EmployeeOrderHistoryAdapter;
import in.net.webinfotech.easywheelz.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 11-06-2019.
 */

public class EmployeeOrderHistoryPresenterImpl extends AbstractPresenter implements EmployeeOrderHistoryPresenter,
                                                                                    GetEmployeeOrderHistoryInteracator.Callback,
                                                                                    EmployeeOrderHistoryAdapter.Callback,
                                                                                    CancelOrderInteractor.Callback
                                                                                    {

    Context mContext;
    GetEmployeeOrderHistoryInteractorImpl mInteractor;
    EmployeeOrderHistoryPresenter.View mView;
    AndroidApplication androidApplication;
    EmployeeOrderHistoryRouter mRouter;
    EmployeeOrderHistory[] newEmployeeOrderHistories;
    EmployeeOrderHistoryAdapter adapter;
    CancelOrderInteractorImpl cancelOrderInteractor;
    ProgressDialog progressDialog;

    public EmployeeOrderHistoryPresenterImpl(Executor executor,
                                             MainThread mainThread,
                                             Context context,
                                             EmployeeOrderHistoryPresenter.View view,
                                             EmployeeOrderHistoryRouter router
                                             ) {
        super(executor, mainThread);
        this.mContext = context;
        mView = view;
        this.mRouter = router;
    }

    @Override
    public void getOrderHistory(int page, String type) {
        if(type.equals("refresh")){
            newEmployeeOrderHistories = null;
        }
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        mInteractor = new GetEmployeeOrderHistoryInteractorImpl(mExecutor, mMainThread, this, new OtherRepositoryImpl(), userInfo.api_key, userInfo.user_id, page);
        mInteractor.execute();
    }

    @Override
    public void onGettingEmployeeOrderHistorySuccess(EmployeeOrderHistory[] employeeOrderHistories, int totalPage) {
        EmployeeOrderHistory[] tempOrderHistories;
        tempOrderHistories = newEmployeeOrderHistories;
        try {
            int len1 = tempOrderHistories.length;
            int len2 = employeeOrderHistories.length;
            newEmployeeOrderHistories = new EmployeeOrderHistory[len1 + len2];
            System.arraycopy(tempOrderHistories, 0, newEmployeeOrderHistories, 0, len1);
            System.arraycopy(employeeOrderHistories, 0, newEmployeeOrderHistories, len1, len2);
            adapter.updateDataSet(newEmployeeOrderHistories);
            adapter.notifyDataSetChanged();
            mView.hidePaginationLoader();
        }catch (NullPointerException e){
            newEmployeeOrderHistories = employeeOrderHistories;
            adapter = new EmployeeOrderHistoryAdapter(mContext, this, employeeOrderHistories);
            mView.loadData(adapter, totalPage);
            mView.hideLoader();
        }
        mView.stopRefreshing();
    }

    @Override
    public void onGettingEmployeeOrderHistoryFail(String errorMsg) {
        mView.hideLoader();
        mView.hidePaginationLoader();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT, true).show();
        mView.stopRefreshing();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void goToJourneyDetails(int bookingId) {
        mRouter.goToJourneyDetails(bookingId);
    }

    @Override
    public void goToOrderDetails(int bookingId) {
        mRouter.goToOrderDetails(bookingId);
    }

    @Override
    public void cancelOrder(int bookingId, int position) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        cancelOrderInteractor = new CancelOrderInteractorImpl(mExecutor, mMainThread, this, new OtherRepositoryImpl(), userInfo.api_key, userInfo.user_id, bookingId, position);
        cancelOrderInteractor.execute();
        showProgressDialog();
    }

    @Override
    public void onOrderCancelSuccess(int position) {
        progressDialog.dismiss();
        Toasty.success(mContext, "Order Cancelled Successfully", Toast.LENGTH_SHORT, true).show();
        adapter.onOrderCancelSuccess(position);
    }

    @Override
    public void onOrderCancelFail(String errorMsg) {
        progressDialog.dismiss();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT, true).show();
    }

    public void showProgressDialog(){
        progressDialog = new ProgressDialog(mContext);
        progressDialog.setMessage("Please Wait..."); // Setting Message
        progressDialog.setTitle("Easy Wheels"); // Setting Title
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER); // Progress Dialog Style Spinner
        progressDialog.show(); // Display Progress Dialog
        progressDialog.setCancelable(false);
    }
}
