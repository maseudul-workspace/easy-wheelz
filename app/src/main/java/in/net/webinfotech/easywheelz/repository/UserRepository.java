package in.net.webinfotech.easywheelz.repository;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Raj on 03-06-2019.
 */

public interface UserRepository {
    @POST("api/user_login/user_login_check.php")
    @FormUrlEncoded
    Call<ResponseBody> userLogInCheck(@Field("email") String email,
                                      @Field("password") String password,
                                      @Field("user_type") int user_type
    );

    @POST("api/user/change_password.php")
    @FormUrlEncoded
    Call<ResponseBody> changePassword(@Field("api_key") String apiKey,
                                      @Field("user_id") int userId,
                                      @Field("old_pass") String oldPassword,
                                      @Field("new_pass") String newPassword,
                                      @Field("cnf_pass") String confirmPassword
    );

    @POST("api/firebase_token/update_token.php")
    @FormUrlEncoded
    Call<ResponseBody> setFireBaseToken(@Field("api_key") String apiKey,
                                      @Field("user_id") int userId,
                                      @Field("firebase_token") String firebaseToken
    );

}
