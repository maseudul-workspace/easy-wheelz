package in.net.webinfotech.easywheelz.presentation.ui.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.easywheelz.R;
import in.net.webinfotech.easywheelz.domain.model.Order.EmployeeOrderHistory;

/**
 * Created by Raj on 10-04-2019.
 */

public class EmployeeOrderHistoryAdapter extends RecyclerView.Adapter<EmployeeOrderHistoryAdapter.ViewHolder> {

    public interface Callback{
        void goToJourneyDetails(int bookingId);
        void goToOrderDetails(int bookingId);
        void cancelOrder(int bookingId, int position);
    }

    Callback mCallback;
    Context mContext;
    public EmployeeOrderHistory[] orderHistories;


    public EmployeeOrderHistoryAdapter(Context context, Callback callback, EmployeeOrderHistory[] orderHistories) {
        this.mCallback = callback;
        this.mContext = context;
        this.orderHistories = orderHistories;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_employee_order_history, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        switch (orderHistories[position].bookingStatus){
            case 1:
                holder.txtViewStatus.setText("Pending");
                holder.txtViewStatus.setTextColor(mContext.getResources().getColor(R.color.orange1));
                holder.btnStartJourney.setVisibility(View.GONE);
                holder.layoutEndJourney.setVisibility(View.GONE);
                holder.btnCancelJourney.setVisibility(View.VISIBLE);
                break;
            case 2:
                holder.txtViewStatus.setText("Accepted");
                holder.txtViewStatus.setTextColor(mContext.getResources().getColor(R.color.green3));
                break;
            case 3:
                holder.txtViewStatus.setText("Rejected");
                holder.txtViewStatus.setTextColor(mContext.getResources().getColor(R.color.red2));
                holder.btnStartJourney.setVisibility(View.GONE);
                holder.layoutEndJourney.setVisibility(View.GONE);
                holder.btnCancelJourney.setVisibility(View.GONE);
                break;
            case 4:
                holder.txtViewStatus.setText("Cancelled");
                holder.txtViewStatus.setTextColor(mContext.getResources().getColor(R.color.red2));
                holder.btnStartJourney.setVisibility(View.GONE);
                holder.layoutEndJourney.setVisibility(View.GONE);
                holder.btnCancelJourney.setVisibility(View.GONE);
                break;
        }

        if(orderHistories[position].bookingStatus == 2){
            switch (orderHistories[position].journeyStatus){
                case 1:
                    holder.btnStartJourney.setVisibility(View.VISIBLE);
                    holder.layoutEndJourney.setVisibility(View.GONE);
                    holder.btnCancelJourney.setVisibility(View.VISIBLE);
                    break;
                case 2:
                    holder.btnStartJourney.setVisibility(View.GONE);
                    holder.layoutEndJourney.setVisibility(View.VISIBLE);
                    holder.txtViewJourneyStatus.setText("Journey Ongoing");
                    holder.btnEndJourney.setText("End Journey");
                    holder.btnCancelJourney.setVisibility(View.GONE);
                    break;
                case 3:
                    holder.btnStartJourney.setVisibility(View.GONE);
                    holder.layoutEndJourney.setVisibility(View.VISIBLE);
                    holder.txtViewJourneyStatus.setText("Journey Completed");
                    holder.btnEndJourney.setText("View Journey");
                    holder.btnCancelJourney.setVisibility(View.GONE);
                    break;
            }
        }

        try {
            holder.txtViewOrderDate.setText(convertDate(orderHistories[position].bookingDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.txtViewCabType.setText(orderHistories[position].cabType);
        holder.txtViewDestination.setText(orderHistories[position].visitPlace);
        holder.txtViewReportingAddress.setText(orderHistories[position].reportingAddress);
        holder.btnStartJourney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.goToJourneyDetails(orderHistories[position].booking_id);
            }
        });
        holder.btnEndJourney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.goToJourneyDetails(orderHistories[position].booking_id);
            }
        });
        holder.layoutMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.goToOrderDetails(orderHistories[position].booking_id);
            }
        });
        holder.btnCancelJourney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setTitle("Easy Wheels");
                builder.setMessage("Are you sure ?");
                builder.setCancelable(false);
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mCallback.cancelOrder(orderHistories[position].booking_id, position);
                    }
                });

                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                builder.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return orderHistories.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.txt_view_status)
        TextView txtViewStatus;
        @BindView(R.id.txt_order_date)
        TextView txtViewOrderDate;
        @BindView(R.id.txt_view_car_type)
        TextView txtViewCabType;
        @BindView(R.id.txt_view_destination)
        TextView txtViewDestination;
        @BindView(R.id.txt_view_reporting_address)
        TextView txtViewReportingAddress;
        @BindView(R.id.btn_start_journey)
        Button btnStartJourney;
        @BindView(R.id.layout_end_journey)
        View layoutEndJourney;
        @BindView(R.id.btn_end_journey)
        Button btnEndJourney;
        @BindView(R.id.txt_view_journey_status)
        TextView txtViewJourneyStatus;
        @BindView(R.id.card_layout_main)
        View layoutMain;
        @BindView(R.id.btn_cancel_journey)
        Button btnCancelJourney;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void updateDataSet(EmployeeOrderHistory[] orderHistories){
        this.orderHistories = orderHistories;
    }

    public void onOrderCancelSuccess(int position){
        orderHistories[position].bookingStatus = 4;
        notifyItemChanged(position);
    }

    public String convertDate(String dateString) throws ParseException {
        DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.ENGLISH);
        DateFormat targetFormat = new SimpleDateFormat("MMMM dd, yyyy hh:mm a");
        Date date = originalFormat.parse(dateString);
        String formattedDate = targetFormat.format(date);
        return formattedDate;
    }

}
