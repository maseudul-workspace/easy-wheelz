package in.net.webinfotech.easywheelz.presentation.presenters;

import in.net.webinfotech.easywheelz.domain.model.Order.EmployeeOrderDetails;
import in.net.webinfotech.easywheelz.presentation.presenters.base.BasePresenter;

/**
 * Created by Raj on 13-06-2019.
 */

public interface EmployeeOrderDetailsPresenter extends BasePresenter {
    void fetchOrderDetails(int bookingId);
    interface View{
        void loadData(EmployeeOrderDetails employeeOrderDetails);
        void showLoader();
        void hideLoader();
    }
}
