package in.net.webinfotech.easywheelz.presentation.presenters.impl;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import in.net.webinfotech.easywheelz.AndroidApplication;
import in.net.webinfotech.easywheelz.domain.executors.Executor;
import in.net.webinfotech.easywheelz.domain.executors.MainThread;
import in.net.webinfotech.easywheelz.domain.interactors.EndJourneyInteractor;
import in.net.webinfotech.easywheelz.domain.interactors.GetEmployeeOrderDetailsInteractor;
import in.net.webinfotech.easywheelz.domain.interactors.StartJourneyInteractor;
import in.net.webinfotech.easywheelz.domain.interactors.impl.EndJourneyInteractorImpl;
import in.net.webinfotech.easywheelz.domain.interactors.impl.GetEmployeeOrderDetailsInteractorImpl;
import in.net.webinfotech.easywheelz.domain.interactors.impl.StartJourneyInteractorImpl;
import in.net.webinfotech.easywheelz.domain.model.Order.EmployeeOrderDetails;
import in.net.webinfotech.easywheelz.domain.model.User.UserInfo;
import in.net.webinfotech.easywheelz.presentation.presenters.EmployeeJourneyPresenter;
import in.net.webinfotech.easywheelz.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.easywheelz.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 11-06-2019.
 */

public class EmployeeJourneyPresenterImpl extends AbstractPresenter implements EmployeeJourneyPresenter, StartJourneyInteractor.Callback, GetEmployeeOrderDetailsInteractor.Callback, EndJourneyInteractor.Callback {

    Context mContext;
    StartJourneyInteractorImpl mInteractor;
    EmployeeJourneyPresenter.View mView;
    AndroidApplication androidApplication;
    GetEmployeeOrderDetailsInteractorImpl getEmployeeOrderDetailsInteractor;
    EndJourneyInteractorImpl endJourneyInteractor;
    int bookingId;

    public EmployeeJourneyPresenterImpl(Executor executor,
                                        MainThread mainThread,
                                        Context context,
                                        EmployeeJourneyPresenter.View view
                                        ) {
        super(executor, mainThread);
        mContext = context;
        mView = view;
    }

    @Override
    public void startJourney(int bookingId, String startKm) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        mInteractor = new StartJourneyInteractorImpl(mExecutor, mMainThread, this, new OtherRepositoryImpl(), userInfo.api_key, userInfo.user_id, bookingId, startKm);
        mInteractor.execute();
    }

    @Override
    public void getJourneyDetails(int bookingId) {
        this.bookingId = bookingId;
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        getEmployeeOrderDetailsInteractor = new GetEmployeeOrderDetailsInteractorImpl(mExecutor, mMainThread, this, new OtherRepositoryImpl(), userInfo.api_key, userInfo.user_id, bookingId);
        getEmployeeOrderDetailsInteractor.execute();
    }

    @Override
    public void endJourney(int bookingId, String endKm) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        endJourneyInteractor = new EndJourneyInteractorImpl(mExecutor, mMainThread, this, new OtherRepositoryImpl(), userInfo.api_key, userInfo.user_id, bookingId, endKm);
        endJourneyInteractor.execute();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void onJourneyStartedSuccess() {
        getJourneyDetails(bookingId);
        Toasty.success(mContext, "Journey started successfully", Toast.LENGTH_SHORT, true).show();
    }

    @Override
    public void onJourStartedFail(String errorMsg) {
        mView.hideLoader();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT, true).show();
    }

    @Override
    public void onGettingOrderDetailsSuccess(EmployeeOrderDetails employeeOrderDetails) {
        mView.hideMainLoader();
        mView.hideLoader();
        mView.loadData(employeeOrderDetails);
    }

    @Override
    public void onGettingOrderDetailsFail(String errorMsg) {
        mView.hideMainLoader();
        mView.hideLoader();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT, true).show();
    }

    @Override
    public void onJourneyEndSuccess() {
        getJourneyDetails(bookingId);
        Toasty.success(mContext, "Journey ended successfully", Toast.LENGTH_SHORT, true).show();
    }

    @Override
    public void onJourneyEndFail(String errorMsg) {
        mView.hideLoader();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT, true).show();
    }
}
