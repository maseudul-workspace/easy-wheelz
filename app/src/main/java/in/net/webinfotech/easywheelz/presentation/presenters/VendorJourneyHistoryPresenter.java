package in.net.webinfotech.easywheelz.presentation.presenters;

import in.net.webinfotech.easywheelz.presentation.presenters.base.BasePresenter;
import in.net.webinfotech.easywheelz.presentation.ui.adapters.VendorJourneyHistoryAdapter;

/**
 * Created by Raj on 01-07-2019.
 */

public interface VendorJourneyHistoryPresenter extends BasePresenter{
    void fetchJourneyHistory(int pageNo, String type);
    interface View{
        void loadAdapter(VendorJourneyHistoryAdapter adapter, int totalPage);
        void showPaginationProgressLayout();
        void hidePaginationProgressLayout();
        void showLoader();
        void hideLoader();
        void stopRefreshing();
    }
}
