package in.net.webinfotech.easywheelz.domain.model.Other;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 07-06-2019.
 */

public class BookingData {
    @SerializedName("vendor_list")
    @Expose
    public Vendor[] vendors;

    @SerializedName("car_type")
    @Expose
    public CarType[] carTypes;

    @SerializedName("reporting_location")
    @Expose
    public ReportingLocation[] reportingLocations;

    @SerializedName("type_of_travel")
    @Expose
    public TypeOfTravel[] typeOfTravels;

}
