package in.net.webinfotech.easywheelz.presentation.ui.dialogs;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import in.net.webinfotech.easywheelz.R;

/**
 * Created by Raj on 10-06-2019.
 */

public class BookingConfirmationDialog {

    public interface Callback{
        void onConfirmationClicked();
    }

    AlertDialog.Builder builder;
    Context mContext;
    Activity mActivity;
    AlertDialog dialog;
    View dialogContainer;
    Callback mCallback;

    public BookingConfirmationDialog(Context mContext, Activity mActivity, Callback mCallback) {
        this.mContext = mContext;
        this.mActivity = mActivity;
        this.mCallback = mCallback;
    }

    public void showDialog(String vendor, String carType, String pickUpDate, String pickUpTime, String reportingLocation, String pickUpLocation, String typeOfTravel, String placeToVisit){
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.dialog_book_confirm, null);

//        Initialisation of textviews
        TextView txtViewVendor = (TextView) dialogContainer.findViewById(R.id.txt_view_vendor);
        TextView txtViewCarType = (TextView) dialogContainer.findViewById(R.id.txt_view_car_type);
        TextView txtViewPickUpDate = (TextView) dialogContainer.findViewById(R.id.txt_view_pick_up_date);
        TextView txtViewPickUpTime = (TextView) dialogContainer.findViewById(R.id.txt_view_pick_up_time);
        TextView txtViewReportingLocation = (TextView) dialogContainer.findViewById(R.id.txt_view_reporting_location);
        TextView txtViewPickUpLocation = (TextView) dialogContainer.findViewById(R.id.txt_view_pick_up_location);
        TextView txtViewTypeOfTravel = (TextView) dialogContainer.findViewById(R.id.txt_view_type_of_travel);
        TextView txtViewPlaceToVisit = (TextView) dialogContainer.findViewById(R.id.txt_view_place_to_visit);
        final Button btnConfirm = (Button) dialogContainer.findViewById(R.id.btn_confirm_book);
        final Button btnCancel = (Button) dialogContainer.findViewById(R.id.btn_dialog_cancel);
        final View loaderLayout = (View) dialogContainer.findViewById(R.id.layout_loader);

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loaderLayout.setVisibility(View.VISIBLE);
                btnCancel.setEnabled(false);
                btnConfirm.setEnabled(false);
                mCallback.onConfirmationClicked();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

//        Setting up the text views
        txtViewVendor.setText(vendor);
        txtViewCarType.setText(carType);
        txtViewPickUpDate.setText(pickUpDate);
        txtViewPickUpTime.setText(pickUpTime);
        txtViewReportingLocation.setText(reportingLocation);
        txtViewPickUpLocation.setText(pickUpLocation);
        txtViewTypeOfTravel.setText(typeOfTravel);
        txtViewPlaceToVisit.setText(placeToVisit);

        builder = new AlertDialog.Builder(mContext);
        builder.setView(dialogContainer);
        dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.setCancelable(false);
        dialog.show();

    }

    public void disimisDialog(){
        dialog.dismiss();
    }

}
