package in.net.webinfotech.easywheelz.domain.interactors;

import in.net.webinfotech.easywheelz.domain.model.Other.Cab;

/**
 * Created by Raj on 10-06-2019.
 */

public interface FetchCabInteractor {
    interface Callback{
        void onCabFetchSuccess(Cab[] cabs);
        void onCabFetchFail(String errorMsg);
    }
}
