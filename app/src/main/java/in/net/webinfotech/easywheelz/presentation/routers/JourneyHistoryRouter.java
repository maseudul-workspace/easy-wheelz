package in.net.webinfotech.easywheelz.presentation.routers;

/**
 * Created by Raj on 01-07-2019.
 */

public interface JourneyHistoryRouter {
    void goToJourneyDetails(int boookingId);
}
