package in.net.webinfotech.easywheelz.domain.interactors.impl;

import in.net.webinfotech.easywheelz.domain.executors.Executor;
import in.net.webinfotech.easywheelz.domain.executors.MainThread;
import in.net.webinfotech.easywheelz.domain.interactors.StartJourneyInteractor;
import in.net.webinfotech.easywheelz.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.easywheelz.domain.model.Other.JourneyStartResponse;
import in.net.webinfotech.easywheelz.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 11-06-2019.
 */

public class StartJourneyInteractorImpl extends AbstractInteractor implements StartJourneyInteractor {

    Callback mCallback;
    OtherRepositoryImpl mRepository;
    String apiKey;
    int userId;
    int bookingId;
    String startKm;

    public StartJourneyInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, OtherRepositoryImpl mRepository, String apiKey, int userId, int bookingId, String startKm) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.apiKey = apiKey;
        this.userId = userId;
        this.bookingId = bookingId;
        this.startKm = startKm;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onJourStartedFail(errorMsg);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onJourneyStartedSuccess();
            }
        });
    }

    @Override
    public void run() {
        final JourneyStartResponse journeyStartResponse = mRepository.startJourney(apiKey, userId, bookingId, startKm);
        if (journeyStartResponse == null) {
            notifyError("Something went wrong");
        } else if (!journeyStartResponse.status) {
            notifyError(journeyStartResponse.message);
        } else {
            postMessage();
        }
    }
}
