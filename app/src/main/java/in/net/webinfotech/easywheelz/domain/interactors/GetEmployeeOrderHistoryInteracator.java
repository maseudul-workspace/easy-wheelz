package in.net.webinfotech.easywheelz.domain.interactors;

import in.net.webinfotech.easywheelz.domain.model.Order.EmployeeOrderHistory;

/**
 * Created by Raj on 11-06-2019.
 */

public interface GetEmployeeOrderHistoryInteracator {
    interface Callback{
        void onGettingEmployeeOrderHistorySuccess(EmployeeOrderHistory[] employeeOrderHistories, int totalPage);
        void onGettingEmployeeOrderHistoryFail(String errorMsg);
    }
}
