package in.net.webinfotech.easywheelz.domain.model.Other;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 10-06-2019.
 */

public class Cab {
    @SerializedName("cab_id")
    @Expose
    public int cab_id;

    @SerializedName("cab_name")
    @Expose
    public String cab_name;
}
