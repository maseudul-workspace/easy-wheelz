package in.net.webinfotech.easywheelz.presentation.ui.activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import in.net.webinfotech.easywheelz.AndroidApplication;
import in.net.webinfotech.easywheelz.R;
import in.net.webinfotech.easywheelz.domain.model.User.UserInfo;

public class SplashActivity extends AppCompatActivity {

    AndroidApplication androidApplication;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
               checkUserLogIn();
            }
        }, 3000);
    }

    public void checkUserLogIn(){
        androidApplication = (AndroidApplication) getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(this);
        if(userInfo == null){
            Intent loginIntent = new Intent(this, LoginActivity.class);
            startActivity(loginIntent);
            finish();
        }else{
            if(userInfo.userType == 1){
                Intent employeeIntent = new Intent(this, EmployeeActivity.class);
                startActivity(employeeIntent);
                finish();
            }else{
                Intent vendorIntent = new Intent(this, VendorActivity.class);
                startActivity(vendorIntent);
                finish();
            }
        }
    }

}
