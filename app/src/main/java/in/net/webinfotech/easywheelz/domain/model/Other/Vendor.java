package in.net.webinfotech.easywheelz.domain.model.Other;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 07-06-2019.
 */

public class Vendor {
    @SerializedName("vendor_id")
    @Expose
    public int vendor_id;

    @SerializedName("vendor_name")
    @Expose
    public String vendor_name;
}
