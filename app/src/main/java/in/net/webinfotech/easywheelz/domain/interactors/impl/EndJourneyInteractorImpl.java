package in.net.webinfotech.easywheelz.domain.interactors.impl;

import in.net.webinfotech.easywheelz.domain.executors.Executor;
import in.net.webinfotech.easywheelz.domain.executors.MainThread;
import in.net.webinfotech.easywheelz.domain.interactors.EndJourneyInteractor;
import in.net.webinfotech.easywheelz.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.easywheelz.domain.model.Other.JourneyEndResponse;
import in.net.webinfotech.easywheelz.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 13-06-2019.
 */

public class EndJourneyInteractorImpl extends AbstractInteractor implements EndJourneyInteractor{

    Callback mCallback;
    OtherRepositoryImpl mRepository;
    String apiKey;
    int userId;
    int bookingId;
    String endKm;

    public EndJourneyInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, OtherRepositoryImpl mRepository, String apiKey, int userId, int bookingId, String endKm) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.apiKey = apiKey;
        this.userId = userId;
        this.bookingId = bookingId;
        this.endKm = endKm;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onJourneyEndFail(errorMsg);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onJourneyEndSuccess();
            }
        });
    }

    @Override
    public void run() {
        final JourneyEndResponse journeyEndResponse = mRepository.endJourney(apiKey, userId, bookingId, endKm);
        if(journeyEndResponse == null){
            notifyError("Something went wrong");
        }else if(!journeyEndResponse.status){
            notifyError(journeyEndResponse.message);
        }else{
            postMessage();
        }
    }
}
