package in.net.webinfotech.easywheelz.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import in.net.webinfotech.easywheelz.domain.executors.Executor;
import in.net.webinfotech.easywheelz.domain.executors.MainThread;
import in.net.webinfotech.easywheelz.domain.interactors.ChangePasswordInteractor;
import in.net.webinfotech.easywheelz.domain.interactors.impl.ChangePasswordInteratorImpl;
import in.net.webinfotech.easywheelz.presentation.presenters.ResetPasswordPresenter;
import in.net.webinfotech.easywheelz.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.easywheelz.repository.impl.UserRepositoryImpl;

/**
 * Created by Raj on 04-06-2019.
 */

public class ResetPasswordPresenterImpl extends AbstractPresenter implements ResetPasswordPresenter, ChangePasswordInteractor.Callback {

    Context mContext;
    ResetPasswordPresenter.View mView;
    ChangePasswordInteratorImpl mInteractor;

    public ResetPasswordPresenterImpl(Executor executor,
                                      MainThread mainThread,
                                      Context context,
                                      ResetPasswordPresenter.View view) {
        super(executor, mainThread);
        mContext = context;
        mView = view;
    }

    @Override
    public void changePassword(String apiKey, int userId, String currentPassword, String newPassword) {
        mInteractor = new ChangePasswordInteratorImpl(mExecutor, mMainThread, this, new UserRepositoryImpl(), apiKey, userId, currentPassword, newPassword);
        mInteractor.execute();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void onChangePasswordSuccess() {
        mView.hideLoader();
        Toasty.success(mContext, "Successfully changed password", Toast.LENGTH_SHORT, true).show();
    }

    @Override
    public void onChangePasswordFail(String errorMsg) {
        mView.hideLoader();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT, true).show();
    }
}
