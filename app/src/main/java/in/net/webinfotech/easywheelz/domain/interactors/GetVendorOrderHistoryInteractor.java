package in.net.webinfotech.easywheelz.domain.interactors;

import in.net.webinfotech.easywheelz.domain.model.Order.VendorOrderHistory;

/**
 * Created by Raj on 20-06-2019.
 */

public interface GetVendorOrderHistoryInteractor {
    interface Callback{
        void onGettingOrdersSuccess(VendorOrderHistory[] vendorOrderHistories, int totalPage);
        void onGettingOrdersFail(String errorMsg);
    }
}
