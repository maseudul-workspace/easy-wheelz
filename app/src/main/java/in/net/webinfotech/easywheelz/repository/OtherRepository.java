package in.net.webinfotech.easywheelz.repository;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Raj on 07-06-2019.
 */

public interface OtherRepository {

    @POST("api/configuration/fetch_required_data.php")
    @FormUrlEncoded
    Call<ResponseBody> fetchData(@Field("api_key") String apiKey,
                                      @Field("user_id") int userId
    );

    @POST("api/booking/booking.php")
    @FormUrlEncoded
    Call<ResponseBody> book(@Field("api_key") String apiKey,
                            @Field("user_id") int userId,
                            @Field("vendor_id") int vendorId,
                            @Field("car_type_id") int car_type_id,
                            @Field("pickup_date") String pickup_date,
                            @Field("pickup_time") String pickup_time,
                            @Field("reporting_location_id") int reporting_location_id,
                            @Field("reporting_address") String reporting_address,
                            @Field("type_of_travel_id") int typeOfTravelId,
                            @Field("visit_place") String visit_place
                            );

    @POST("api/booking/employee_booking_history.php")
    @FormUrlEncoded
    Call<ResponseBody> getBookingHistory(@Field("api_key") String apiKey,
                                 @Field("user_id") int userId
    );

    @POST("api/cabs/fetch_cabs_with_type.php")
    @FormUrlEncoded
    Call<ResponseBody> fetchCabs(@Field("api_key") String apiKey,
                                 @Field("user_id") int userId,
                                 @Field("cab_type_id") int cabTypeId
    );

    @POST("api/booking/employee_booking_history.php")
    @FormUrlEncoded
    Call<ResponseBody> fetchEmployeeOrderHistory(@Field("api_key") String apiKey,
                                                 @Field("user_id") int userId,
                                                 @Field("page") int page
    );

    @POST("api/booking/start_journey_user.php")
    @FormUrlEncoded
    Call<ResponseBody> startJourney(@Field("api_key") String apiKey,
                                    @Field("user_id") int userId,
                                    @Field("booking_id") int bookingId,
                                    @Field("start_km") String startKm
    );

    @POST("api/booking/end_journey_user.php")
    @FormUrlEncoded
    Call<ResponseBody> endJourney(@Field("api_key") String apiKey,
                                    @Field("user_id") int userId,
                                    @Field("booking_id") int bookingId,
                                    @Field("end_km") String startKm
    );

    @POST("api/booking/employee_single_order_view.php")
    @FormUrlEncoded
    Call<ResponseBody> getEmployeeOrderDetails(@Field("api_key") String apiKey,
                                               @Field("user_id") int userId,
                                               @Field("booking_id") int bookingId);

    @POST("api/booking/vendor_all_bookings.php")
    @FormUrlEncoded
    Call<ResponseBody> fetchVendorOrderHistory(@Field("api_key") String apiKey,
                                                 @Field("user_id") int userId,
                                                 @Field("page") int page
    );

    @POST("api/booking/vendor_single_order_view.php")
    @FormUrlEncoded
    Call<ResponseBody> getVendorOrderDetails(@Field("api_key") String apiKey,
                                               @Field("user_id") int userId,
                                               @Field("booking_id") int bookingId);

    @POST("api/booking/vendor_accept_booking.php")
    @FormUrlEncoded
    Call<ResponseBody> acceptOrder(@Field("api_key") String apiKey,
                                   @Field("user_id") int userId,
                                   @Field("booking_id") int bookingId,
                                   @Field("cab_id") int cabId,
                                   @Field("driver_name") String driverName,
                                   @Field("driver_mobile") String bookingMobile,
                                   @Field("cab_no") String cabNo
                                   );

    @POST("api/booking/vendor_reject_booking.php")
    @FormUrlEncoded
    Call<ResponseBody> rejectOrder(@Field("api_key") String apiKey,
                                   @Field("user_id") int userId,
                                   @Field("booking_id") int bookingId,
                                   @Field("comment") String comment
                                   );

    @POST("api/booking/employee_journey_history.php")
    @FormUrlEncoded
    Call<ResponseBody> fetchEmployeeJourneyHistory(@Field("api_key") String apiKey,
                                           @Field("user_id") int userId,
                                           @Field("page") int page);

    @POST("api/booking/vendor_journey_history.php")
    @FormUrlEncoded
    Call<ResponseBody> fetchVendorJourneyHistory(@Field("api_key") String apiKey,
                                                   @Field("user_id") int userId,
                                                   @Field("page") int page);


    @POST("api/booking/cancel_booking.php")
    @FormUrlEncoded
    Call<ResponseBody> cancelBooking(@Field("api_key") String apiKey,
                                     @Field("user_id") int userId,
                                     @Field("booking_id") int bookingId);

}
