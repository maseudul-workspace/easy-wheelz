package in.net.webinfotech.easywheelz.presentation.presenters;

import in.net.webinfotech.easywheelz.domain.model.Order.VendorOrderDetails;
import in.net.webinfotech.easywheelz.domain.model.Other.Cab;
import in.net.webinfotech.easywheelz.presentation.presenters.base.BasePresenter;

/**
 * Created by Raj on 10-06-2019.
 */

public interface VendorOrderDetailsPresenter extends BasePresenter{
    void fetchCabs(int cabTypeId);
    void getOrderDetails(int bookingId);
    void acceptOrder(int bookingId, int cabId, String driverName, String bookingMobile, String cabNo);
    void rejectOrder(int bookingId, String comment);
    interface View{
        void setCabs(Cab[] cabs);
        void loadData(VendorOrderDetails vendorOrderDetails);
        void showLoader();
        void hideLoader();
        void showAcceptDialogLoader();
        void hideAcceptDialogLoader();
        void showRejectDialogLoader();
        void hideRejectDialogLoader();
        void finishActivity();
        void onCabGettingFailed();
    }
}
