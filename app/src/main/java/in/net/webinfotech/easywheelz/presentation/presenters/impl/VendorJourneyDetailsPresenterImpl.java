package in.net.webinfotech.easywheelz.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import in.net.webinfotech.easywheelz.AndroidApplication;
import in.net.webinfotech.easywheelz.domain.executors.Executor;
import in.net.webinfotech.easywheelz.domain.executors.MainThread;
import in.net.webinfotech.easywheelz.domain.interactors.GetVendorOrderDetailsInteractor;
import in.net.webinfotech.easywheelz.domain.interactors.impl.GetVendorOrderDetailsInteractorImpl;
import in.net.webinfotech.easywheelz.domain.model.Order.VendorOrderDetails;
import in.net.webinfotech.easywheelz.domain.model.User.UserInfo;
import in.net.webinfotech.easywheelz.presentation.presenters.VendorJourneyDetailsPresenter;
import in.net.webinfotech.easywheelz.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.easywheelz.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 02-07-2019.
 */

public class VendorJourneyDetailsPresenterImpl extends AbstractPresenter implements VendorJourneyDetailsPresenter,
                                                                                    GetVendorOrderDetailsInteractor.Callback{

    Context mContext;
    VendorJourneyDetailsPresenter.View mView;
    GetVendorOrderDetailsInteractorImpl mInteractor;
    AndroidApplication androidApplication;

    public VendorJourneyDetailsPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void fetchJourneyDetails(int bookingId) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        mInteractor = new GetVendorOrderDetailsInteractorImpl(mExecutor, mMainThread, this, new OtherRepositoryImpl(), userInfo.api_key, userInfo.user_id, bookingId);
        mInteractor.execute();
    }

    @Override
    public void onGettingOrderDetailsSuccess(VendorOrderDetails vendorOrderDetails) {
        mView.loadData(vendorOrderDetails);
        mView.hideLoader();
    }

    @Override
    public void onGettingOrderDetailsFail(String errorMsg) {
        mView.hideLoader();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT, true).show();
    }
}
