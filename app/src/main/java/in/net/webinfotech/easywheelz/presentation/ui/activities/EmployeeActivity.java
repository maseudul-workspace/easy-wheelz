package in.net.webinfotech.easywheelz.presentation.ui.activities;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import in.net.webinfotech.easywheelz.R;
import in.net.webinfotech.easywheelz.domain.executors.impl.ThreadExecutor;
import in.net.webinfotech.easywheelz.domain.model.Other.BookingData;
import in.net.webinfotech.easywheelz.domain.model.Other.CarType;
import in.net.webinfotech.easywheelz.domain.model.Other.ReportingLocation;
import in.net.webinfotech.easywheelz.domain.model.Other.TypeOfTravel;
import in.net.webinfotech.easywheelz.domain.model.Other.Vendor;
import in.net.webinfotech.easywheelz.presentation.presenters.EmployeePresenter;
import in.net.webinfotech.easywheelz.presentation.presenters.impl.EmployeePresenterImpl;
import in.net.webinfotech.easywheelz.presentation.ui.dialogs.BookingConfirmationDialog;
import in.net.webinfotech.easywheelz.threading.MainThreadImpl;

public class EmployeeActivity extends BaseActivity implements EmployeePresenter.View, BookingConfirmationDialog.Callback{

    @BindView(R.id.radio_group_car_type)
    RadioGroup radioGroupCarType;
    @BindView(R.id.radio_group_reporting_location)
    RadioGroup radioGroupReportingLocation;
    @BindView(R.id.radio_group_travel_type)
    RadioGroup radioGroupTravelType;
    @BindView(R.id.radio_group_vendor)
    RadioGroup radioGroupVendor;
    @BindView(R.id.txt_view_date)
    TextView txtViewDate;
    @BindView(R.id.txt_view_time)
    TextView txtViewTime;
    @BindView(R.id.edit_text_pickup_location)
    TextView editTextPickUpLocation;
    @BindView(R.id.edit_text_place_visit)
    TextView editTextPlaceVisit;
    boolean isVendorSelected = false;
    boolean isCarTypeSelected = false;
    boolean isReportingLocationSelected = false;
    boolean isTypeOfTravelSelected = false;
    boolean isTimeSelected = false;
    boolean isDateSelected = false;
    String vendorName;
    String carType;
    String reportingLocation;
    String typeOfTravel;
    EmployeePresenterImpl mPresenter;
    BookingData bookingData;
    int vendorId;
    int carTypeId;
    String pickupDate;
    String pickUpTime;
    int reportingLocationId;
    String reportingAddress;
    int typeOfTravelId;
    String visitPlace;
    BookingConfirmationDialog bookingConfirmationDialog;
    Calendar tempCalendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inflateContent(R.layout.activity_employee_cab_book);
//        setUpRadioButtons();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        initialisePresenter();
        setFirebaseToken();
        mPresenter.fetchBookingData();
    }

    public void initialisePresenter(){
        mPresenter = new EmployeePresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }


    @OnClick(R.id.img_view_calendar) void onCalendarClicked(){
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        isDateSelected = true;
                        pickupDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        txtViewDate.setText(pickupDate);

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
    }

    @OnClick(R.id.img_view_clock) void onClockClicked(){
        final Calendar c = Calendar.getInstance();
        int mHour = c.get(Calendar.HOUR_OF_DAY);
        int mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        final TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        Calendar calendar = Calendar.getInstance();
                        calendar.set(Calendar.HOUR_OF_DAY,hourOfDay);
                        calendar.set(Calendar.MINUTE,minute);
                        tempCalendar = calendar;
                        isTimeSelected = true;
                        pickUpTime = new SimpleDateFormat("HH:mm:ss").format(calendar.getTime());
                        txtViewTime.setText(new SimpleDateFormat("hh:mm a").format(calendar.getTime()));
                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();
    }

    @Override
    public void loadData(BookingData bookingData) {
        this.bookingData = bookingData;
        setRadioGroupVendor(bookingData.vendors);
        setRadioGroupCarType(bookingData.carTypes);
        setRadioGroupTravelType(bookingData.typeOfTravels);
        setRadioGroupReportingLocation(bookingData.reportingLocations);

    }

    @Override
    public void dismisDialog() {
        bookingConfirmationDialog.disimisDialog();
    }

    @Override
    public void onBookingSuccess() {
        editTextPickUpLocation.setText("");
        editTextPlaceVisit.setText("");
        Intent orderHistoryIntent = new Intent(getApplicationContext(), EmployeeOrderHistoryActivity.class);
        startActivity(orderHistoryIntent);
    }

    public void setRadioGroupVendor(final Vendor[] vendors){
        for(int i = 0; i < vendors.length; i++){
            RadioButton radioButton = new RadioButton(this);
            radioButton.setText(vendors[i].vendor_name);
            radioButton.setId(View.generateViewId());
            radioGroupVendor.addView(radioButton);
        }
        radioGroupVendor.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                RadioButton checkedRadioBtn = (RadioButton) radioGroup.findViewById(i);
                vendorName = checkedRadioBtn.getText().toString();
                isVendorSelected = true;
                for (int index = 0; index < vendors.length; index++){
                    if(vendors[index].vendor_name.equals(vendorName)){
                        vendorId = vendors[index].vendor_id;
                        break;
                    }
                }
            }
        });
    }

    public void setRadioGroupCarType(final CarType[] carTypes){
        for(int i = 0; i < carTypes.length; i++){
            RadioButton radioButton = new RadioButton(this);
            radioButton.setText(carTypes[i].type_name);
            radioButton.setId(View.generateViewId());
            LinearLayout linearLayout = new LinearLayout(this);
            linearLayout.setOrientation(LinearLayout.HORIZONTAL);
            radioGroupCarType.addView(radioButton);
        }
        radioGroupCarType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                RadioButton checkedRadioBtn = (RadioButton) radioGroup.findViewById(i);
                carType = checkedRadioBtn.getText().toString();
                isCarTypeSelected = true;
                for (int index = 0; index < carTypes.length; index++){
                    if(carTypes[index].type_name.equals(carType)){
                        carTypeId = carTypes[index].type_id;
                        break;
                    }
                }
            }
        });
    }

    public void setRadioGroupReportingLocation(final ReportingLocation[] reportingLocations){
        for(int i = 0; i < reportingLocations.length; i++){
            RadioButton radioButton = new RadioButton(this);
            radioButton.setText(reportingLocations[i].location_name);
            radioButton.setId(View.generateViewId());
            radioGroupReportingLocation.addView(radioButton);
        }
        radioGroupReportingLocation.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                RadioButton checkedRadioBtn = (RadioButton) radioGroup.findViewById(i);
                reportingLocation = checkedRadioBtn.getText().toString();
                isReportingLocationSelected = true;
                for (int index = 0; index < reportingLocations.length; index++) {
                    if (reportingLocations[index].location_name.equals(reportingLocation)) {
                        reportingLocationId = reportingLocations[index].location_id;
                        break;
                    }

                }
            }
         });
    }

    public void setRadioGroupTravelType(final TypeOfTravel[] travelTypes){
        for(int i = 0; i < travelTypes.length; i++){
            RadioButton radioButton = new RadioButton(this);
            radioButton.setText(travelTypes[i].travel_name);
            radioButton.setId(View.generateViewId());
            radioGroupTravelType.addView(radioButton);
        }
        radioGroupTravelType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                RadioButton checkedRadioBtn = (RadioButton) radioGroup.findViewById(i);
                typeOfTravel = checkedRadioBtn.getText().toString();
                isTypeOfTravelSelected = true;
                for (int index = 0; index < travelTypes.length; index++) {
                    if (travelTypes[index].travel_name.equals(typeOfTravel)) {
                        typeOfTravelId = travelTypes[index].travel_id;
                        break;
                    }

                }
            }
        });

    }

    @OnClick(R.id.layout_book) void onBookClicked(){
        if(!isCarTypeSelected || !isReportingLocationSelected || !isTypeOfTravelSelected || !isVendorSelected || editTextPickUpLocation.getText().toString().trim().isEmpty() || editTextPlaceVisit.getText().toString().trim().isEmpty()){
            Toasty.warning(this, "Some fields are missing", Toast.LENGTH_SHORT, true).show();
        }else{
            bookingConfirmationDialog = new BookingConfirmationDialog(this, this, this);
            bookingConfirmationDialog.showDialog(vendorName, carType, pickupDate, new SimpleDateFormat("hh:mm a").format(tempCalendar.getTime()), reportingLocation, editTextPickUpLocation.getText().toString(), typeOfTravel, editTextPlaceVisit.getText().toString());
        }
    }

    @Override
    public void onConfirmationClicked() {
        mPresenter.bookCab(vendorId, carTypeId, pickupDate, pickUpTime, reportingLocationId, editTextPickUpLocation.getText().toString(), typeOfTravelId, editTextPlaceVisit.getText().toString());
    }

    public void setFirebaseToken(){
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        mPresenter.setFirebaseToken(refreshedToken);
    }
}
