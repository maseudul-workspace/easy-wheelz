package in.net.webinfotech.easywheelz.presentation.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.easywheelz.R;
import in.net.webinfotech.easywheelz.domain.model.Order.JourneyDetails;

/**
 * Created by Raj on 01-07-2019.
 */

public class VendorJourneyHistoryAdapter extends RecyclerView.Adapter<VendorJourneyHistoryAdapter.ViewHolder>{

    public interface Callback{
        void goToJourneyDetails(int bookingId);
    }

    Context mContext;
    JourneyDetails[] journeyDetails;
    Callback mCallback;

    public VendorJourneyHistoryAdapter(Context mContext, JourneyDetails[] journeyDetails, Callback callback) {
        this.mContext = mContext;
        this.journeyDetails = journeyDetails;
        mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_vendor_journey_history, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.txtViewReportingAddress.setText(journeyDetails[position].reportingAddress);
        holder.txtViewPlaceToVisit.setText(journeyDetails[position].visitPlace);
        try {
            holder.txtViewOrderDate.setText(convertDate(journeyDetails[position].bookingDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.goToJourneyDetails(journeyDetails[position].booking_id);
            }
        });
        holder.txtViewBookingId.setText(Integer.toString(journeyDetails[position].booking_id));
    }

    @Override
    public int getItemCount() {
        return journeyDetails.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.txt_view_total_km)
        TextView txtViewTotalKm;
        @BindView(R.id.txt_view_reporting_address)
        TextView txtViewReportingAddress;
        @BindView(R.id.txt_view_place_to_visit)
        TextView txtViewPlaceToVisit;
        @BindView(R.id.txt_view_booking_date)
        TextView txtViewOrderDate;
        @BindView(R.id.main_layout)
        View mainLayout;
        @BindView(R.id.txt_view_booking_id)
        TextView txtViewBookingId;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void updateDataSet(JourneyDetails[] journeyDetails){
        this.journeyDetails = journeyDetails;
    }

    public String convertDate(String dateString) throws ParseException {
        DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.ENGLISH);
        DateFormat targetFormat = new SimpleDateFormat("MMMM dd, yyyy hh:mm a");
        Date date = originalFormat.parse(dateString);
        String formattedDate = targetFormat.format(date);
        return formattedDate;
    }

}
