package in.net.webinfotech.easywheelz.domain.interactors.impl;

import in.net.webinfotech.easywheelz.domain.executors.Executor;
import in.net.webinfotech.easywheelz.domain.executors.MainThread;
import in.net.webinfotech.easywheelz.domain.interactors.GetEmployeeOrderDetailsInteractor;
import in.net.webinfotech.easywheelz.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.easywheelz.domain.model.Order.EmployeeOrderDetails;
import in.net.webinfotech.easywheelz.domain.model.Order.EmployeeOrderDetailsWrapper;
import in.net.webinfotech.easywheelz.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 12-06-2019.
 */

public class GetEmployeeOrderDetailsInteractorImpl extends AbstractInteractor implements GetEmployeeOrderDetailsInteractor {

    Callback mCallback;
    OtherRepositoryImpl mRepository;
    String apiKey;
    int userId;
    int bookingId;

    public GetEmployeeOrderDetailsInteractorImpl(Executor threadExecutor,
                                                 MainThread mainThread,
                                                 Callback callback,
                                                 OtherRepositoryImpl repository,
                                                 String apiKey,
                                                 int userId,
                                                 int bookingId
                                                 ) {
        super(threadExecutor, mainThread);
        this.mCallback = callback;
        this.mRepository = repository;
        this.apiKey = apiKey;
        this.userId = userId;
        this.bookingId = bookingId;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingOrderDetailsFail(errorMsg);
            }
        });
    }

    private void postMessage(final EmployeeOrderDetails employeeOrderDetails){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingOrderDetailsSuccess(employeeOrderDetails);
            }
        });
    }

    @Override
    public void run() {
        final EmployeeOrderDetailsWrapper employeeOrderDetailsWrapper = mRepository.getEmployeeOrderDetails(apiKey, userId, bookingId);
        if(employeeOrderDetailsWrapper == null){
            notifyError("Something went wrong");
        }else if(!employeeOrderDetailsWrapper.status){
            notifyError(employeeOrderDetailsWrapper.message);
        }else{
            postMessage(employeeOrderDetailsWrapper.employeeOrderDetails[0]);
        }
    }
}
