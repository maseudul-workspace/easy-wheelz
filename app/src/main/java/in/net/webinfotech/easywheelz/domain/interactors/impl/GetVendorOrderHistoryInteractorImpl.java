package in.net.webinfotech.easywheelz.domain.interactors.impl;

import in.net.webinfotech.easywheelz.domain.executors.Executor;
import in.net.webinfotech.easywheelz.domain.executors.MainThread;
import in.net.webinfotech.easywheelz.domain.interactors.GetVendorOrderHistoryInteractor;
import in.net.webinfotech.easywheelz.domain.interactors.base.AbstractInteractor;
import in.net.webinfotech.easywheelz.domain.model.Order.VendorOrderHistory;
import in.net.webinfotech.easywheelz.domain.model.Order.VendorOrderHistoryWrapper;
import in.net.webinfotech.easywheelz.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 20-06-2019.
 */

public class GetVendorOrderHistoryInteractorImpl extends AbstractInteractor implements GetVendorOrderHistoryInteractor {

    Callback mCallback;
    String apiKey;
    int userId;
    int page;
    OtherRepositoryImpl mRepository;

    public GetVendorOrderHistoryInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, String apiKey, int userId, int page, OtherRepositoryImpl mRepository) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.apiKey = apiKey;
        this.userId = userId;
        this.page = page;
        this.mRepository = mRepository;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingOrdersFail(errorMsg);
            }
        });
    }

    private void postMessage(final VendorOrderHistory[] vendorOrderHistories, final int totalPage){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingOrdersSuccess(vendorOrderHistories, totalPage);
            }
        });
    }

    @Override
    public void run() {
        final VendorOrderHistoryWrapper vendorOrderHistoryWrapper = mRepository.getVendorOrderHistory(apiKey, userId, page);
        if(vendorOrderHistoryWrapper == null){
            notifyError("Something went wrong");
        }else if(!vendorOrderHistoryWrapper.status){
            notifyError(vendorOrderHistoryWrapper.message);
        }else{
            postMessage(vendorOrderHistoryWrapper.vendorOrderHistories, vendorOrderHistoryWrapper.totalPage);
        }
    }
}
