package in.net.webinfotech.easywheelz.presentation.ui.activities;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.easywheelz.R;
import in.net.webinfotech.easywheelz.domain.executors.impl.ThreadExecutor;
import in.net.webinfotech.easywheelz.presentation.presenters.VendorJourneyHistoryPresenter;
import in.net.webinfotech.easywheelz.presentation.presenters.impl.VendorJourneyHistoryPresenterImpl;
import in.net.webinfotech.easywheelz.presentation.routers.JourneyHistoryRouter;
import in.net.webinfotech.easywheelz.presentation.ui.adapters.VendorJourneyHistoryAdapter;
import in.net.webinfotech.easywheelz.threading.MainThreadImpl;

public class VendorJourneyHistoryActivity extends AppCompatActivity implements JourneyHistoryRouter, VendorJourneyHistoryPresenter.View {

    @BindView(R.id.recycler_view_journey_history)
    RecyclerView recyclerView;
    @BindView(R.id.layout_loader)
    View layoutLoader;
    @BindView(R.id.pagination_progress_layout)
    View paginationProgressLayout;
    VendorJourneyHistoryPresenterImpl mPresenter;
    Boolean isScrolling = false;
    Integer currentItems;
    Integer totalItems;
    Integer scrollOutItems;
    int pageNo = 1;
    int totalPage = 1;
    LinearLayoutManager layoutManager;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vendor_journey_history);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initalisePresenter();
        mPresenter.fetchJourneyHistory(pageNo, "refresh");
        showLoader();
        setSwipeRefreshLayout();
    }

    public void setSwipeRefreshLayout(){
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isScrolling = false;
                pageNo = 1;
                totalPage = 1;
                mPresenter.fetchJourneyHistory(pageNo, "refresh");
            }
        });
    }

    public void initalisePresenter(){
        mPresenter = new VendorJourneyHistoryPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this, this);
    }

    @Override
    public void goToJourneyDetails(int boookingId) {
        Intent journeyDetailsIntent = new Intent(this, VendorJourneyDetailsActivity.class);
        journeyDetailsIntent.putExtra("bookingId", boookingId);
        startActivity(journeyDetailsIntent);
    }

    @Override
    public void loadAdapter(VendorJourneyHistoryAdapter adapter, final int totalPage) {
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
                {
                    isScrolling= true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItems = layoutManager.getChildCount();
                totalItems  = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstCompletelyVisibleItemPosition();
                if(!recyclerView.canScrollVertically(1))
                {
                    if(pageNo < totalPage) {
                        isScrolling = false;
                        pageNo = pageNo + 1;
                        showPaginationProgressLayout();
                        mPresenter.fetchJourneyHistory(pageNo, "");
                    }
                }
            }
        });
        recyclerView.setVisibility(View.VISIBLE);

    }

    @Override
    public void showPaginationProgressLayout() {
        paginationProgressLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hidePaginationProgressLayout() {
        paginationProgressLayout.setVisibility(View.GONE);
    }

    @Override
    public void showLoader() {
        layoutLoader.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoader() {
        layoutLoader.setVisibility(View.GONE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void stopRefreshing() {
        if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing())
        {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

}
