package in.net.webinfotech.easywheelz.domain.model.Other;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 07-06-2019.
 */

public class ReportingLocation {
    @SerializedName("location_id")
    @Expose
    public int location_id;

    @SerializedName("location_name")
    @Expose
    public String location_name;
}
